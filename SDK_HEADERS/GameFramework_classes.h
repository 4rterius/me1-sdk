/*
#############################################################################################
# Mass Effect (2007) (1.2.20608.0) SDK
# Generated with TheFeckless UE3 SDK Generator v1.4_Beta-Rev.51
# ========================================================================================= #
# File: GameFramework_classes.h
# ========================================================================================= #
# Credits: uNrEaL, Tamimego, SystemFiles, R00T88, _silencer, the1domo, K@N@VEL
# Thanks: HOOAH07, lowHertz
# Forums: www.uc-forum.com, www.gamedeception.net
#############################################################################################
*/

#ifdef _MSC_VER
	#pragma pack ( push, 0x4 )
#endif

/*
# ========================================================================================= #
# Constants
# ========================================================================================= #
*/


/*
# ========================================================================================= #
# Enums
# ========================================================================================= #
*/

// Enum GameFramework.GameSkelCtrl_Recoil.ERecoilStart
/*enum ERecoilStart
{
	ERS_Zero                                           = 0,
	ERS_Random                                         = 1,
	ERS_MAX                                            = 2
};*/


/*
# ========================================================================================= #
# Classes
# ========================================================================================= #
*/

// Class GameFramework.GameBreakableActor
// 0x0024 (0x02A8 - 0x0284)
class AGameBreakableActor : public AKActor
{
public:
	TArray< class UClass* >                            DamageTypes;                                      		// 0x0284 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	TArray< struct FBreakableStep >                    BreakableSteps;                                   		// 0x0290 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	int                                                CurrentBreakableStep;                             		// 0x029C (0x0004) [0x0000000000000000]              
	struct FLightingChannelContainer                   ParticleLightingChannels;                         		// 0x02A0 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      bParticlesAcceptLights : 1;                       		// 0x02A4 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	unsigned long                                      bParticlesAcceptDynamicLights : 1;                		// 0x02A4 (0x0004) [0x0000000000000001] [0x00000002] ( CPF_Edit )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 1070 ];

		return pClassPointer;
	};

	void eventDestroyed ( );
	void HideAndDestroy ( );
	void BreakLastApart ( class AController* EventInstigator );
	void BreakStepApart ( int BrokenStep );
	bool IsValidDamageType ( class UClass* inDamageType );
	void TakeStepDamage ( int Damage, class AController* EventInstigator, unsigned long bIsBroken, int BrokenStep );
	void TakeLastDamage ( int Damage, class AController* EventInstigator, unsigned long bIsBroken, int BrokenStep );
	void eventTakeDamage ( float Damage, class AController* EventInstigator, struct FVector HitLocation, struct FVector Momentum, class UClass* DamageType, struct FTraceHitInfo HitInfo, class AActor* DamageCauser, class UDamageType* pDamage );
	void RemoveDecals ( );
	void SetParticlesLighting ( class AEmitter* Emit );
	struct FVector GetOffsetToWorld ( struct FVector Offset );
};

UClass* AGameBreakableActor::pClassPointer = NULL;

// Class GameFramework.GamePawn
// 0x0000 (0x04C4 - 0x04C4)
class AGamePawn : public APawn
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 1071 ];

		return pClassPointer;
	};

	void GetTargetFrictionCylinder ( float* CylinderRadius, float* CylinderHeight );
};

UClass* AGamePawn::pClassPointer = NULL;

// Class GameFramework.GamePlayerController
// 0x0000 (0x0580 - 0x0580)
class AGamePlayerController : public APlayerController
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 1072 ];

		return pClassPointer;
	};

};

UClass* AGamePlayerController::pClassPointer = NULL;

// Class GameFramework.GamePlayerInput
// 0x0000 (0x0148 - 0x0148)
class UGamePlayerInput : public UPlayerInput
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 1073 ];

		return pClassPointer;
	};

};

UClass* UGamePlayerInput::pClassPointer = NULL;

// Class GameFramework.GameProjectile
// 0x0000 (0x0228 - 0x0228)
class AGameProjectile : public AProjectile
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 1074 ];

		return pClassPointer;
	};

};

UClass* AGameProjectile::pClassPointer = NULL;

// Class GameFramework.GameTypes
// 0x0000 (0x003C - 0x003C)
class UGameTypes : public UObject
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 1075 ];

		return pClassPointer;
	};

};

UClass* UGameTypes::pClassPointer = NULL;

// Class GameFramework.GameVehicle
// 0x0000 (0x064C - 0x064C)
class AGameVehicle : public ASVehicle
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 1076 ];

		return pClassPointer;
	};

};

UClass* AGameVehicle::pClassPointer = NULL;

// Class GameFramework.GameWeapon
// 0x0000 (0x02D0 - 0x02D0)
class AGameWeapon : public AWeapon
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 1077 ];

		return pClassPointer;
	};

};

UClass* AGameWeapon::pClassPointer = NULL;

// Class GameFramework.GameSkelCtrl_Recoil
// 0x007C (0x0110 - 0x0094)
class UGameSkelCtrl_Recoil : public USkelControlBase
{
public:
	struct FRecoilDef                                  Recoil;                                           		// 0x0094 (0x0070) [0x0000000000000001]              ( CPF_Edit )
	struct FVector2D                                   Aim;                                              		// 0x0104 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      bPlayRecoil : 1;                                  		// 0x010C (0x0004) [0x0000000000002001] [0x00000001] ( CPF_Edit | CPF_Transient )
	unsigned long                                      bOldPlayRecoil : 1;                               		// 0x010C (0x0004) [0x0000000000002000] [0x00000002] ( CPF_Transient )
	unsigned long                                      bApplyControl : 1;                                		// 0x010C (0x0004) [0x0000000000002000] [0x00000004] ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 1078 ];

		return pClassPointer;
	};

};

UClass* UGameSkelCtrl_Recoil::pClassPointer = NULL;

// Class GameFramework.GameActorFactoryBreakable
// 0x0000 (0x0095 - 0x0095)
class UGameActorFactoryBreakable : public UActorFactoryRigidBody
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 25580 ];

		return pClassPointer;
	};

};

UClass* UGameActorFactoryBreakable::pClassPointer = NULL;


#ifdef _MSC_VER
	#pragma pack ( pop )
#endif