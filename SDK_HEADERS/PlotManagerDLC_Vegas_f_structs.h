/*
#############################################################################################
# Mass Effect (2007) (1.2.20608.0) SDK
# Generated with TheFeckless UE3 SDK Generator v1.4_Beta-Rev.51
# ========================================================================================= #
# File: PlotManagerDLC_Vegas_f_structs.h
# ========================================================================================= #
# Credits: uNrEaL, Tamimego, SystemFiles, R00T88, _silencer, the1domo, K@N@VEL
# Thanks: HOOAH07, lowHertz
# Forums: www.uc-forum.com, www.gamedeception.net
#############################################################################################
*/

#ifdef _MSC_VER
	#pragma pack ( push, 0x4 )
#endif

/*
# ========================================================================================= #
# Function Structs
# ========================================================================================= #
*/

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1987
// [0x00020002] 
struct UBioAutoConditionals_execF1987_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1985
// [0x00020002] 
struct UBioAutoConditionals_execF1985_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1983
// [0x00020002] 
struct UBioAutoConditionals_execF1983_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1982
// [0x00020002] 
struct UBioAutoConditionals_execF1982_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1975
// [0x00020002] 
struct UBioAutoConditionals_execF1975_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1974
// [0x00020002] 
struct UBioAutoConditionals_execF1974_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1973
// [0x00020002] 
struct UBioAutoConditionals_execF1973_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F2002
// [0x00020002] 
struct UBioAutoConditionals_execF2002_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1970
// [0x00020002] 
struct UBioAutoConditionals_execF1970_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1969
// [0x00020002] 
struct UBioAutoConditionals_execF1969_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1968
// [0x00020002] 
struct UBioAutoConditionals_execF1968_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1967
// [0x00020002] 
struct UBioAutoConditionals_execF1967_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1966
// [0x00020002] 
struct UBioAutoConditionals_execF1966_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1965
// [0x00020002] 
struct UBioAutoConditionals_execF1965_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1964
// [0x00020002] 
struct UBioAutoConditionals_execF1964_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1963
// [0x00020002] 
struct UBioAutoConditionals_execF1963_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1962
// [0x00020002] 
struct UBioAutoConditionals_execF1962_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1961
// [0x00020002] 
struct UBioAutoConditionals_execF1961_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1960
// [0x00020002] 
struct UBioAutoConditionals_execF1960_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1959
// [0x00020002] 
struct UBioAutoConditionals_execF1959_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1958
// [0x00020002] 
struct UBioAutoConditionals_execF1958_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1957
// [0x00020002] 
struct UBioAutoConditionals_execF1957_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1956
// [0x00020002] 
struct UBioAutoConditionals_execF1956_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1955
// [0x00020002] 
struct UBioAutoConditionals_execF1955_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1954
// [0x00020002] 
struct UBioAutoConditionals_execF1954_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1953
// [0x00020002] 
struct UBioAutoConditionals_execF1953_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1952
// [0x00020002] 
struct UBioAutoConditionals_execF1952_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1951
// [0x00020002] 
struct UBioAutoConditionals_execF1951_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1950
// [0x00020002] 
struct UBioAutoConditionals_execF1950_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1949
// [0x00020002] 
struct UBioAutoConditionals_execF1949_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1948
// [0x00020002] 
struct UBioAutoConditionals_execF1948_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1947
// [0x00020002] 
struct UBioAutoConditionals_execF1947_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1946
// [0x00020002] 
struct UBioAutoConditionals_execF1946_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1945
// [0x00020002] 
struct UBioAutoConditionals_execF1945_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1944
// [0x00020002] 
struct UBioAutoConditionals_execF1944_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1943
// [0x00020002] 
struct UBioAutoConditionals_execF1943_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1942
// [0x00020002] 
struct UBioAutoConditionals_execF1942_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1941
// [0x00020002] 
struct UBioAutoConditionals_execF1941_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1940
// [0x00020002] 
struct UBioAutoConditionals_execF1940_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1935
// [0x00020002] 
struct UBioAutoConditionals_execF1935_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1934
// [0x00020002] 
struct UBioAutoConditionals_execF1934_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1933
// [0x00020002] 
struct UBioAutoConditionals_execF1933_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1932
// [0x00020002] 
struct UBioAutoConditionals_execF1932_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1931
// [0x00020002] 
struct UBioAutoConditionals_execF1931_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1930
// [0x00020002] 
struct UBioAutoConditionals_execF1930_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1929
// [0x00020002] 
struct UBioAutoConditionals_execF1929_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1928
// [0x00020002] 
struct UBioAutoConditionals_execF1928_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1927
// [0x00020002] 
struct UBioAutoConditionals_execF1927_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1926
// [0x00020002] 
struct UBioAutoConditionals_execF1926_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1925
// [0x00020002] 
struct UBioAutoConditionals_execF1925_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1924
// [0x00020002] 
struct UBioAutoConditionals_execF1924_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1923
// [0x00020002] 
struct UBioAutoConditionals_execF1923_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1922
// [0x00020002] 
struct UBioAutoConditionals_execF1922_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1921
// [0x00020002] 
struct UBioAutoConditionals_execF1921_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1920
// [0x00020002] 
struct UBioAutoConditionals_execF1920_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1919
// [0x00020002] 
struct UBioAutoConditionals_execF1919_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1918
// [0x00020002] 
struct UBioAutoConditionals_execF1918_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1917
// [0x00020002] 
struct UBioAutoConditionals_execF1917_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1916
// [0x00020002] 
struct UBioAutoConditionals_execF1916_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1915
// [0x00020002] 
struct UBioAutoConditionals_execF1915_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1914
// [0x00020002] 
struct UBioAutoConditionals_execF1914_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1913
// [0x00020002] 
struct UBioAutoConditionals_execF1913_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1912
// [0x00020002] 
struct UBioAutoConditionals_execF1912_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1911
// [0x00020002] 
struct UBioAutoConditionals_execF1911_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1910
// [0x00020002] 
struct UBioAutoConditionals_execF1910_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1909
// [0x00020002] 
struct UBioAutoConditionals_execF1909_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1908
// [0x00020002] 
struct UBioAutoConditionals_execF1908_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1907
// [0x00020002] 
struct UBioAutoConditionals_execF1907_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1906
// [0x00020002] 
struct UBioAutoConditionals_execF1906_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1905
// [0x00020002] 
struct UBioAutoConditionals_execF1905_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1904
// [0x00020002] 
struct UBioAutoConditionals_execF1904_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1903
// [0x00020002] 
struct UBioAutoConditionals_execF1903_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1902
// [0x00020002] 
struct UBioAutoConditionals_execF1902_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1901
// [0x00020002] 
struct UBioAutoConditionals_execF1901_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1900
// [0x00020002] 
struct UBioAutoConditionals_execF1900_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1899
// [0x00020002] 
struct UBioAutoConditionals_execF1899_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1898
// [0x00020002] 
struct UBioAutoConditionals_execF1898_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1897
// [0x00020002] 
struct UBioAutoConditionals_execF1897_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1896
// [0x00020002] 
struct UBioAutoConditionals_execF1896_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1895
// [0x00020002] 
struct UBioAutoConditionals_execF1895_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1894
// [0x00020002] 
struct UBioAutoConditionals_execF1894_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1888
// [0x00020002] 
struct UBioAutoConditionals_execF1888_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1986
// [0x00020002] 
struct UBioAutoConditionals_execF1986_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1998
// [0x00020002] 
struct UBioAutoConditionals_execF1998_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1997
// [0x00020002] 
struct UBioAutoConditionals_execF1997_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1996
// [0x00020002] 
struct UBioAutoConditionals_execF1996_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1995
// [0x00020002] 
struct UBioAutoConditionals_execF1995_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1994
// [0x00020002] 
struct UBioAutoConditionals_execF1994_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1971
// [0x00020002] 
struct UBioAutoConditionals_execF1971_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F2001
// [0x00020002] 
struct UBioAutoConditionals_execF2001_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1887
// [0x00020002] 
struct UBioAutoConditionals_execF1887_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1891
// [0x00020002] 
struct UBioAutoConditionals_execF1891_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1890
// [0x00020002] 
struct UBioAutoConditionals_execF1890_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1892
// [0x00020002] 
struct UBioAutoConditionals_execF1892_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1885
// [0x00020002] 
struct UBioAutoConditionals_execF1885_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1882
// [0x00020002] 
struct UBioAutoConditionals_execF1882_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1881
// [0x00020002] 
struct UBioAutoConditionals_execF1881_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1880
// [0x00020002] 
struct UBioAutoConditionals_execF1880_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1879
// [0x00020002] 
struct UBioAutoConditionals_execF1879_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1878
// [0x00020002] 
struct UBioAutoConditionals_execF1878_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1877
// [0x00020002] 
struct UBioAutoConditionals_execF1877_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1876
// [0x00020002] 
struct UBioAutoConditionals_execF1876_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1875
// [0x00020002] 
struct UBioAutoConditionals_execF1875_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1874
// [0x00020002] 
struct UBioAutoConditionals_execF1874_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1873
// [0x00020002] 
struct UBioAutoConditionals_execF1873_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1872
// [0x00020002] 
struct UBioAutoConditionals_execF1872_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1871
// [0x00020002] 
struct UBioAutoConditionals_execF1871_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1870
// [0x00020002] 
struct UBioAutoConditionals_execF1870_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1869
// [0x00020002] 
struct UBioAutoConditionals_execF1869_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1868
// [0x00020002] 
struct UBioAutoConditionals_execF1868_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1867
// [0x00020002] 
struct UBioAutoConditionals_execF1867_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1866
// [0x00020002] 
struct UBioAutoConditionals_execF1866_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F2003
// [0x00020002] 
struct UBioAutoConditionals_execF2003_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1972
// [0x00020002] 
struct UBioAutoConditionals_execF1972_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1981
// [0x00020002] 
struct UBioAutoConditionals_execF1981_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1980
// [0x00020002] 
struct UBioAutoConditionals_execF1980_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1939
// [0x00020002] 
struct UBioAutoConditionals_execF1939_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1884
// [0x00020002] 
struct UBioAutoConditionals_execF1884_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1883
// [0x00020002] 
struct UBioAutoConditionals_execF1883_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1886
// [0x00020002] 
struct UBioAutoConditionals_execF1886_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1865
// [0x00020002] 
struct UBioAutoConditionals_execF1865_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1864
// [0x00020002] 
struct UBioAutoConditionals_execF1864_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1863
// [0x00020002] 
struct UBioAutoConditionals_execF1863_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1862
// [0x00020002] 
struct UBioAutoConditionals_execF1862_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1861
// [0x00020002] 
struct UBioAutoConditionals_execF1861_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1860
// [0x00020002] 
struct UBioAutoConditionals_execF1860_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1859
// [0x00020002] 
struct UBioAutoConditionals_execF1859_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1858
// [0x00020002] 
struct UBioAutoConditionals_execF1858_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1857
// [0x00020002] 
struct UBioAutoConditionals_execF1857_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1856
// [0x00020002] 
struct UBioAutoConditionals_execF1856_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1855
// [0x00020002] 
struct UBioAutoConditionals_execF1855_Parms
{
	class ABioWorldInfo*                               BioWorld;                                         		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	int                                                Argument;                                         		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	unsigned long                                      ReturnValue : 1;                                  		// 0x0008 (0x0004) [0x0000000000000580] [0x00000001] ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
	// class UBioGlobalVariableTable*                  gv;                                               		// 0x000C (0x0004) [0x0000000000000000]              
};


#ifdef _MSC_VER
	#pragma pack ( pop )
#endif