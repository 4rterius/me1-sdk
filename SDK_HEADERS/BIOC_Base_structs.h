/*
#############################################################################################
# Mass Effect (2007) (1.2.20608.0) SDK
# Generated with TheFeckless UE3 SDK Generator v1.4_Beta-Rev.51
# ========================================================================================= #
# File: BIOC_Base_structs.h
# ========================================================================================= #
# Credits: uNrEaL, Tamimego, SystemFiles, R00T88, _silencer, the1domo, K@N@VEL
# Thanks: HOOAH07, lowHertz
# Forums: www.uc-forum.com, www.gamedeception.net
#############################################################################################
*/

#ifdef _MSC_VER
	#pragma pack ( push, 0x4 )
#endif

/*
# ========================================================================================= #
# Script Structs
# ========================================================================================= #
*/

// ScriptStruct BIOC_Base.BioDeathVFXCore.BioDeathVFXSpec
// 0x0028
struct FBioDeathVFXSpec
{
	struct FName                                       m_nmLabel;                                        		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	class UBioVFXTemplate*                             m_pExplosionVFXTemplate;                          		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class UBioVFXTemplate*                             m_pCrustVFXTemplate;                              		// 0x000C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      m_bIsTargetDeletedOnCrustEffectCompletion : 1;    		// 0x0010 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	unsigned long                                      m_bIsImpulseEnabled : 1;                          		// 0x0010 (0x0004) [0x0000000000000001] [0x00000002] ( CPF_Edit )
	float                                              m_fImpulseRadius;                                 		// 0x0014 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              m_fImpulseMagnitude;                              		// 0x0018 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	struct FVector                                     m_vImpulseOriginOffset;                           		// 0x001C (0x000C) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioBaseAppearance.MorphFeatureModelType
// 0x0010
struct FMorphFeatureModelType
{
	struct FName                                       sFeatureName;                                     		// 0x0000 (0x0008) [0x0000000000000000]              
	class USkeletalMeshComponent*                      MinModel;                                         		// 0x0008 (0x0004) [0x0000000004080008]              ( CPF_ExportObject | CPF_Component | CPF_EditInline )
	class USkeletalMeshComponent*                      MaxModel;                                         		// 0x000C (0x0004) [0x0000000004080008]              ( CPF_ExportObject | CPF_Component | CPF_EditInline )
};

// ScriptStruct BIOC_Base.BioBaseAppearance.MorphFeatureType
// 0x0008
struct FMorphFeatureType
{
	struct FName                                       sFeatureName;                                     		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.Bio_Appr_Character_HeadGear_Settings.BioHeadGearComponentSettings
// 0x000C
struct FBioHeadGearComponentSettings
{
	int                                                m_nMeshIndex;                                     		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	int                                                m_nMaterialIndex;                                 		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      m_bUseBodyMaterialConfig : 1;                     		// 0x0008 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	unsigned long                                      m_bIsHidden : 1;                                  		// 0x0008 (0x0004) [0x0000000000000001] [0x00000002] ( CPF_Edit )
	unsigned long                                      m_bIsLoaded : 1;                                  		// 0x0008 (0x0004) [0x0000000000000001] [0x00000004] ( CPF_Edit )
};

// ScriptStruct BIOC_Base.Bio_Appr_Character.FootStepAttachment
// 0x000C
struct FFootStepAttachment
{
	struct FName                                       BoneName;                                         		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	float                                              MinPeriodTime;                                    		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.Bio_Appr_Character.LookAtBoneDef
// 0x0024
struct FLookAtBoneDef
{
	struct FName                                       m_nBoneName;                                      		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	float                                              m_fLimit;                                         		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              m_fUpDownLimit;                                   		// 0x000C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              m_fDelay;                                         		// 0x0010 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned char                                      m_nLookAxis;                                      		// 0x0014 (0x0001) [0x0000000000000001]              ( CPF_Edit )
	unsigned char                                      m_nUpAxis;                                        		// 0x0015 (0x0001) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      m_bSeparateUpDownLimit : 1;                       		// 0x0018 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	unsigned long                                      m_bUseUpAxis : 1;                                 		// 0x0018 (0x0004) [0x0000000000000001] [0x00000002] ( CPF_Edit )
	unsigned long                                      m_bLookAtInverted : 1;                            		// 0x0018 (0x0004) [0x0000000000000001] [0x00000004] ( CPF_Edit )
	unsigned long                                      m_bUpAxisInverted : 1;                            		// 0x0018 (0x0004) [0x0000000000000001] [0x00000008] ( CPF_Edit )
	float                                              m_fSpeedFactor;                                   		// 0x001C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              m_fConversationStrength;                          		// 0x0020 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.Bio_Appr_Character_Body.ModelVariation
// 0x000C
struct FModelVariation
{
	int                                                NumVariations;                                    		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	int                                                MaterialsPerVariation;                            		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      m_bHasHeadGear : 1;                               		// 0x0008 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
};

// ScriptStruct BIOC_Base.Bio_Appr_Character_Body.ArmorTypes
// 0x0024
struct FArmorTypes
{
	unsigned char                                      ArmorType;                                        		// 0x0000 (0x0001) [0x0000000000020003]              ( CPF_Edit | CPF_Const | CPF_EditConst )
	struct FName                                       m_meshPackageName;                                		// 0x0004 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	struct FName                                       m_materialPackageName;                            		// 0x000C (0x0008) [0x0000000000000001]              ( CPF_Edit )
	TArray< struct FModelVariation >                   Variations;                                       		// 0x0014 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	class UPhysicsAsset*                               ArmorPhysicsAsset;                                		// 0x0020 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.Bio_Appr_Character_Body.WeaponAnimSpec
// 0x0014
struct FWeaponAnimSpec
{
	unsigned char                                      m_weaponType;                                     		// 0x0000 (0x0001) [0x0000000000020003]              ( CPF_Edit | CPF_Const | CPF_EditConst )
	TArray< class UAnimSet* >                          m_animSets;                                       		// 0x0004 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	class UAnimSet*                                    m_drawAnimSet;                                    		// 0x0010 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.Bio_Appr_Character_Body.OverrideAnimTreeTemplate
// 0x0008
struct FOverrideAnimTreeTemplate
{
	unsigned char                                      eClassification;                                  		// 0x0000 (0x0001) [0x0000000000020001]              ( CPF_Edit | CPF_EditConst )
	class UAnimTree*                                   AnimTreeTemplate;                                 		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.Bio_Appr_Character_Head.BioCharacterHeadAppearanceMaterialConfig
// 0x000C
struct FBioCharacterHeadAppearanceMaterialConfig
{
	TArray< class UMaterialInterface* >                m_aMaterials;                                     		// 0x0000 (0x000C) [0x0000000000400009]              ( CPF_Edit | CPF_ExportObject | CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.Bio_Appr_Character_Head.BioWrinkleConfig
// 0x0010
struct FBioWrinkleConfig
{
	struct FString                                     WrinkleParameterName;                             		// 0x0000 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	class UTexture2D*                                  WrinkleTexture;                                   		// 0x000C (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.Bio_Appr_Character_HeadGear.BioFacePlateMeshSpec
// 0x0008
struct FBioFacePlateMeshSpec
{
	unsigned long                                      m_bHidesVisor : 1;                                		// 0x0000 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	class USkeletalMesh*                               m_pMesh;                                          		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.Bio_Appr_Character_HeadGear.BioHeadGearAppearanceModelSpec
// 0x000C
struct FBioHeadGearAppearanceModelSpec
{
	int                                                m_nMaterialConfigCount;                           		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	int                                                m_nMaterialCountPerConfig;                        		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      m_bIsHairHidden : 1;                              		// 0x0008 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	unsigned long                                      m_bIsHeadHidden : 1;                              		// 0x0008 (0x0004) [0x0000000000000001] [0x00000002] ( CPF_Edit )
	unsigned long                                      m_bSuppressFacePlate : 1;                         		// 0x0008 (0x0004) [0x0000000000000001] [0x00000004] ( CPF_Edit )
	unsigned long                                      m_bSuppressVisor : 1;                             		// 0x0008 (0x0004) [0x0000000000000001] [0x00000008] ( CPF_Edit )
};

// ScriptStruct BIOC_Base.Bio_Appr_Character_HeadGear.BioHeadGearAppearanceArmorSpec
// 0x0018
struct FBioHeadGearAppearanceArmorSpec
{
	unsigned char                                      m_eArmorType;                                     		// 0x0000 (0x0001) [0x0000000000020003]              ( CPF_Edit | CPF_Const | CPF_EditConst )
	TArray< struct FBioHeadGearAppearanceModelSpec >   m_aModelSpec;                                     		// 0x0004 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	struct FName                                       m_nmPackage;                                      		// 0x0010 (0x0008) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioBaseAppearancePlaceable.AudioAPLStatePair
// 0x000C
struct FAudioAPLStatePair
{
	struct FName                                       m_nmAPLState;                                     		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	class USoundCue*                                   m_SoundCue;                                       		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioBaseAppearancePlaceable.VFXAPLStatePair
// 0x0009
struct FVFXAPLStatePair
{
	struct FName                                       m_nmAPLState;                                     		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	unsigned char                                      m_VFXState;                                       		// 0x0008 (0x0001) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioBaseAppearancePlaceable.VisualEffectAttachment
// 0x0018
struct FVisualEffectAttachment
{
	class UBioVFXTemplate*                             m_oEffectTemplate;                                		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	struct FName                                       m_nmSocket;                                       		// 0x0004 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	TArray< struct FVFXAPLStatePair >                  m_States;                                         		// 0x000C (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioBaseAppearancePlaceable.StaticAttachments
// 0x000C
struct FStaticAttachments
{
	struct FName                                       BoneName;                                         		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	class UStaticMesh*                                 StaticMesh;                                       		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioInventory.BioPlotPseudoItem
// 0x0018
struct FBioPlotPseudoItem
{
	void*                                              m_LocalizedName;                                  		// 0x0000 (0x0004) [0x0000000000000000]              
	void*                                              m_LocalizedDesc;                                  		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                m_exportID;                                       		// 0x0008 (0x0004) [0x0000000000000000]              
	int                                                m_basePrice;                                      		// 0x000C (0x0004) [0x0000000000000000]              
	int                                                m_shopGUIImageID;                                 		// 0x0010 (0x0004) [0x0000000000000000]              
	int                                                m_plotConditional;                                		// 0x0014 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGamePropertyTimer.BioGPTimingData
// 0x0010
struct FBioGPTimingData
{
	float                                              fTime;                                            		// 0x0000 (0x0004) [0x0000000000000000]              
	class UBioGameProperty*                            oGP;                                              		// 0x0004 (0x0004) [0x0000000000000000]              
	unsigned char                                      eTimingType;                                      		// 0x0008 (0x0001) [0x0000000000000000]              
	unsigned long                                      bFrameTicked : 1;                                 		// 0x000C (0x0004) [0x0000000000000000] [0x00000001] 
};

// ScriptStruct BIOC_Base.BioGamePropertyManager.BioGamePropertyManagerTimedOutTicked
// 0x0008
struct FBioGamePropertyManagerTimedOutTicked
{
	class UBioGameProperty*                            m_pGP;                                            		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              m_fTimeRemaining;                                 		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioAttributes.BioComplexFloatStructAttribute
// 0x005C
struct FBioComplexFloatStructAttribute
{
	float                                              m_Base;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              m_modifiers[ 0x4 ];                               		// 0x0004 (0x0010) [0x0000000000000000]              
	unsigned long                                      m_isBaseOverrideInEffect : 1;                     		// 0x0014 (0x0004) [0x0000000000000000] [0x00000001] 
	TArray< int >                                      m_baseOverrides;                                  		// 0x0018 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< unsigned long >                            m_isBaseOverrideValid;                            		// 0x0024 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	float                                              m_Current;                                        		// 0x0030 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_Dirty : 1;                                      		// 0x0034 (0x0004) [0x0000000000000000] [0x00000001] 
	float                                              m_Max;                                            		// 0x0038 (0x0004) [0x0000000000000000]              
	float                                              m_Min;                                            		// 0x003C (0x0004) [0x0000000000000000]              
	int                                                m_isModifierEnabled[ 0x4 ];                       		// 0x0040 (0x0010) [0x0000000000000000]              
	struct FPointer                                    m_Parent;                                         		// 0x0050 (0x0004) [0x0000000000001000]              ( CPF_Native )
	class UBio2DA*                                     m_LookupTable;                                    		// 0x0054 (0x0004) [0x0000000000000000]              
	int                                                m_TableColumn;                                    		// 0x0058 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioAttributes.BioComplexIntStructAttribute
// 0x0074
struct FBioComplexIntStructAttribute
{
	int                                                m_Base;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                m_modifiers[ 0x4 ];                               		// 0x0004 (0x0010) [0x0000000000000000]              
	unsigned long                                      m_isBaseOverrideInEffect : 1;                     		// 0x0014 (0x0004) [0x0000000000000000] [0x00000001] 
	TArray< int >                                      m_baseOverrides;                                  		// 0x0018 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< unsigned long >                            m_isBaseOverrideValid;                            		// 0x0024 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	int                                                m_Current;                                        		// 0x0030 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_Dirty : 1;                                      		// 0x0034 (0x0004) [0x0000000000000000] [0x00000001] 
	int                                                m_Max;                                            		// 0x0038 (0x0004) [0x0000000000000000]              
	int                                                m_Min;                                            		// 0x003C (0x0004) [0x0000000000000000]              
	int                                                m_isModifierEnabled[ 0x4 ];                       		// 0x0040 (0x0010) [0x0000000000000000]              
	struct FPointer                                    m_Parent;                                         		// 0x0050 (0x0004) [0x0000000000001000]              ( CPF_Native )
	class UBio2DA*                                     m_LookupTable;                                    		// 0x0054 (0x0004) [0x0000000000000000]              
	int                                                m_TableColumn;                                    		// 0x0058 (0x0004) [0x0000000000000000]              
	TArray< struct FPointer >                          m_Children;                                       		// 0x005C (0x000C) [0x0000000000001000]              ( CPF_Native )
	TArray< struct FPointer >                          m_FloatChildren;                                  		// 0x0068 (0x000C) [0x0000000000001000]              ( CPF_Native )
};

// ScriptStruct BIOC_Base.BioActorBehavior.BioVOSettings
// 0x000C
struct FBioVOSettings
{
	struct FColor                                      cSubtitleColour;                                  		// 0x0000 (0x0004) [0x0000000000000000]              
	unsigned char                                      nSubtitleMode;                                    		// 0x0004 (0x0001) [0x0000000000000000]              
	unsigned long                                      bSuppressSubtitlesIfVO : 1;                       		// 0x0008 (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      bAlert : 1;                                       		// 0x0008 (0x0004) [0x0000000000000000] [0x00000002] 
};

// ScriptStruct BIOC_Base.BioActorBehavior.BioDamageReporter
// 0x001C
struct FBioDamageReporter
{
	float                                              fShieldDamage;                                    		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              fShieldDamageCapacity;                            		// 0x0004 (0x0004) [0x0000000000000000]              
	float                                              fHealthDamage;                                    		// 0x0008 (0x0004) [0x0000000000000000]              
	float                                              fToxicDamage;                                     		// 0x000C (0x0004) [0x0000000000000000]              
	float                                              fStabilityDamage;                                 		// 0x0010 (0x0004) [0x0000000000000000]              
	float                                              fDamageResistance;                                		// 0x0014 (0x0004) [0x0000000000000000]              
	float                                              fToxicResistance;                                 		// 0x0018 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioActorBehavior.BioActorBehaviorWeaponLOSCache
// 0x0034
struct FBioActorBehaviorWeaponLOSCache
{
	int                                                m_flags;                                          		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              m_fHitTarget_Time;                                		// 0x0004 (0x0004) [0x0000000000000000]              
	struct FVector                                     m_vHitTarget_Location;                            		// 0x0008 (0x000C) [0x0000000000000000]              
	class AActor*                                      m_oHitTarget_Actor;                               		// 0x0014 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bHitTarget_Result : 1;                          		// 0x0018 (0x0004) [0x0000000000000000] [0x00000001] 
	float                                              m_fHitLocation_Time;                              		// 0x001C (0x0004) [0x0000000000000000]              
	struct FVector                                     m_vHitLocation_Location;                          		// 0x0020 (0x000C) [0x0000000000000000]              
	class AActor*                                      m_oHitLocation_Actor;                             		// 0x002C (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bHitLocation_Result : 1;                        		// 0x0030 (0x0004) [0x0000000000000000] [0x00000001] 
};

// ScriptStruct BIOC_Base.BioActorBehavior.BioActorPawnBehaviorLOSCache
// 0x000C
struct FBioActorPawnBehaviorLOSCache
{
	TArray< struct FBioActorBehaviorWeaponLOSCache >   m_WeaponLOSCaches;                                		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioGamerProfile.GameOptions
// 0x0090
struct FGameOptions
{
	unsigned long                                      bYAxisInverted : 1;                               		// 0x0000 (0x0004) [0x0000000000000000] [0x00000001] 
	int                                                m_GammaPostProcessTemplate;                       		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                m_nCombatDifficulty;                              		// 0x0008 (0x0004) [0x0000000000000000]              
	int                                                m_nDialogMode;                                    		// 0x000C (0x0004) [0x0000000000000000]              
	int                                                m_nAutoLevelUp;                                   		// 0x0010 (0x0004) [0x0000000000000000]              
	int                                                m_nAutoEquip;                                     		// 0x0014 (0x0004) [0x0000000000000000]              
	int                                                m_nTutorialFlag;                                  		// 0x0018 (0x0004) [0x0000000000000000]              
	int                                                m_nSubTitles;                                     		// 0x001C (0x0004) [0x0000000000000000]              
	int                                                m_nAutoPauseEnemySighted;                         		// 0x0020 (0x0004) [0x0000000000000000]              
	int                                                m_nAutoPauseSquadMemberDown;                      		// 0x0024 (0x0004) [0x0000000000000000]              
	int                                                m_nMusicVolume;                                   		// 0x0028 (0x0004) [0x0000000000000000]              
	int                                                m_nFXVolume;                                      		// 0x002C (0x0004) [0x0000000000000000]              
	int                                                m_nDialogVolume;                                  		// 0x0030 (0x0004) [0x0000000000000000]              
	int                                                m_nSouthpawFlag;                                  		// 0x0034 (0x0004) [0x0000000000000000]              
	int                                                m_nTargetingAssistMode;                           		// 0x0038 (0x0004) [0x0000000000000000]              
	float                                              m_nTargetingAssistModifier;                       		// 0x003C (0x0004) [0x0000000000000000]              
	int                                                m_nHorizontalCombatSensitivity;                   		// 0x0040 (0x0004) [0x0000000000000000]              
	int                                                m_nVerticalCombatSensitivity;                     		// 0x0044 (0x0004) [0x0000000000000000]              
	int                                                m_nHorizontalExplorationSensitivity;              		// 0x0048 (0x0004) [0x0000000000000000]              
	int                                                m_nVerticalExplorationSensitivity;                		// 0x004C (0x0004) [0x0000000000000000]              
	int                                                m_nRumbleFlag;                                    		// 0x0050 (0x0004) [0x0000000000000000]              
	int                                                m_nAutoPauseBleedOut;                             		// 0x0054 (0x0004) [0x0000000000000000]              
	int                                                m_nMotionBlur;                                    		// 0x0058 (0x0004) [0x0000000000000000]              
	int                                                m_nFilmGrain;                                     		// 0x005C (0x0004) [0x0000000000000000]              
	int                                                m_nSquadPowerUse;                                 		// 0x0060 (0x0004) [0x0000000000000000]              
	int                                                m_nAutoSave;                                      		// 0x0064 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bHardwareMouseState : 1;                        		// 0x0068 (0x0004) [0x0000000000000000] [0x00000001] 
	int                                                m_nMouseSensePerc;                                		// 0x006C (0x0004) [0x0000000000000000]              
	int                                                m_nAnalogSensePerc;                               		// 0x0070 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bVSync : 1;                                     		// 0x0074 (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      m_bWindowModeState : 1;                           		// 0x0074 (0x0004) [0x0000000000000000] [0x00000002] 
	float                                              m_nGammaValue;                                    		// 0x0078 (0x0004) [0x0000000000000000]              
	int                                                m_nEffectsState;                                  		// 0x007C (0x0004) [0x0000000000000000]              
	int                                                m_nTextureDetailLevel;                            		// 0x0080 (0x0004) [0x0000000000000000]              
	int                                                m_nShaderDetailLevel;                             		// 0x0084 (0x0004) [0x0000000000000000]              
	int                                                m_nFilterLevel;                                   		// 0x0088 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_nShadowDetailLevel : 1;                         		// 0x008C (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      m_nHardwareSound : 1;                             		// 0x008C (0x0004) [0x0000000000000000] [0x00000002] 
	unsigned long                                      m_nCooldown : 1;                                  		// 0x008C (0x0004) [0x0000000000000000] [0x00000004] 
};

// ScriptStruct BIOC_Base.BioAnimNodeBlendByAction.BlendTime
// 0x0008
struct FBlendTime
{
	unsigned char                                      m_eAnimNode;                                      		// 0x0000 (0x0001) [0x0000000000020003]              ( CPF_Edit | CPF_Const | CPF_EditConst )
	float                                              m_fTime;                                          		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioAnimNodeBlendByAnimationStateTracking.BioAnimationNode
// 0x0014
struct FBioAnimationNode
{
	struct FName                                       nmState;                                          		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	struct FName                                       nmToState;                                        		// 0x0008 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	float                                              fBlendTime;                                       		// 0x0010 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioAnimNodeBlendMultiAdditive.BioChildActivateData
// 0x0010
struct FBioChildActivateData
{
	unsigned long                                      bApplyData : 1;                                   		// 0x0000 (0x0004) [0x0000000000000000] [0x00000001] 
	float                                              fFinalWeight;                                     		// 0x0004 (0x0004) [0x0000000000000000]              
	float                                              fRemainingTime;                                   		// 0x0008 (0x0004) [0x0000000000000000]              
	float                                              fTotalBlendTime;                                  		// 0x000C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioAnimNodeBlendPoseAndGestures.BioChildPinData
// 0x0020
struct FBioChildPinData
{
	float                                              fEndBlendStartTime;                               		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              fEndBlendDuration;                                		// 0x0004 (0x0004) [0x0000000000000000]              
	float                                              fEndTime;                                         		// 0x0008 (0x0004) [0x0000000000000000]              
	unsigned long                                      bPlayUntilNext : 1;                               		// 0x000C (0x0004) [0x0000000000000000] [0x00000001] 
	TArray< class UBioGestChainTree* >                 aChainedTrees;                                    		// 0x0010 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	unsigned long                                      bUseDynAnimSets : 1;                              		// 0x001C (0x0004) [0x0000000000000000] [0x00000001] 
};

// ScriptStruct BIOC_Base.BioAnimNodeBlendPoseAndGestures.BioEndBlendData
// 0x0008
struct FBioEndBlendData
{
	float                                              fEndBlendStartTime;                               		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              fEndBlendDuration;                                		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioAnimNodeBlendScalar.BioScalarBlendParams
// 0x000C
struct FBioScalarBlendParams
{
	float                                              Min;                                              		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              Peak;                                             		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              Max;                                              		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioAnimNodeBlendScalar.BioScalarPrecomputedValues
// 0x0008
struct FBioScalarPrecomputedValues
{
	float                                              fRangeLowerRatio;                                 		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              fRangeUpperRatio;                                 		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioAnimNodeBlendScalarBehavior.BioAnimScalarNodeChildDef
// 0x0014
struct FBioAnimScalarNodeChildDef
{
	struct FName                                       Name;                                             		// 0x0000 (0x0008) [0x0000000000000000]              
	struct FBioScalarBlendParams                       BlendParams;                                      		// 0x0008 (0x000C) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioAnimNodeBlendScalarBehavior.BioAnimScalarNodeBehaviorDef
// 0x0024
struct FBioAnimScalarNodeBehaviorDef
{
	TArray< struct FBioAnimScalarNodeChildDef >        Children;                                         		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	unsigned long                                      BlendInstant : 1;                                 		// 0x000C (0x0004) [0x0000000000000000] [0x00000001] 
	float                                              BlendPctPerSecond;                                		// 0x0010 (0x0004) [0x0000000000000000]              
	float                                              DefaultScalar;                                    		// 0x0014 (0x0004) [0x0000000000000000]              
	struct FString                                     Description;                                      		// 0x0018 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioAnimNodeBlendState.BioAnimBlendParams
// 0x000D
struct FBioAnimBlendParams
{
	TArray< float >                                    BlendToChildTimes;                                		// 0x0000 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	unsigned char                                      PlayMode;                                         		// 0x000C (0x0001) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioAnimNodeBlendStateBehavior.BioAnimStateNodeChildDef
// 0x001C
struct FBioAnimStateNodeChildDef
{
	struct FName                                       Name;                                             		// 0x0000 (0x0008) [0x0000000000000000]              
	float                                              DefaultWeight;                                    		// 0x0008 (0x0004) [0x0000000000000000]              
	struct FBioAnimBlendParams                         BlendParams;                                      		// 0x000C (0x0010) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioAnimNodeBlendStateBehavior.BioAnimStateNodeBehaviorDef
// 0x0018
struct FBioAnimStateNodeBehaviorDef
{
	TArray< struct FBioAnimStateNodeChildDef >         Children;                                         		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FString                                     Description;                                      		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioAnimNodeSequenceByBoneRotation.AnimByRotation
// 0x0014
struct FAnimByRotation
{
	struct FRotator                                    DesiredRotation;                                  		// 0x0000 (0x000C) [0x0000000000000001]              ( CPF_Edit )
	struct FName                                       AnimName;                                         		// 0x000C (0x0008) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioAppearanceItemSophisticated.BioAppearanceItemSophisticatedVariant
// 0x0020
struct FBioAppearanceItemSophisticatedVariant
{
	int                                                m_Label;                                          		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class UAnimSet*                                    m_oWeaponAnimSet;                                 		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class UAnimTree*                                   m_oWeaponAnimTree;                                		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class UPhysicsAsset*                               m_oPhysicsAsset;                                  		// 0x000C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class USkeletalMesh*                               m_oModelMesh;                                     		// 0x0010 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	TArray< class UMaterialInterface* >                m_aMaterials;                                     		// 0x0014 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioAppearanceItemSophisticated.WeaponEffects
// 0x0018
struct FWeaponEffects
{
	class UBioVISWeapon*                               VisualImpactSet;                                  		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class UBioVFXTemplate*                             vfxMuzzleFlash;                                   		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class UBioVFXTemplate*                             vfxMuzzleFlash2;                                  		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class UBioVFXTemplate*                             vfxTracer;                                        		// 0x000C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class UBioVFXTemplate*                             vfxCoolDown;                                      		// 0x0010 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class UBioVFXTemplate*                             vfxSabotage;                                      		// 0x0014 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioAppearanceItemWeapon.BioAppearanceItemWeaponVFXSpec
// 0x000C
struct FBioAppearanceItemWeaponVFXSpec
{
	int                                                m_type;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	unsigned char                                      m_Damage;                                         		// 0x0004 (0x0001) [0x0000000000002001]              ( CPF_Edit | CPF_Transient )
	class UBioWeaponVFXAppearance*                     m_appearance;                                     		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioAppearanceVehicle.BioVehicleAttachmentInfo
// 0x0018
struct FBioVehicleAttachmentInfo
{
	class UActorComponent*                             oComponentToAttach;                               		// 0x0000 (0x0004) [0x0000000004080009]              ( CPF_Edit | CPF_ExportObject | CPF_Component | CPF_EditInline )
	struct FName                                       nmAttachSocket;                                   		// 0x0004 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	TArray< class UMaterialInterface* >                aMeshMaterials;                                   		// 0x000C (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioAppearanceVehicle.BioVehicleTurretInfo
// 0x0028
struct FBioVehicleTurretInfo
{
	struct FName                                       nmAttachSocket;                                   		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	struct FName                                       nmTurretYawBone;                                  		// 0x0008 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	struct FName                                       nmTurretPitchBone;                                		// 0x0010 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	struct FName                                       nmYawController;                                  		// 0x0018 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	struct FName                                       nmPitchController;                                		// 0x0020 (0x0008) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioAppearanceVehicle.BioVehicleThrusterInfo
// 0x0008
struct FBioVehicleThrusterInfo
{
	struct FName                                       nmThrusterSocket;                                 		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioAppearanceVehicle.BioVehicleSoundEntityStateInfo
// 0x0018
struct FBioVehicleSoundEntityStateInfo
{
	struct FString                                     sStateName;                                       		// 0x0000 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	struct FString                                     sStateValue;                                      		// 0x000C (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioArtPlaceable.CoverLinkRecord
// 0x0008
struct FCoverLinkRecord
{
	class AActor*                                      oCover;                                           		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	int                                                nSlotIndex;                                       		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioArtPlaceableRoles.ArtPlaceableProperty
// 0x0018
struct FArtPlaceableProperty
{
	struct FString                                     sPropertyName;                                    		// 0x0000 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	struct FString                                     sPropertyValue;                                   		// 0x000C (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioArtPlaceableRoles.ArtPlaceableRole
// 0x0018
struct FArtPlaceableRole
{
	struct FString                                     sRoleName;                                        		// 0x0000 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	TArray< struct FArtPlaceableProperty >             aProperties;                                      		// 0x000C (0x000C) [0x0000000004400001]              ( CPF_Edit | CPF_NeedCtorLink | CPF_EditInline )
};

// ScriptStruct BIOC_Base.BioArtPlaceableType.CoverInformation
// 0x002C
struct FCoverInformation
{
	unsigned char                                      CoverType;                                        		// 0x0000 (0x0001) [0x0000000000000001]              ( CPF_Edit )
	unsigned char                                      CoverDirection;                                   		// 0x0001 (0x0001) [0x0000000000000001]              ( CPF_Edit )
	float                                              fStepLeftDistance;                                		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fStepRightDistance;                               		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      bEnabled : 1;                                     		// 0x000C (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	unsigned long                                      bOverridePosition : 1;                            		// 0x000C (0x0004) [0x0000000000000001] [0x00000002] ( CPF_Edit )
	unsigned long                                      bOverrideRotation : 1;                            		// 0x000C (0x0004) [0x0000000000000001] [0x00000004] ( CPF_Edit )
	struct FVector                                     vOverridenPosition;                               		// 0x0010 (0x000C) [0x0000000000000001]              ( CPF_Edit )
	struct FRotator                                    rOverridenRotation;                               		// 0x001C (0x000C) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      bDisableWhenOnTop : 1;                            		// 0x0028 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioBaseActivity.ActivityProp
// 0x000C
struct FActivityProp
{
	class UStaticMesh*                                 m_oStaticMesh;                                    		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	struct FName                                       m_nAttachlocation;                                		// 0x0004 (0x0008) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioSquadAPI.SLocation
// 0x0010
struct FSLocation
{
	int                                                nNode;                                            		// 0x0000 (0x0004) [0x0000000000000000]              
	struct FVector                                     vPoint;                                           		// 0x0004 (0x000C) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioBaseSquad.MemberData
// 0x0070
struct FMemberData
{
	class APawn*                                       SquadMember;                                      		// 0x0000 (0x0004) [0x0000000000000000]              
	struct FSLocation                                  EscapeLocation;                                   		// 0x0004 (0x0010) [0x0000000000000000]              
	class ABioTacticalMoveToIndicator*                 oDestinationIndicator;                            		// 0x0014 (0x0004) [0x0000000000000000]              
	unsigned long                                      bMoveOrderAssigned : 1;                           		// 0x0018 (0x0004) [0x0000000000000000] [0x00000001] 
	struct FVector                                     vMoveLocation;                                    		// 0x001C (0x000C) [0x0000000000000000]              
	float                                              fMoveDelayTime;                                   		// 0x0028 (0x0004) [0x0000000000000000]              
	unsigned long                                      bCurrentlyMoving : 1;                             		// 0x002C (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      bCoverAssigned : 1;                               		// 0x002C (0x0004) [0x0000000000000000] [0x00000002] 
	struct FCoverRecord                                stCoverToUse;                                     		// 0x0030 (0x0014) [0x0000000000000000]              
	unsigned long                                      bActionOrderAssigned : 1;                         		// 0x0044 (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned char                                      eAction;                                          		// 0x0048 (0x0001) [0x0000000000000000]              
	unsigned char                                      eSwitchWeapon;                                    		// 0x0049 (0x0001) [0x0000000000000000]              
	unsigned char                                      eFormation;                                       		// 0x004A (0x0001) [0x0000000000000000]              
	struct FName                                       nmPower;                                          		// 0x004C (0x0008) [0x0000000000000000]              
	struct FVector                                     vTarget;                                          		// 0x0054 (0x000C) [0x0000000000000000]              
	struct FVector                                     vHoldPosition;                                    		// 0x0060 (0x000C) [0x0000000000000000]              
	class APawn*                                       oOrderedAttackTarget;                             		// 0x006C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioBaseSquad.PathNodeData
// 0x0030
struct FPathNodeData
{
	unsigned char                                      NodeAction;                                       		// 0x0000 (0x0001) [0x0000000000000000]              
	struct FName                                       NodeActionOn;                                     		// 0x0004 (0x0008) [0x0000000000000000]              
	struct FVector                                     vPathNode;                                        		// 0x000C (0x000C) [0x0000000000000000]              
	struct FRotator                                    rRotation;                                        		// 0x0018 (0x000C) [0x0000000000000000]              
	struct FRotator                                    rDirectionOfTravel;                               		// 0x0024 (0x000C) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioBaseSquad.StrategyChoice
// 0x0010
struct FStrategyChoice
{
	struct FName                                       StateName;                                        		// 0x0000 (0x0008) [0x0000000000020001]              ( CPF_Edit | CPF_EditConst )
	float                                              MaxLikelihood;                                    		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              CurrentLikelihood;                                		// 0x000C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioBaseSquad.CapacityFilteringRecord
// 0x000C
struct FCapacityFilteringRecord
{
	unsigned char                                      nCapMode;                                         		// 0x0000 (0x0001) [0x0000000000000003]              ( CPF_Edit | CPF_Const )
	struct FName                                       nmTechnique;                                      		// 0x0004 (0x0008) [0x0000000000000003]              ( CPF_Edit | CPF_Const )
};

// ScriptStruct BIOC_Base.BioCameraUtility.BioCameraUtilityOrientation
// 0x0008
struct FBioCameraUtilityOrientation
{
	float                                              m_yaw;                                            		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              m_pitch;                                          		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioCameraBehavior.BioCameraUtilityStickInputSpec
// 0x0008
struct FBioCameraUtilityStickInputSpec
{
	float                                              m_result;                                         		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              m_rawMax;                                         		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioProceduralCameraBasic.ActorInfo
// 0x0054
struct FActorInfo
{
	struct FVector                                     myPosition;                                       		// 0x0000 (0x000C) [0x0000000000000000]              
	struct FRotator                                    myRotation;                                       		// 0x000C (0x000C) [0x0000000000000000]              
	struct FVector                                     headBaseBonePosition;                             		// 0x0018 (0x000C) [0x0000000000000000]              
	struct FVector                                     headBonePosition;                                 		// 0x0024 (0x000C) [0x0000000000000000]              
	struct FVector                                     vCameraFocusPoint;                                		// 0x0030 (0x000C) [0x0000000000000000]              
	struct FVector                                     vProceduralCameraPosition;                        		// 0x003C (0x000C) [0x0000000000000000]              
	struct FRotator                                    rProceduralCameraRotation;                        		// 0x0048 (0x000C) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioProceduralCameraBasic.CameraInfo
// 0x0020
struct FCameraInfo
{
	struct FVector                                     vPosition;                                        		// 0x0000 (0x000C) [0x0000000000000000]              
	struct FRotator                                    rRotation;                                        		// 0x000C (0x000C) [0x0000000000000000]              
	float                                              fFov;                                             		// 0x0018 (0x0004) [0x0000000000000000]              
	float                                              fNearPlane;                                       		// 0x001C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioCameraZoom.BioZoomFocusConfig
// 0x0028
struct FBioZoomFocusConfig
{
	float                                              m_fNearClipFactor;                                		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              m_fNearClipMax;                                   		// 0x0004 (0x0004) [0x0000000000000000]              
	float                                              m_fMinRate;                                       		// 0x0008 (0x0004) [0x0000000000000000]              
	float                                              m_fDuration;                                      		// 0x000C (0x0004) [0x0000000000000000]              
	float                                              m_fInnerRadiusFactor;                             		// 0x0010 (0x0004) [0x0000000000000000]              
	float                                              m_fFalloffExponent;                               		// 0x0014 (0x0004) [0x0000000000000000]              
	float                                              m_fBlurKernelSize;                                		// 0x0018 (0x0004) [0x0000000000000000]              
	float                                              m_fMaxNearBlurAmount;                             		// 0x001C (0x0004) [0x0000000000000000]              
	float                                              m_fMaxFarBlurAmount;                              		// 0x0020 (0x0004) [0x0000000000000000]              
	struct FColor                                      m_clrModulateBlur;                                		// 0x0024 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioCameraZoom.BioZoomMagnificationConfig
// 0x0010
struct FBioZoomMagnificationConfig
{
	float                                              m_fCamSpeedFactor;                                		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              m_fFOVFactor;                                     		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                m_nLevelCount;                                    		// 0x0008 (0x0004) [0x0000000000000000]              
	float                                              m_fTransitionDuration;                            		// 0x000C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioCameraBehaviorGalaxy.BioMassRelayLine
// 0x006C
struct FBioMassRelayLine
{
	int                                                m_nStartClusterIdx;                               		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	int                                                m_nEndClusterIdx;                                 		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	struct FString                                     m_sStartClusterLabel;                             		// 0x0008 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	struct FString                                     m_sEndClusterLabel;                               		// 0x0014 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	struct FVector                                     m_vLeftEndPosition;                               		// 0x0020 (0x000C) [0x0000000000000001]              ( CPF_Edit )
	struct FVector                                     m_vRightEndPosition;                              		// 0x002C (0x000C) [0x0000000000000001]              ( CPF_Edit )
	struct FVector                                     m_vMiddlePosition;                                		// 0x0038 (0x000C) [0x0000000000000001]              ( CPF_Edit )
	struct FVector                                     m_vDrawScale;                                     		// 0x0044 (0x000C) [0x0000000000000001]              ( CPF_Edit )
	struct FRotator                                    m_rOrientation;                                   		// 0x0050 (0x000C) [0x0000000000000001]              ( CPF_Edit )
	class AActor*                                      m_pLeftEndActor;                                  		// 0x005C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class AActor*                                      m_pRighEndActor;                                  		// 0x0060 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class AActor*                                      m_pMiddleActor;                                   		// 0x0064 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      m_bIsGlowing : 1;                                 		// 0x0068 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioCameraManager.BioCameraManagerBehaviorSpec
// 0x0010
struct FBioCameraManagerBehaviorSpec
{
	class UBioCameraBehavior*                          m_pBehavior;                                      		// 0x0000 (0x0004) [0x0000000000000000]              
	TArray< class UBioCameraBehavior* >                m_apToBehaviorsNotPreservingTargetPoint;          		// 0x0004 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioCameraManager.BioCameraManagerTraceInfo
// 0x0024
struct FBioCameraManagerTraceInfo
{
	struct FVector                                     m_vCollVectorLocation;                            		// 0x0000 (0x000C) [0x0000000000000000]              
	struct FVector                                     m_vCollVectorNormal;                              		// 0x000C (0x000C) [0x0000000000000000]              
	class AActor*                                      m_oCollVectorActor;                               		// 0x0018 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bCollDetected : 1;                              		// 0x001C (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      m_bCollisionDirty : 1;                            		// 0x001C (0x0004) [0x0000000000000000] [0x00000002] 
	unsigned long                                      m_bDebugDraw : 1;                                 		// 0x001C (0x0004) [0x0000000000000000] [0x00000004] 
	struct FColor                                      m_clrDebugDraw;                                   		// 0x0020 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioCameraManager.BioCameraManagerTransitionSpec
// 0x0008
struct FBioCameraManagerTransitionSpec
{
	int                                                m_nBehaviorHandle;                                		// 0x0000 (0x0004) [0x0000000000000000]              
	class UBioCameraTransition*                        m_pTransition;                                    		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioCharacterImporter.BioIntArray
// 0x000C
struct FBioIntArray
{
	TArray< int >                                      m_a;                                              		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioCharacterImporter.PlayerInfo
// 0x0028
struct FPlayerInfo
{
	struct FString                                     m_Label;                                          		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	unsigned char                                      m_Gender;                                         		// 0x000C (0x0001) [0x0000000000000000]              
	struct FString                                     m_FirstName;                                      		// 0x0010 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	unsigned char                                      m_origin;                                         		// 0x001C (0x0001) [0x0000000000000000]              
	unsigned char                                      m_notoriety;                                      		// 0x001D (0x0001) [0x0000000000000000]              
	class UBioMorphFace*                               m_oMorph;                                         		// 0x0020 (0x0004) [0x0000000000000000]              
	int                                                nBonusTalentID;                                   		// 0x0024 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioCharacterImporter.BioClassGuiInfo
// 0x001C
struct FBioClassGuiInfo
{
	struct FString                                     sLabel;                                           		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	void*                                              srClassName;                                      		// 0x000C (0x0004) [0x0000000000000000]              
	void*                                              srDescription;                                    		// 0x0010 (0x0004) [0x0000000000000000]              
	void*                                              srPrimaryAbilityDescription;                      		// 0x0014 (0x0004) [0x0000000000000000]              
	void*                                              srSecondaryAbilityDescription;                    		// 0x0018 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioCharacterImporter.BioSpecializationData
// 0x0010
struct FBioSpecializationData
{
	int                                                m_bonusID;                                        		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                m_replaceTalentID;                                		// 0x0004 (0x0004) [0x0000000000000000]              
	void*                                              m_LocalizedName;                                  		// 0x0008 (0x0004) [0x0000000000000000]              
	void*                                              m_LocalizedDesc;                                  		// 0x000C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioInventoryGuiInterface.ResourceInfo
// 0x0008
struct FResourceInfo
{
	unsigned char                                      eType;                                            		// 0x0000 (0x0001) [0x0000000000000000]              
	float                                              fQty;                                             		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioItem.BioItemPropertyInfo
// 0x000C
struct FBioItemPropertyInfo
{
	void*                                              srName;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	void*                                              srDescription;                                    		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                nIconIndex;                                       		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioInventoryGuiInterface.BioInventoryGuiInterfaceItemStat
// 0x0009
struct FBioInventoryGuiInterfaceItemStat
{
	float                                              Current;                                          		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                nAbsolute;                                        		// 0x0004 (0x0004) [0x0000000000000000]              
	unsigned char                                      eType;                                            		// 0x0008 (0x0001) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioInventoryGuiInterface.BioInventoryGuiInterfaceItemStats
// 0x0030
struct FBioInventoryGuiInterfaceItemStats
{
	struct FBioInventoryGuiInterfaceItemStat           lstStats[ 0x3 ];                                  		// 0x0000 (0x0024) [0x0000000000000000]              
	TArray< struct FBioItemPropertyInfo >              lstModifiers;                                     		// 0x0024 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioInventoryGuiInterface.GuiEquipSlotDetails
// 0x0014
struct FGuiEquipSlotDetails
{
	TArray< void* >                                    lstStatLabels;                                    		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	void*                                              srSlotName;                                       		// 0x000C (0x0004) [0x0000000000000000]              
	int                                                nEmptyIcon;                                       		// 0x0010 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioInventoryGuiInterface.XModInfo
// 0x0018
struct FXModInfo
{
	class UBioItemXMod*                                m_oXMod;                                          		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                m_nType;                                          		// 0x0004 (0x0004) [0x0000000000000000]              
	struct FString                                     ItemName;                                         		// 0x0008 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	int                                                sophistication;                                   		// 0x0014 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioInventoryGuiInterface.ItemInfo
// 0x0090
struct FItemInfo
{
	struct FString                                     ItemName;                                         		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	void*                                              ItemCategoryDescription;                          		// 0x000C (0x0004) [0x0000000000000000]              
	void*                                              ItemCategoryName;                                 		// 0x0010 (0x0004) [0x0000000000000000]              
	int                                                nItemCategoryGuiIcon;                             		// 0x0014 (0x0004) [0x0000000000000000]              
	unsigned char                                      ItemSubcategoryRace;                              		// 0x0018 (0x0001) [0x0000000000000000]              
	void*                                              ItemSubcategoryRacialLabel;                       		// 0x001C (0x0004) [0x0000000000000000]              
	void*                                              ManufacturerName;                                 		// 0x0020 (0x0004) [0x0000000000000000]              
	void*                                              ManufacturerDescription;                          		// 0x0024 (0x0004) [0x0000000000000000]              
	int                                                nManufacturerIcon;                                		// 0x0028 (0x0004) [0x0000000000000000]              
	void*                                              BonusesDescription;                               		// 0x002C (0x0004) [0x0000000000000000]              
	int                                                sophistication;                                   		// 0x0030 (0x0004) [0x0000000000000000]              
	unsigned char                                      m_eSlot;                                          		// 0x0034 (0x0001) [0x0000000000000000]              
	struct FBioInventoryGuiInterfaceItemStats          m_rawStats;                                       		// 0x0038 (0x0030) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< struct FXModInfo >                         m_lstXMods;                                       		// 0x0068 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	float                                              m_price;                                          		// 0x0074 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bIsEquippable : 1;                              		// 0x0078 (0x0004) [0x0000000000000000] [0x00000001] 
	int                                                m_plotConditional;                                		// 0x007C (0x0004) [0x0000000000000000]              
	int                                                InvIndex;                                         		// 0x0080 (0x0004) [0x0000000000000000]              
	int                                                FilteredIndexOverride;                            		// 0x0084 (0x0004) [0x0000000000000000]              
	int                                                m_nmShopGUIImageID;                               		// 0x0088 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bIsNew : 1;                                     		// 0x008C (0x0004) [0x0000000000000000] [0x00000001] 
};

// ScriptStruct BIOC_Base.BioItem.BioItemPropertyStatus
// 0x0008
struct FBioItemPropertyStatus
{
	int                                                m_nID;                                            		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                m_nCount;                                         		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioItemSophisticated.BioManufacturerDetails
// 0x000C
struct FBioManufacturerDetails
{
	void*                                              srName;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	void*                                              srDescription;                                    		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                nIcon;                                            		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioItemXModdable.BioItemXModdableSlotSpec
// 0x0010
struct FBioItemXModdableSlotSpec
{
	int                                                m_type;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	TArray< class UBioItemXMod* >                      m_xMods;                                          		// 0x0004 (0x000C) [0x0000000004400008]              ( CPF_ExportObject | CPF_NeedCtorLink | CPF_EditInline )
};

// ScriptStruct BIOC_Base.BioItemXModdable.BioItemXModVirtualSlot
// 0x0008
struct FBioItemXModVirtualSlot
{
	int                                                m_type;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	class UBioItemXMod*                                m_oXMod;                                          		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioCheatManager.BioDebugMessage
// 0x0014
struct FBioDebugMessage
{
	struct FString                                     sMessage;                                         		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	float                                              fStartTime;                                       		// 0x000C (0x0004) [0x0000000000000000]              
	float                                              fDisplayTime;                                     		// 0x0010 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioCheatManager.PowerAimingInfo
// 0x0030
struct FPowerAimingInfo
{
	struct FVector                                     m_vStartLocation1;                                		// 0x0000 (0x000C) [0x0000000000000000]              
	struct FVector                                     m_vEndLocation1;                                  		// 0x000C (0x000C) [0x0000000000000000]              
	struct FVector                                     m_vStartLocation2;                                		// 0x0018 (0x000C) [0x0000000000000000]              
	struct FVector                                     m_vEndLocation2;                                  		// 0x0024 (0x000C) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioConversation.BioDialogReplyListDetails
// 0x0015
struct FBioDialogReplyListDetails
{
	int                                                nIndex;                                           		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	void*                                              srParaphrase;                                     		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	struct FString                                     sParaphrase;                                      		// 0x0008 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	unsigned char                                      Category;                                         		// 0x0014 (0x0001) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioConversation.BioDialogNode
// 0x0044
struct FBioDialogNode
{
	void*                                              srText;                                           		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	struct FString                                     sText;                                            		// 0x0004 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	unsigned long                                      bFireConditional : 1;                             		// 0x0010 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	int                                                nConditionalFunc;                                 		// 0x0014 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	int                                                nConditionalParam;                                		// 0x0018 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	int                                                nStateTransition;                                 		// 0x001C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	int                                                nStateTransitionParam;                            		// 0x0020 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	int                                                nExportID;                                        		// 0x0024 (0x0004) [0x0000000000020001]              ( CPF_Edit | CPF_EditConst )
	int                                                nScriptIndex;                                     		// 0x0028 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      bAmbient : 1;                                     		// 0x002C (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	unsigned long                                      bNonTextLine : 1;                                 		// 0x002C (0x0004) [0x0000000000000001] [0x00000002] ( CPF_Edit )
	class USoundCue*                                   pCue;                                             		// 0x0030 (0x0004) [0x0000000000002000]              ( CPF_Transient )
	unsigned long                                      bSoundLoaded : 1;                                 		// 0x0034 (0x0004) [0x0000000000002000] [0x00000001] ( CPF_Transient )
	unsigned char                                      eGUIStyle;                                        		// 0x0038 (0x0001) [0x0000000000000001]              ( CPF_Edit )
	int                                                nCameraIntimacy;                                  		// 0x003C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      bIgnoreBodyGestures : 1;                          		// 0x0040 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioConversation.BioDialogEntryNode
// 0x0024(0x0068 - 0x0044)
struct FBioDialogEntryNode : FBioDialogNode
{
	TArray< struct FBioDialogReplyListDetails >        ReplyListNew;                                     		// 0x0044 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	int                                                nSpeakerIndex;                                    		// 0x0050 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	int                                                nListenerIndex;                                   		// 0x0054 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      bSkippable : 1;                                   		// 0x0058 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	TArray< int >                                      aSpeakerList;                                     		// 0x005C (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioConversation.BioDialogReplyNode
// 0x0018(0x005C - 0x0044)
struct FBioDialogReplyNode : FBioDialogNode
{
	TArray< int >                                      EntryList;                                        		// 0x0044 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	unsigned char                                      ReplyType;                                        		// 0x0050 (0x0001) [0x0000000000000001]              ( CPF_Edit )
	int                                                nListenerIndex;                                   		// 0x0054 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      bUnskippable : 1;                                 		// 0x0058 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	unsigned long                                      bIllegal : 1;                                     		// 0x0058 (0x0004) [0x0000000000002000] [0x00000002] ( CPF_Transient )
};

// ScriptStruct BIOC_Base.BioConversation.BioDialogSpeaker
// 0x000C
struct FBioDialogSpeaker
{
	struct FName                                       sSpeakerTag;                                      		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	class AActor*                                      aSpeaker;                                         		// 0x0008 (0x0004) [0x0000000000002000]              ( CPF_Transient )
};

// ScriptStruct BIOC_Base.BioConversation.BioDialogLookat
// 0x000C
struct FBioDialogLookat
{
	class AActor*                                      pActor;                                           		// 0x0000 (0x0004) [0x0000000000002000]              ( CPF_Transient )
	float                                              fLookAtDelay;                                     		// 0x0004 (0x0004) [0x0000000000002000]              ( CPF_Transient )
	class AActor*                                      pLookAtTarget;                                    		// 0x0008 (0x0004) [0x0000000000002000]              ( CPF_Transient )
};

// ScriptStruct BIOC_Base.BioConversation.BioDialogScript
// 0x0008
struct FBioDialogScript
{
	struct FName                                       sScriptTag;                                       		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioConversation.BioSavedActorPos
// 0x001C
struct FBioSavedActorPos
{
	class AActor*                                      pActor;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	struct FVector                                     vPos;                                             		// 0x0004 (0x000C) [0x0000000000000000]              
	struct FRotator                                    rRot;                                             		// 0x0010 (0x000C) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioConversation.BioNextCamData
// 0x0038
struct FBioNextCamData
{
	float                                              fFov;                                             		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	struct FVector                                     vPos;                                             		// 0x0004 (0x000C) [0x0000000000000001]              ( CPF_Edit )
	struct FRotator                                    rRot;                                             		// 0x0010 (0x000C) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      bUseThis : 1;                                     		// 0x001C (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	float                                              fNearPlane;                                       		// 0x0020 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	struct FName                                       sCameraName;                                      		// 0x0024 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	struct FBioStageDOFData                            tDOFData;                                         		// 0x002C (0x000C) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioConvNodeTrack.BioTrackKey
// 0x000C
struct FBioTrackKey
{
	float                                              fTime;                                            		// 0x0000 (0x0004) [0x0000000000000000]              
	struct FName                                       KeyName;                                          		// 0x0004 (0x0008) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioDEBUGMarkerComponent.LineData
// 0x001B
struct FLineData
{
	struct FVector                                     Start;                                            		// 0x0000 (0x000C) [0x0000000000000000]              
	struct FVector                                     End;                                              		// 0x000C (0x000C) [0x0000000000000000]              
	unsigned char                                      R;                                                		// 0x0018 (0x0001) [0x0000000000000000]              
	unsigned char                                      G;                                                		// 0x0019 (0x0001) [0x0000000000000000]              
	unsigned char                                      B;                                                		// 0x001A (0x0001) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioDEBUGMarkerComponent.MarkerData
// 0x0013
struct FMarkerData
{
	struct FVector                                     Start;                                            		// 0x0000 (0x000C) [0x0000000000000000]              
	int                                                Size;                                             		// 0x000C (0x0004) [0x0000000000000000]              
	unsigned char                                      R;                                                		// 0x0010 (0x0001) [0x0000000000000000]              
	unsigned char                                      G;                                                		// 0x0011 (0x0001) [0x0000000000000000]              
	unsigned char                                      B;                                                		// 0x0012 (0x0001) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGUIBox.CanvasProperties
// 0x0020
struct FCanvasProperties
{
	class UFont*                                       m_font;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                m_nCurX;                                          		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                m_nCurY;                                          		// 0x0008 (0x0004) [0x0000000000000000]              
	int                                                m_nOrgX;                                          		// 0x000C (0x0004) [0x0000000000000000]              
	int                                                m_nOrgY;                                          		// 0x0010 (0x0004) [0x0000000000000000]              
	int                                                m_nClipX;                                         		// 0x0014 (0x0004) [0x0000000000000000]              
	int                                                m_nClipY;                                         		// 0x0018 (0x0004) [0x0000000000000000]              
	struct FColor                                      m_color;                                          		// 0x001C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioEffectsMaterialsPriorityMap.EffectMaterialPriority
// 0x0008
struct FEffectMaterialPriority
{
	unsigned char                                      eType;                                            		// 0x0000 (0x0001) [0x0000000000000000]              
	int                                                nPriority;                                        		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioEmissionAreaListBoneList.BoneAndWeight
// 0x000C
struct FBoneAndWeight
{
	struct FName                                       BoneName;                                         		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	float                                              BoneWeight;                                       		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioEmissionAreaListBoneList.BoneListEmissionArea
// 0x0018
struct FBoneListEmissionArea
{
	struct FName                                       AreaTag;                                          		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      UseNumVertsAsWeights : 1;                         		// 0x0008 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	TArray< struct FBoneAndWeight >                    Bones;                                            		// 0x000C (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioEpicPawnBehavior.EpicPawnVisualEffect
// 0x0014
struct FEpicPawnVisualEffect
{
	struct FName                                       m_nmLabel;                                        		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	class UBioVFXTemplate*                             m_oVFXTemplate;                                   		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	struct FName                                       m_nmTagOfActorAtVFXLocation;                      		// 0x000C (0x0008) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioEpicPawnBehavior.BioEpicPawnBehaviorWeaponCache
// 0x0058
struct FBioEpicPawnBehaviorWeaponCache
{
	class ABioWeaponRanged*                            m_weapon;                                         		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                m_flags;                                          		// 0x0004 (0x0004) [0x0000000000000000]              
	struct FVector                                     m_targetPoint;                                    		// 0x0008 (0x000C) [0x0000000000000000]              
	struct FVector                                     m_muzzleLocation;                                 		// 0x0014 (0x000C) [0x0000000000000000]              
	struct FVector                                     m_muzzleToTargetDisplacement;                     		// 0x0020 (0x000C) [0x0000000000000000]              
	struct FRotator                                    m_muzzleToTargetDirection;                        		// 0x002C (0x000C) [0x0000000000000000]              
	class AActor*                                      m_traceHitActor;                                  		// 0x0038 (0x0004) [0x0000000000000000]              
	struct FVector                                     m_traceHitLocation;                               		// 0x003C (0x000C) [0x0000000000000000]              
	struct FVector                                     m_traceHitNormal;                                 		// 0x0048 (0x000C) [0x0000000000000000]              
	float                                              m_traceHitDistance;                               		// 0x0054 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioEpicPawnBehavior.BioEpicPawnBehaviorCache
// 0x000C
struct FBioEpicPawnBehaviorCache
{
	TArray< struct FBioEpicPawnBehaviorWeaponCache >   m_weaponCaches;                                   		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioEquipmentBase.BioEquipmentBaseSlotType
// 0x001C
struct FBioEquipmentBaseSlotType
{
	class UBioItem*                                    oEquippedItem;                                    		// 0x0000 (0x0004) [0x0000000000000000]              
	struct FScriptDelegate                             EquipAction;                                      		// 0x0004 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FScriptDelegate                             UnEquipAction;                                    		// 0x0010 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioEventNotifier.BioDisplayNotice
// 0x002C
struct FBioDisplayNotice
{
	int                                                nEventType;                                       		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                nTimeToLive;                                      		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                nIconIndex;                                       		// 0x0008 (0x0004) [0x0000000000000000]              
	int                                                nContext;                                         		// 0x000C (0x0004) [0x0000000000000000]              
	void*                                              srTitle;                                          		// 0x0010 (0x0004) [0x0000000000000000]              
	struct FString                                     strTitle;                                         		// 0x0014 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	int                                                nQuantity;                                        		// 0x0020 (0x0004) [0x0000000000000000]              
	int                                                nQuantMin;                                        		// 0x0024 (0x0004) [0x0000000000000000]              
	int                                                nQuantMax;                                        		// 0x0028 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioEventNotifier.BioTalentNotice
// 0x000C
struct FBioTalentNotice
{
	int                                                nIcon;                                            		// 0x0000 (0x0004) [0x0000000000000000]              
	void*                                              srName;                                           		// 0x0004 (0x0004) [0x0000000000000000]              
	class ABioPawn*                                    oCharacter;                                       		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioEvtSysTrackDOF.BioDOFTrackData
// 0x0030
struct FBioDOFTrackData
{
	unsigned long                                      bEnableDOF : 1;                                   		// 0x0000 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	float                                              fFalloffExponent;                                 		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fBlurKernelSize;                                  		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fMaxNearBlurAmount;                               		// 0x000C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fMaxFarBlurAmount;                                		// 0x0010 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	struct FColor                                      cModulateBlurColor;                               		// 0x0014 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fFocusInnerRadius;                                		// 0x0018 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fFocusDistance;                                   		// 0x001C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	struct FVector                                     vFocusPosition;                                   		// 0x0020 (0x000C) [0x0000000000000001]              ( CPF_Edit )
	float                                              fInterpolateSeconds;                              		// 0x002C (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioEvtSysTrackFuncShot.BioFuncShotData
// 0x0038
struct FBioFuncShotData
{
	unsigned char                                      eShotType;                                        		// 0x0000 (0x0001) [0x0000000000000001]              ( CPF_Edit )
	struct FString                                     sActor1;                                          		// 0x0004 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	struct FString                                     sActor2;                                          		// 0x0010 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	struct FString                                     sStage;                                           		// 0x001C (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	int                                                nActor1PosNode;                                   		// 0x0028 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      bUseCamera : 1;                                   		// 0x002C (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	struct FName                                       nmShotName;                                       		// 0x0030 (0x0008) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioEvtSysTrackGesture.BioGestTrackPriority
// 0x0008
struct FBioGestTrackPriority
{
	int                                                nTrackIndex;                                      		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                nPriority;                                        		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioEvtSysTrackGesture.BioGestureRenameData
// 0x0018
struct FBioGestureRenameData
{
	struct FName                                       nmOldAnim;                                        		// 0x0000 (0x0008) [0x0000000000000000]              
	struct FName                                       nmNewSet;                                         		// 0x0008 (0x0008) [0x0000000000000000]              
	struct FName                                       nmNewAnim;                                        		// 0x0010 (0x0008) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioEvtSysTrackLookAt.BioLookAtTrackData
// 0x0020
struct FBioLookAtTrackData
{
	unsigned long                                      bEnabled : 1;                                     		// 0x0000 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	struct FName                                       nmTargetTag;                                      		// 0x0004 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	float                                              fDuration;                                        		// 0x000C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      bEnableEyes : 1;                                  		// 0x0010 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	unsigned long                                      bEnableHead : 1;                                  		// 0x0010 (0x0004) [0x0000000000000001] [0x00000002] ( CPF_Edit )
	unsigned long                                      bEnableTorso : 1;                                 		// 0x0010 (0x0004) [0x0000000000000001] [0x00000004] ( CPF_Edit )
	float                                              fEyeWeight;                                       		// 0x0014 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fHeadWeight;                                      		// 0x0018 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fTorsoWeight;                                     		// 0x001C (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioEvtSysTrackSetFacing.BioSetFacingData
// 0x0009
struct FBioSetFacingData
{
	unsigned long                                      bApplyOrientation : 1;                            		// 0x0000 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	float                                              fOrientation;                                     		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned char                                      eStageNode;                                       		// 0x0008 (0x0001) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioStage.BioStageCameraCustom
// 0x0028
struct FBioStageCameraCustom
{
	struct FString                                     m_sCameraName;                                    		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	float                                              m_fCameraHeightDelta;                             		// 0x000C (0x0004) [0x0000000000000000]              
	float                                              m_fCameraPitchDelta;                              		// 0x0010 (0x0004) [0x0000000000000000]              
	float                                              m_fCameraYawDelta;                                		// 0x0014 (0x0004) [0x0000000000000000]              
	float                                              m_fCameraFOVDelta;                                		// 0x0018 (0x0004) [0x0000000000000000]              
	float                                              m_fCameraNearPlaneDelta;                          		// 0x001C (0x0004) [0x0000000000000000]              
	float                                              m_fCameraFocusInnerRadiusDelta;                   		// 0x0020 (0x0004) [0x0000000000000000]              
	float                                              m_fCameraFocusDistanceDelta;                      		// 0x0024 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioEvtSysTrackSubtitles.BioSubtitleTrackData
// 0x0008
struct FBioSubtitleTrackData
{
	int                                                nStrRefID;                                        		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fLength;                                          		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioEvtSysTrackSwitchCamera.BioCameraSwitchData
// 0x0020
struct FBioCameraSwitchData
{
	unsigned long                                      bForceCrossingLineOfAction : 1;                   		// 0x0000 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	struct FName                                       nmExplicitCamera;                                 		// 0x0004 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	int                                                nIntimacy;                                        		// 0x000C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned char                                      eStandardCamera;                                  		// 0x0010 (0x0001) [0x0000000000000001]              ( CPF_Edit )
	unsigned char                                      eStageSpecificCam;                                		// 0x0011 (0x0001) [0x0000000000000001]              ( CPF_Edit )
	struct FName                                       nmStageSpecificCam;                               		// 0x0014 (0x0008) [0x0000000000000000]              
	unsigned long                                      bUseForNextCamera : 1;                            		// 0x001C (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioEvtSysTrackVOElements.FaceFXInfo
// 0x0020
struct FFaceFXInfo
{
	class ABioPawn*                                    Pawn;                                             		// 0x0000 (0x0004) [0x0000000000000000]              
	class UFaceFXAsset*                                Asset;                                            		// 0x0004 (0x0004) [0x0000000000000000]              
	class UFaceFXAnimSet*                              AnimSet;                                          		// 0x0008 (0x0004) [0x0000000000000000]              
	struct FString                                     AnimName;                                         		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	float                                              AnimPos;                                          		// 0x0018 (0x0004) [0x0000000000000000]              
	unsigned long                                      Dirty : 1;                                        		// 0x001C (0x0004) [0x0000000000000000] [0x00000001] 
};

// ScriptStruct BIOC_Base.BioEvtSysTrackVOElementsInst.BioVOActorType
// 0x0018
struct FBioVOActorType
{
	struct FString                                     sTag;                                             		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FString                                     sActorType;                                       		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioEvtSysTrackVOElementsInst.BioVOActorData
// 0x0028
struct FBioVOActorData
{
	class AActor*                                      pActor;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	unsigned long                                      bSpawned : 1;                                     		// 0x0004 (0x0004) [0x0000000000000000] [0x00000001] 
	struct FVector                                     vLastPos;                                         		// 0x0008 (0x000C) [0x0000000000000000]              
	struct FRotator                                    rLastRot;                                         		// 0x0014 (0x000C) [0x0000000000000000]              
	struct FName                                       nmStageNode;                                      		// 0x0020 (0x0008) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioForceFeedbackPlayer.BioForceFeedbackPlayerSpec
// 0x0008
struct FBioForceFeedbackPlayerSpec
{
	unsigned char                                      m_eLabel;                                         		// 0x0000 (0x0001) [0x0000000000020003]              ( CPF_Edit | CPF_Const | CPF_EditConst )
	class UForceFeedbackWaveform*                      m_pWaveform;                                      		// 0x0004 (0x0004) [0x000000000440000B]              ( CPF_Edit | CPF_Const | CPF_ExportObject | CPF_NeedCtorLink | CPF_EditInline )
};

// ScriptStruct BIOC_Base.BioForceFeedbackPlayer.BioForceFeedbackPlayerWeaponSpec
// 0x0008
struct FBioForceFeedbackPlayerWeaponSpec
{
	unsigned char                                      m_eLabel;                                         		// 0x0000 (0x0001) [0x0000000000020003]              ( CPF_Edit | CPF_Const | CPF_EditConst )
	class UForceFeedbackWaveform*                      m_pWaveform;                                      		// 0x0004 (0x0004) [0x000000000440000B]              ( CPF_Edit | CPF_Const | CPF_ExportObject | CPF_NeedCtorLink | CPF_EditInline )
};

// ScriptStruct BIOC_Base.BioFormations.FormationPosition
// 0x0034
struct FFormationPosition
{
	int                                                EscapeDistance;                                   		// 0x0000 (0x0004) [0x0000000000000000]              
	struct FVector                                     Formation;                                        		// 0x0004 (0x000C) [0x0000000000000000]              
	struct FRotator                                    facing;                                           		// 0x0010 (0x000C) [0x0000000000000000]              
	struct FVector                                     InvFormation;                                     		// 0x001C (0x000C) [0x0000000000000000]              
	struct FRotator                                    InvFacing;                                        		// 0x0028 (0x000C) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGameProperty.BioGamePropertyApplication
// 0x0008
struct FBioGamePropertyApplication
{
	class UObject*                                     m_pActualTarget;                                  		// 0x0000 (0x0004) [0x0000000000000000]              
	class UBioGameEffect*                              m_pGE;                                            		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGamePropertyEffectLevelIterator.BioEffectLevelIteratorPair
// 0x0010
struct FBioEffectLevelIteratorPair
{
	int                                                m_nGameProperty;                                  		// 0x0000 (0x0004) [0x0000000000000000]              
	TArray< int >                                      m_aGPEffectRows;                                  		// 0x0004 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioGamePropertyImporter.BioGPLoadData
// 0x0024
struct FBioGPLoadData
{
	class UObject*                                     oOwner;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	class UBioGamePropertyContainer*                   oContainer;                                       		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                nGamePropId;                                      		// 0x0008 (0x0004) [0x0000000000000000]              
	class UBio2DA*                                     oFXLevelTable;                                    		// 0x000C (0x0004) [0x0000000000000000]              
	int                                                m_nPowerLevel;                                    		// 0x0010 (0x0004) [0x0000000000000000]              
	TArray< int >                                      aEffectLevelRows;                                 		// 0x0014 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	int                                                m_nMaxRanks;                                      		// 0x0020 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGamerProfile.ProfileBOOL
// 0x0008
struct FProfileBOOL
{
	int                                                PMIndex;                                          		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                Value;                                            		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGamerProfile.ProfileINT
// 0x0008
struct FProfileINT
{
	int                                                PMIndex;                                          		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                Value;                                            		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGamerProfile.ProfileFLOAT
// 0x0008
struct FProfileFLOAT
{
	int                                                PMIndex;                                          		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              Value;                                            		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGamerProfile.PlotManagerAchievement
// 0x000C
struct FPlotManagerAchievement
{
	int                                                PMIndex;                                          		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                nAchievementID;                                   		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                UnlockedAt;                                       		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGamerProfile.ProfilePlaythrough
// 0x0008
struct FProfilePlaythrough
{
	int                                                nPlaythroughID;                                   		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                nDifficultySetting;                               		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGamerProfile.ProfileRewardStat
// 0x0020
struct FProfileRewardStat
{
	struct FString                                     sName;                                            		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	int                                                Value;                                            		// 0x000C (0x0004) [0x0000000000000000]              
	int                                                UnlockedAt;                                       		// 0x0010 (0x0004) [0x0000000000000000]              
	int                                                AchievementId;                                    		// 0x0014 (0x0004) [0x0000000000000000]              
	unsigned long                                      IsAchievementUnlocked : 1;                        		// 0x0018 (0x0004) [0x0000000000000000] [0x00000001] 
	int                                                TalenTreeID;                                      		// 0x001C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGamerProfile.BonusTalent
// 0x0008
struct FBonusTalent
{
	int                                                nAchievementID;                                   		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                nBonusTalentID;                                   		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGamerProfile.ItemAppearanceData
// 0x0010
struct FItemAppearanceData
{
	unsigned long                                      bEquipped : 1;                                    		// 0x0000 (0x0004) [0x0000000000000000] [0x00000001] 
	int                                                nItemID;                                          		// 0x0004 (0x0004) [0x0000000000000000]              
	unsigned char                                      eSophistication;                                  		// 0x0008 (0x0001) [0x0000000000000000]              
	int                                                nManufacturer;                                    		// 0x000C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGamerProfile.CharacterAppearanceData
// 0x0054
struct FCharacterAppearanceData
{
	struct FItemAppearanceData                         lstWeapons[ 0x4 ];                                		// 0x0000 (0x0040) [0x0000000000000000]              
	struct FItemAppearanceData                         stArmor;                                          		// 0x0040 (0x0010) [0x0000000000000000]              
	unsigned long                                      bHelmetVisible : 1;                               		// 0x0050 (0x0004) [0x0000000000000000] [0x00000001] 
};

// ScriptStruct BIOC_Base.BioGamerProfile.CharacterStatisticsData
// 0x0010
struct FCharacterStatisticsData
{
	int                                                m_Stamina;                                        		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                m_Focus;                                          		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                m_Precision;                                      		// 0x0008 (0x0004) [0x0000000000000000]              
	int                                                m_Coordination;                                   		// 0x000C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGamerProfile.CharacterProfile
// 0x00B4
struct FCharacterProfile
{
	struct FString                                     sCharacterID;                                     		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FString                                     sFullName;                                        		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	int                                                nLastPlayedSlot;                                  		// 0x0018 (0x0004) [0x0000000000000000]              
	struct FCharacterAppearanceData                    stCharacterAppearance;                            		// 0x001C (0x0054) [0x0000000000000000]              
	struct FCharacterStatisticsData                    stCharacterStatistics;                            		// 0x0070 (0x0010) [0x0000000000000000]              
	unsigned char                                      m_ClassBase;                                      		// 0x0080 (0x0001) [0x0000000000000000]              
	unsigned char                                      m_origin;                                         		// 0x0081 (0x0001) [0x0000000000000000]              
	unsigned char                                      m_Reputation;                                     		// 0x0082 (0x0001) [0x0000000000000000]              
	int                                                m_CharacterLevel;                                 		// 0x0084 (0x0004) [0x0000000000000000]              
	int                                                m_CreationYear;                                   		// 0x0088 (0x0004) [0x0000000000000000]              
	int                                                m_CreationMonth;                                  		// 0x008C (0x0004) [0x0000000000000000]              
	int                                                m_CreationDayOfWeek;                              		// 0x0090 (0x0004) [0x0000000000000000]              
	int                                                m_CreationDay;                                    		// 0x0094 (0x0004) [0x0000000000000000]              
	int                                                m_CreationHour;                                   		// 0x0098 (0x0004) [0x0000000000000000]              
	int                                                m_CreationMin;                                    		// 0x009C (0x0004) [0x0000000000000000]              
	int                                                m_CreationSec;                                    		// 0x00A0 (0x0004) [0x0000000000000000]              
	int                                                m_CreationMSec;                                   		// 0x00A4 (0x0004) [0x0000000000000000]              
	int                                                m_PlayedHours;                                    		// 0x00A8 (0x0004) [0x0000000000000000]              
	int                                                m_PlayedMin;                                      		// 0x00AC (0x0004) [0x0000000000000000]              
	int                                                m_PlayedSec;                                      		// 0x00B0 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGamerProfile.AchievementInfo
// 0x0018
struct FAchievementInfo
{
	int                                                nAchievementID;                                   		// 0x0000 (0x0004) [0x0000000000000000]              
	void*                                              srTitle;                                          		// 0x0004 (0x0004) [0x0000000000000000]              
	void*                                              srIncomplete;                                     		// 0x0008 (0x0004) [0x0000000000000000]              
	void*                                              srComplete;                                       		// 0x000C (0x0004) [0x0000000000000000]              
	int                                                nIconRef;                                         		// 0x0010 (0x0004) [0x0000000000000000]              
	unsigned long                                      bEarned : 1;                                      		// 0x0014 (0x0004) [0x0000000000000000] [0x00000001] 
};

// ScriptStruct BIOC_Base.BioGamerProfile.UnlockedBonusTalentInfo
// 0x000C
struct FUnlockedBonusTalentInfo
{
	int                                                nBonusTalentID;                                   		// 0x0000 (0x0004) [0x0000000000000000]              
	void*                                              srTitle;                                          		// 0x0004 (0x0004) [0x0000000000000000]              
	void*                                              srDesc;                                           		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGestureRuntimeData.BioGestCharOverride
// 0x0048
struct FBioGestCharOverride
{
	struct FName                                       nmFemale_NATIVE_MIRROR;                           		// 0x0000 (0x0008) [0x0000000000001002]              ( CPF_Const | CPF_Native )
	struct FName                                       nmAsari_NATIVE_MIRROR;                            		// 0x0008 (0x0008) [0x0000000000001002]              ( CPF_Const | CPF_Native )
	struct FName                                       nmTurian_NATIVE_MIRROR;                           		// 0x0010 (0x0008) [0x0000000000001002]              ( CPF_Const | CPF_Native )
	struct FName                                       nmSalarian_NATIVE_MIRROR;                         		// 0x0018 (0x0008) [0x0000000000001002]              ( CPF_Const | CPF_Native )
	struct FName                                       nmQuarian_NATIVE_MIRROR;                          		// 0x0020 (0x0008) [0x0000000000001002]              ( CPF_Const | CPF_Native )
	struct FName                                       nmOther_NATIVE_MIRROR;                            		// 0x0028 (0x0008) [0x0000000000001002]              ( CPF_Const | CPF_Native )
	struct FName                                       nmKrogan_NATIVE_MIRROR;                           		// 0x0030 (0x0008) [0x0000000000001002]              ( CPF_Const | CPF_Native )
	struct FName                                       nmGeth_NATIVE_MIRROR;                             		// 0x0038 (0x0008) [0x0000000000001002]              ( CPF_Const | CPF_Native )
	struct FName                                       nmOther_Artificial_NATIVE_MIRROR;                 		// 0x0040 (0x0008) [0x0000000000001002]              ( CPF_Const | CPF_Native )
};

// ScriptStruct BIOC_Base.BioGestureLangOverride.BioGestLangOverride
// 0x0018
struct FBioGestLangOverride
{
	struct FName                                       nmAnimToOverride;                                 		// 0x0000 (0x0008) [0x0000000000000000]              
	struct FName                                       nmOverrideSet;                                    		// 0x0008 (0x0008) [0x0000000000000000]              
	struct FName                                       nmOverrideSeq;                                    		// 0x0010 (0x0008) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGestureRulesData.BioGestRuleItem
// 0x0028
struct FBioGestRuleItem
{
	struct FName                                       nmPose1;                                          		// 0x0000 (0x0008) [0x0000000000000000]              
	struct FName                                       nmPose2;                                          		// 0x0008 (0x0008) [0x0000000000000000]              
	struct FName                                       nmGesture;                                        		// 0x0010 (0x0008) [0x0000000000000000]              
	struct FName                                       nmAnimSet;                                        		// 0x0018 (0x0008) [0x0000000000000000]              
	struct FName                                       nmAnimSeq;                                        		// 0x0020 (0x0008) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioGestureRulesData.BioARPUBodyConfig
// 0x0024
struct FBioARPUBodyConfig
{
	struct FName                                       nmCurveName;                                      		// 0x0000 (0x0008) [0x0000000000000000]              
	unsigned long                                      bUsesSingleKeyframe : 1;                          		// 0x0008 (0x0004) [0x0000000000000000] [0x00000001] 
	struct FName                                       nmAnimSet;                                        		// 0x000C (0x0008) [0x0000000000000000]              
	struct FName                                       nmAnimSeq;                                        		// 0x0014 (0x0008) [0x0000000000000000]              
	float                                              fStartBlendDuration;                              		// 0x001C (0x0004) [0x0000000000000000]              
	float                                              fEndBlendDuration;                                		// 0x0020 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioProjectile.BioProjectilePhysicsCollisionContext
// 0x001C
struct FBioProjectilePhysicsCollisionContext
{
	class AActor*                                      m_oActor;                                         		// 0x0000 (0x0004) [0x0000000000000000]              
	struct FVector                                     m_vHitLocation;                                   		// 0x0004 (0x000C) [0x0000000000000000]              
	struct FVector                                     m_vHitNormal;                                     		// 0x0010 (0x000C) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioMaterialOverride.TextureParameter
// 0x000C
struct FTextureParameter
{
	struct FName                                       nName;                                            		// 0x0000 (0x0008) [0x0000000000000000]              
	class UTexture*                                    m_pTexture;                                       		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioMaterialOverride.ColorParameter
// 0x0018
struct FColorParameter
{
	struct FName                                       nName;                                            		// 0x0000 (0x0008) [0x0000000000000000]              
	struct FLinearColor                                cValue;                                           		// 0x0008 (0x0010) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioMaterialOverride.ScalarParameter
// 0x000C
struct FScalarParameter
{
	struct FName                                       nName;                                            		// 0x0000 (0x0008) [0x0000000000000000]              
	float                                              sValue;                                           		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioInterface_Appearance_Pawn.CreatureSpeeds
// 0x0060
struct FCreatureSpeeds
{
	float                                              fGroundSpeed;                                     		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fAirSpeed;                                        		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fWaterSpeed;                                      		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fClimbSpeed;                                      		// 0x000C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fWalkSpeed;                                       		// 0x0010 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fSprintSpeed;                                     		// 0x0014 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fExploreSprintSpeed;                              		// 0x0018 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fTacticalSpeed;                                   		// 0x001C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fTacWalkSpeed;                                    		// 0x0020 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fTacCrouchSpeed;                                  		// 0x0024 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fTacCrouchWalkSpeed;                              		// 0x0028 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fTurnSpeedHigh;                                   		// 0x002C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fTurnSpeedLow;                                    		// 0x0030 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fTurnSpeedCombat;                                 		// 0x0034 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fTurnSpeedCombatCrouch;                           		// 0x0038 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fTurnSpeedCombatSprint;                           		// 0x003C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fStandardFriction;                                		// 0x0040 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fAccelRate;                                       		// 0x0044 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fMaxDSAccel;                                      		// 0x0048 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fMaxDSDecel;                                      		// 0x004C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fMaxDSAccelTac;                                   		// 0x0050 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fMaxDSDecelTac;                                   		// 0x0054 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fMaxDSAccelTacCrouch;                             		// 0x0058 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fMaxDSDecelTacCrouch;                             		// 0x005C (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioInterface_Appearance_Pawn.BioPawnLabelledHeadGearBool
// 0x0008
struct FBioPawnLabelledHeadGearBool
{
	unsigned char                                      m_eComponent;                                     		// 0x0000 (0x0001) [0x0000000000020003]              ( CPF_Edit | CPF_Const | CPF_EditConst )
	unsigned long                                      m_bIsVisible : 1;                                 		// 0x0004 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioInterface_Appearance_Pawn.BioPawnHeadGearVisibility
// 0x001C
struct FBioPawnHeadGearVisibility
{
	unsigned long                                      m_bOverride : 1;                                  		// 0x0000 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	struct FBioPawnLabelledHeadGearBool                m_a[ 0x3 ];                                       		// 0x0004 (0x0018) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioInterfaceAppearanceVehicle.BioVehicleTurretRunTimeInfo
// 0x000C
struct FBioVehicleTurretRunTimeInfo
{
	unsigned long                                      m_bTurretAutoTrackYaw : 1;                        		// 0x0000 (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      m_bTurretAutoTrackPitch : 1;                      		// 0x0000 (0x0004) [0x0000000000000000] [0x00000002] 
	float                                              m_fCurrentTurretRelativeYaw;                      		// 0x0004 (0x0004) [0x0000000000000000]              
	float                                              m_fCurrentTurretRelativePitch;                    		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioItemXModdableSaveObject.BioItemXModdableSaveObjectSlotSpec
// 0x0010
struct FBioItemXModdableSaveObjectSlotSpec
{
	int                                                m_nType;                                          		// 0x0000 (0x0004) [0x0000000000000000]              
	TArray< class UBioItemXModSaveObject* >            m_aXMod;                                          		// 0x0004 (0x000C) [0x0000000004400008]              ( CPF_ExportObject | CPF_NeedCtorLink | CPF_EditInline )
};

// ScriptStruct BIOC_Base.BioVISSimple.BioVISSData
// 0x000C
struct FBioVISSData
{
	class UPhysicalMaterial*                           oPhysMat;                                         		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class UBioVFXTemplate*                             oVFXTemplate;                                     		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class UBioVFXTemplate*                             oImpactOverrideVFXTemplate;                       		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioLevelSaveObject.SavedLootBag
// 0x0018
struct FSavedLootBag
{
	struct FVector                                     m_vPosition;                                      		// 0x0000 (0x000C) [0x0000000000000000]              
	struct FString                                     m_sActorType;                                     		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioLookAtDefinition.LookAtBoneDefinition
// 0x0040
struct FLookAtBoneDefinition
{
	struct FName                                       m_nBoneName;                                      		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	float                                              m_fLimit;                                         		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              m_fUpDownLimit;                                   		// 0x000C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              m_fDelay;                                         		// 0x0010 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned char                                      m_nLookAxis;                                      		// 0x0014 (0x0001) [0x0000000000000001]              ( CPF_Edit )
	unsigned char                                      m_nUpAxis;                                        		// 0x0015 (0x0001) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      m_bSeparateUpDownLimit : 1;                       		// 0x0018 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	unsigned long                                      m_bUseUpAxis : 1;                                 		// 0x0018 (0x0004) [0x0000000000000001] [0x00000002] ( CPF_Edit )
	unsigned long                                      m_bUpAxisInLocalSpace : 1;                        		// 0x0018 (0x0004) [0x0000000000000001] [0x00000004] ( CPF_Edit )
	unsigned long                                      m_bLookAtInverted : 1;                            		// 0x0018 (0x0004) [0x0000000000000001] [0x00000008] ( CPF_Edit )
	unsigned long                                      m_bUpAxisInverted : 1;                            		// 0x0018 (0x0004) [0x0000000000000001] [0x00000010] ( CPF_Edit )
	unsigned long                                      m_bUseAcceleration : 1;                           		// 0x0018 (0x0004) [0x0000000000000001] [0x00000020] ( CPF_Edit )
	float                                              m_fSpeedFactor;                                   		// 0x001C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              m_fMaxAcceleration;                               		// 0x0020 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              m_fConversationStrength;                          		// 0x0024 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      m_bUseMasterBone : 1;                             		// 0x0028 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	struct FName                                       m_nmMasterBoneName;                               		// 0x002C (0x0008) [0x0000000000000001]              ( CPF_Edit )
	TArray< struct FName >                             m_anTargetBones;                                  		// 0x0034 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioMorphFace.MorphFeature
// 0x000C
struct FMorphFeature
{
	struct FName                                       sFeatureName;                                     		// 0x0000 (0x0008) [0x0000000000000000]              
	float                                              Offset;                                           		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioMorphFace.OffsetBonePos
// 0x0014
struct FOffsetBonePos
{
	struct FName                                       nName;                                            		// 0x0000 (0x0008) [0x0000000000000000]              
	struct FVector                                     vPos;                                             		// 0x0008 (0x000C) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioMorphFace.BioDummyRenderCommandFence
// 0x0008
struct FBioDummyRenderCommandFence
{
	int                                                nDummy;                                           		// 0x0000 (0x0004) [0x0000000000001002]              ( CPF_Const | CPF_Native )
	int                                                nDummy2;                                          		// 0x0004 (0x0004) [0x0000000000001002]              ( CPF_Const | CPF_Native )
};

// ScriptStruct BIOC_Base.BioMorphUtility.TextureData
// 0x000C
struct FTextureData
{
	struct FName                                       m_nParamName;                                     		// 0x0000 (0x0008) [0x0000000000000000]              
	class UTexture*                                    m_oTexture;                                       		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioMorphUtility.ScalarData
// 0x000C
struct FScalarData
{
	struct FName                                       m_nParamName;                                     		// 0x0000 (0x0008) [0x0000000000000000]              
	float                                              m_fScalarValue;                                   		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioMorphUtility.HairComponent
// 0x0048
struct FHairComponent
{
	struct FString                                     StyleName;                                        		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FString                                     MeshName;                                         		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	class USkeletalMesh*                               HairMesh;                                         		// 0x0018 (0x0004) [0x0000000000000000]              
	struct FString                                     ScalpMorphName;                                   		// 0x001C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	float                                              ScalpMorphWeight;                                 		// 0x0028 (0x0004) [0x0000000000000000]              
	unsigned char                                      HairType;                                         		// 0x002C (0x0001) [0x0000000000000000]              
	TArray< struct FTextureData >                      m_aHairTextures;                                  		// 0x0030 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< struct FScalarData >                       m_aHairScalars;                                   		// 0x003C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioMorphUtility.HairData
// 0x0024
struct FHairData
{
	struct FString                                     PackageName;                                      		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FString                                     HairMorphSpecMaskName;                            		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< struct FHairComponent >                    HairComponents;                                   		// 0x0018 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioMorphUtility.MaterialComponent
// 0x0040
struct FMaterialComponent
{
	struct FString                                     Label;                                            		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	unsigned char                                      Type;                                             		// 0x000C (0x0001) [0x0000000000000000]              
	struct FString                                     Name;                                             		// 0x0010 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FString                                     Panel;                                            		// 0x001C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FString                                     ParameterName;                                    		// 0x0028 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< struct FString >                           Params;                                           		// 0x0034 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioMorphUtility.MaterialGroup
// 0x0018
struct FMaterialGroup
{
	struct FString                                     Name;                                             		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< struct FMaterialComponent >                Components;                                       		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioMorphUtility.MaterialPanel
// 0x0018
struct FMaterialPanel
{
	struct FString                                     Name;                                             		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< struct FMaterialGroup >                    Groups;                                           		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioMorphUtility.MaterialData
// 0x000C
struct FMaterialData
{
	TArray< struct FMaterialPanel >                    Panels;                                           		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioMorphUtility.AdditionalData
// 0x0024
struct FAdditionalData
{
	struct FHairData                                   Hair;                                             		// 0x0000 (0x0024) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioMorphFaceFrontEnd.BaseSliders
// 0x0010
struct FBaseSliders
{
	struct FString                                     m_sSliderName;                                    		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	float                                              m_fValue;                                         		// 0x000C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioMorphFaceFrontEnd.BaseHeads
// 0x000C
struct FBaseHeads
{
	TArray< struct FBaseSliders >                      m_fBaseHeadSettings;                              		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioMorphFaceFrontEnd.SliderModifierSliderData
// 0x001C
struct FSliderModifierSliderData
{
	TArray< class UBioMorphFaceFESliderBase* >         m_aoSliderData;                                   		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< float >                                    m_fRandWeights;                                   		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	float                                              m_fRandWeightsTotal;                              		// 0x0018 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioMorphFaceFrontEnd.SliderModifier
// 0x0030
struct FSliderModifier
{
	struct FString                                     m_sName;                                          		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< float >                                    m_aRandMin;                                       		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< float >                                    m_aRandMax;                                       		// 0x0018 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< struct FSliderModifierSliderData >         m_aSliders;                                       		// 0x0024 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioMorphFaceFrontEnd.Slider
// 0x0054
struct FSlider
{
	struct FString                                     m_sName;                                          		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	int                                                m_iIndex;                                         		// 0x000C (0x0004) [0x0000000000000000]              
	int                                                m_iValue;                                         		// 0x0010 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bNotched : 1;                                   		// 0x0014 (0x0004) [0x0000000000000000] [0x00000001] 
	int                                                m_iSteps;                                         		// 0x0018 (0x0004) [0x0000000000000000]              
	int                                                m_iStringRef;                                     		// 0x001C (0x0004) [0x0000000000000000]              
	int                                                m_iDescriptionStringRef;                          		// 0x0020 (0x0004) [0x0000000000000000]              
	TArray< class UBioMorphFaceFESliderBase* >         m_aoSliderData;                                   		// 0x0024 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< float >                                    m_fRandWeights;                                   		// 0x0030 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	float                                              m_fRandWeightsTotal;                              		// 0x003C (0x0004) [0x0000000000000000]              
	float                                              m_fRandMin;                                       		// 0x0040 (0x0004) [0x0000000000000000]              
	float                                              m_fRandMax;                                       		// 0x0044 (0x0004) [0x0000000000000000]              
	TArray< struct FSliderModifier >                   m_aSliderModifiers;                               		// 0x0048 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioMorphFaceFrontEnd.Category
// 0x0024
struct FCategory
{
	struct FString                                     m_sCatName;                                       		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	int                                                m_iCatIndex;                                      		// 0x000C (0x0004) [0x0000000000000000]              
	int                                                m_iStringRef;                                     		// 0x0010 (0x0004) [0x0000000000000000]              
	int                                                m_iDescriptionStringRef;                          		// 0x0014 (0x0004) [0x0000000000000000]              
	TArray< struct FSlider >                           m_aoSliders;                                      		// 0x0018 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioMorphFaceFrontEnd.FaceData
// 0x0030
struct FFaceData
{
	struct FAdditionalData                             m_pAdditionalParams;                              		// 0x0000 (0x0024) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< struct FCategory >                         m_oCategories;                                    		// 0x0024 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioMorphFaceSaveObject.VertexBuffer
// 0x000C
struct FVertexBuffer
{
	TArray< struct FVector >                           m_vPosition;                                      		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioParticleModuleLocationAttachedMesh.EmissionAreaWeight
// 0x000C
struct FEmissionAreaWeight
{
	struct FName                                       AreaTag;                                          		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	float                                              Weight;                                           		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioTalentContainer.BioTalentSpecification
// 0x0040
struct FBioTalentSpecification
{
	class UBioTalent*                                  m_Talent;                                         		// 0x0000 (0x0004) [0x0000000004400008]              ( CPF_ExportObject | CPF_NeedCtorLink | CPF_EditInline )
	TArray< int >                                      m_PrereqTalentIDArray;                            		// 0x0004 (0x000C) [0x0000000004400008]              ( CPF_ExportObject | CPF_NeedCtorLink | CPF_EditInline )
	TArray< int >                                      m_PrereqTalentRankArray;                          		// 0x0010 (0x000C) [0x0000000004400008]              ( CPF_ExportObject | CPF_NeedCtorLink | CPF_EditInline )
	int                                                m_MaxRank;                                        		// 0x001C (0x0004) [0x0000000000000000]              
	int                                                m_LevelOffset;                                    		// 0x0020 (0x0004) [0x0000000000000000]              
	int                                                m_LevelsPerRank;                                  		// 0x0024 (0x0004) [0x0000000000000000]              
	int                                                m_OriginalRank;                                   		// 0x0028 (0x0004) [0x0000000000000000]              
	int                                                m_VisualOrder;                                    		// 0x002C (0x0004) [0x0000000000000000]              
	void*                                              m_LocalizedName;                                  		// 0x0030 (0x0004) [0x0000000000000000]              
	void*                                              m_LocalizedDesc;                                  		// 0x0034 (0x0004) [0x0000000000000000]              
	void*                                              m_StringForUnlockReferences;                      		// 0x0038 (0x0004) [0x0000000000000000]              
	void*                                              m_BlurbForUnlockReferences;                       		// 0x003C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioTalentContainer.BioTalentStaticData
// 0x0018
struct FBioTalentStaticData
{
	void*                                              m_TalentName;                                     		// 0x0000 (0x0004) [0x0000000000000000]              
	void*                                              m_TalentDesc;                                     		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                m_MaxRank;                                        		// 0x0008 (0x0004) [0x0000000000000000]              
	struct FString                                     m_sUnlockDetails;                                 		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioTalentContainer.BioTalentDynamicData
// 0x0040
struct FBioTalentDynamicData
{
	unsigned long                                      m_IsUnlocked : 1;                                 		// 0x0000 (0x0004) [0x0000000000000000] [0x00000001] 
	int                                                m_CurrentRank;                                    		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                m_AvailableRank;                                  		// 0x0008 (0x0004) [0x0000000000000000]              
	int                                                m_MaxRank;                                        		// 0x000C (0x0004) [0x0000000000000000]              
	TArray< int >                                      m_RankIconArray;                                  		// 0x0010 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< void* >                                    m_RankNameArray;                                  		// 0x001C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< void* >                                    m_RankDescArray;                                  		// 0x0028 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< void* >                                    m_RankUnlockBlurbArray;                           		// 0x0034 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioTalentContainer.BioTalentDebugData
// 0x000C
struct FBioTalentDebugData
{
	int                                                m_ID;                                             		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                m_Rank;                                           		// 0x0004 (0x0004) [0x0000000000000000]              
	void*                                              m_Name;                                           		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioTalentContainer.BioSimpleTalentSave
// 0x0008
struct FBioSimpleTalentSave
{
	int                                                m_TalentID;                                       		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                m_Ranks;                                          		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioTalentContainer.BioComplexTalentSave
// 0x0030
struct FBioComplexTalentSave
{
	int                                                m_TalentID;                                       		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                m_Ranks;                                          		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                m_MaxRank;                                        		// 0x0008 (0x0004) [0x0000000000000000]              
	int                                                m_LevelOffset;                                    		// 0x000C (0x0004) [0x0000000000000000]              
	int                                                m_LevelsPerRank;                                  		// 0x0010 (0x0004) [0x0000000000000000]              
	int                                                m_VisualOrder;                                    		// 0x0014 (0x0004) [0x0000000000000000]              
	TArray< int >                                      m_PrereqTalentIDArray;                            		// 0x0018 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	TArray< int >                                      m_PrereqTalentRankArray;                          		// 0x0024 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioPawnBehavior.ParticleSystemComponentSpec
// 0x000C
struct FParticleSystemComponentSpec
{
	TArray< class UParticleSystemComponent* >          m_aParticleSystemComponent;                       		// 0x0000 (0x000C) [0x0000000004480008]              ( CPF_ExportObject | CPF_Component | CPF_NeedCtorLink | CPF_EditInline )
};

// ScriptStruct BIOC_Base.BioPawnBehavior.BioPawnBehaviorCache
// 0x000C
struct FBioPawnBehaviorCache
{
	int                                                m_flags;                                          		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              m_driftAngle;                                     		// 0x0004 (0x0004) [0x0000000000000000]              
	float                                              m_driftZoneRadius;                                		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioPawnType.SoundSpec
// 0x0008
struct FSoundSpec
{
	unsigned char                                      m_ePawnSound;                                     		// 0x0000 (0x0001) [0x0000000000020003]              ( CPF_Edit | CPF_Const | CPF_EditConst )
	class USoundCue*                                   m_oSoundCue;                                      		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioPawnType.ParticleSystemSpec
// 0x0014
struct FParticleSystemSpec
{
	unsigned char                                      m_ePawnParticleSystem;                            		// 0x0000 (0x0001) [0x0000000000020003]              ( CPF_Edit | CPF_Const | CPF_EditConst )
	class UParticleSystem*                             m_oParticleSystem;                                		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	TArray< struct FName >                             m_aBoneName;                                      		// 0x0008 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioPawn.BioGestDataKey
// 0x006C
struct FBioGestDataKey
{
	struct FBioGestureData                             tRawData;                                         		// 0x0000 (0x0064) [0x0000000000400000]              ( CPF_NeedCtorLink )
	class UBioGestChainTree*                           pChainTree;                                       		// 0x0064 (0x0004) [0x0000000000000000]              
	unsigned long                                      bUseDynamicAnimSets : 1;                          		// 0x0068 (0x0004) [0x0000000000000000] [0x00000001] 
};

// ScriptStruct BIOC_Base.BioPawn.PawnFootStepData
// 0x0010
struct FPawnFootStepData
{
	int                                                BoneIndex;                                        		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              TimeOutRemaining;                                 		// 0x0004 (0x0004) [0x0000000000000000]              
	unsigned long                                      FootStepRequested : 1;                            		// 0x0008 (0x0004) [0x0000000000000000] [0x00000001] 
	class UBioVISFootstep*                             FootStepVIS;                                      		// 0x000C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioPawn.BioGestDataCopy
// 0x004C
struct FBioGestDataCopy
{
	struct FName                                       nmPoseSet;                                        		// 0x0000 (0x0008) [0x0000000000000000]              
	struct FName                                       nmPoseAnim;                                       		// 0x0008 (0x0008) [0x0000000000000000]              
	struct FName                                       nmGestureSet;                                     		// 0x0010 (0x0008) [0x0000000000000000]              
	struct FName                                       nmGestureAnim;                                    		// 0x0018 (0x0008) [0x0000000000000000]              
	struct FName                                       nmTransitionSet;                                  		// 0x0020 (0x0008) [0x0000000000000000]              
	struct FName                                       nmTransitionAnim;                                 		// 0x0028 (0x0008) [0x0000000000000000]              
	float                                              fPlayRate;                                        		// 0x0030 (0x0004) [0x0000000000000000]              
	float                                              fStartOffset;                                     		// 0x0034 (0x0004) [0x0000000000000000]              
	float                                              fEndOffset;                                       		// 0x0038 (0x0004) [0x0000000000000000]              
	unsigned long                                      bOneShotAnim : 1;                                 		// 0x003C (0x0004) [0x0000000000000000] [0x00000001] 
	float                                              fStartBlendDuration;                              		// 0x0040 (0x0004) [0x0000000000000000]              
	float                                              fEndBlendDuration;                                		// 0x0044 (0x0004) [0x0000000000000000]              
	float                                              fWeight;                                          		// 0x0048 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioPlayerController.BioActionMapping
// 0x0010
struct FBioActionMapping
{
	unsigned char                                      eType;                                            		// 0x0000 (0x0001) [0x0000000000000000]              
	struct FName                                       nmPower;                                          		// 0x0004 (0x0008) [0x0000000000000000]              
	class APawn*                                       oPawn;                                            		// 0x000C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioPhysicsSounds.BioPhysicsSoundsInfo
// 0x000C
struct FBioPhysicsSoundsInfo
{
	int                                                nPriority;                                        		// 0x0000 (0x0004) [0x0000000000000000]              
	class UAudioComponent*                             oAudioComp;                                       		// 0x0004 (0x0004) [0x0000000004080008]              ( CPF_ExportObject | CPF_Component | CPF_EditInline )
	float                                              fLastTimePlayed;                                  		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioPlayerSelection.BioSelection
// 0x000C
struct FBioSelection
{
	float                                              m_fAngle;                                         		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              m_fRange;                                         		// 0x0004 (0x0004) [0x0000000000000000]              
	class AActor*                                      m_pTarget;                                        		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioPlayerController.BioPlayerControllerTwistThresholdSpec
// 0x0008
struct FBioPlayerControllerTwistThresholdSpec
{
	unsigned char                                      m_eActionState;                                   		// 0x0000 (0x0001) [0x0000000000020003]              ( CPF_Edit | CPF_Const | CPF_EditConst )
	float                                              m_fTwistThreshold;                                		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioPlayerController.PostProcessInfo
// 0x0014
struct FPostProcessInfo
{
	unsigned char                                      Preset;                                           		// 0x0000 (0x0001) [0x0000000000000000]              
	float                                              Shadows;                                          		// 0x0004 (0x0004) [0x0000000000000000]              
	float                                              MidTones;                                         		// 0x0008 (0x0004) [0x0000000000000000]              
	float                                              HighLights;                                       		// 0x000C (0x0004) [0x0000000000000000]              
	float                                              Desaturation;                                     		// 0x0010 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioPlayerController.BioRadarData
// 0x003C
struct FBioRadarData
{
	int                                                nIndex;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              fPassTime;                                        		// 0x0004 (0x0004) [0x0000000000000000]              
	unsigned char                                      eRadarType;                                       		// 0x0008 (0x0001) [0x0000000000000000]              
	struct FVector                                     vPosition;                                        		// 0x000C (0x000C) [0x0000000000000000]              
	int                                                nSize;                                            		// 0x0018 (0x0004) [0x0000000000000000]              
	unsigned long                                      bPlayerLockedOn : 1;                              		// 0x001C (0x0004) [0x0000000000000000] [0x00000001] 
	int                                                nLast_Index;                                      		// 0x0020 (0x0004) [0x0000000000000000]              
	unsigned char                                      eLast_RadarType;                                  		// 0x0024 (0x0001) [0x0000000000000000]              
	struct FVector                                     vLast_Position;                                   		// 0x0028 (0x000C) [0x0000000000000000]              
	int                                                nLast_Size;                                       		// 0x0034 (0x0004) [0x0000000000000000]              
	unsigned long                                      bLast_PlayerLockedOn : 1;                         		// 0x0038 (0x0004) [0x0000000000000000] [0x00000001] 
};

// ScriptStruct BIOC_Base.BioPlayerController.BioRadarCacheUpdateData
// 0x0010
struct FBioRadarCacheUpdateData
{
	int                                                nIndex;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	struct FVector                                     vPosition;                                        		// 0x0004 (0x000C) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioPlayerController.BioDamageIndicatorData
// 0x0004
struct FBioDamageIndicatorData
{
	float                                              fCoolDownTime;                                    		// 0x0000 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioPlayerInput.DebugMenuEntry
// 0x0018
struct FDebugMenuEntry
{
	struct FString                                     Name;                                             		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FString                                     Command;                                          		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioPlayerInput.TutorialCommandsDetails
// 0x0018
struct FTutorialCommandsDetails
{
	struct FString                                     srParseString;                                    		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FString                                     Action;                                           		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioPlayerSquad.SquadTargetData
// 0x0020
struct FSquadTargetData
{
	class AActor*                                      oTarget;                                          		// 0x0000 (0x0004) [0x0000000000000000]              
	struct FVector                                     vLocation;                                        		// 0x0004 (0x000C) [0x0000000000000000]              
	int                                                nActionIcon;                                      		// 0x0010 (0x0004) [0x0000000000000000]              
	int                                                nSquadIcon;                                       		// 0x0014 (0x0004) [0x0000000000000000]              
	unsigned long                                      bHidden : 1;                                      		// 0x0018 (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      bActive : 1;                                      		// 0x0018 (0x0004) [0x0000000000000000] [0x00000002] 
	float                                              fTimeOut;                                         		// 0x001C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioPower.BioPowerParam
// 0x0064
struct FBioPowerParam
{
	struct FName                                       nmParamName;                                      		// 0x0000 (0x0008) [0x0000000000000000]              
	struct FBioComplexFloatStructAttribute             m_value;                                          		// 0x0008 (0x005C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioPowerVFXAppearance.PowerVFXData
// 0x0008
struct FPowerVFXData
{
	unsigned long                                      bUsePowerTime : 1;                                		// 0x0000 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	class UBioVFXTemplate*                             m_EffectTemplate;                                 		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioSaveGame.HenchmanLookup
// 0x0014
struct FHenchmanLookup
{
	struct FName                                       Tag;                                              		// 0x0000 (0x0008) [0x0000000000000000]              
	class UBioBaseSaveObject*                          Save;                                             		// 0x0008 (0x0004) [0x0000000000000000]              
	struct FName                                       nmMappedPower;                                    		// 0x000C (0x0008) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSaveGame.SaveGameAreaInfo
// 0x002C
struct FSaveGameAreaInfo
{
	int                                                m_nAreaId;                                        		// 0x0000 (0x0004) [0x0000000000000000]              
	struct FString                                     m_sMap;                                           		// 0x0004 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	void*                                              m_nMapNameRef;                                    		// 0x0010 (0x0004) [0x0000000000000000]              
	struct FString                                     m_sImageResource;                                 		// 0x0014 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FString                                     m_sSWFStopPoint;                                  		// 0x0020 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioSaveGame.SaveGameRecord
// 0x00C4
struct FSaveGameRecord
{
	struct FString                                     m_filename;                                       		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FString                                     m_saveName;                                       		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FString                                     m_saveNameShort;                                  		// 0x0018 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	int                                                m_sec;                                            		// 0x0024 (0x0004) [0x0000000000000000]              
	int                                                m_Min;                                            		// 0x0028 (0x0004) [0x0000000000000000]              
	int                                                m_hr;                                             		// 0x002C (0x0004) [0x0000000000000000]              
	int                                                m_day;                                            		// 0x0030 (0x0004) [0x0000000000000000]              
	int                                                m_mo;                                             		// 0x0034 (0x0004) [0x0000000000000000]              
	int                                                m_year;                                           		// 0x0038 (0x0004) [0x0000000000000000]              
	int                                                m_secPlayed;                                      		// 0x003C (0x0004) [0x0000000000000000]              
	int                                                m_minPlayed;                                      		// 0x0040 (0x0004) [0x0000000000000000]              
	int                                                m_hrPlayed;                                       		// 0x0044 (0x0004) [0x0000000000000000]              
	struct FSaveGameAreaInfo                           m_SaveAreaInfo;                                   		// 0x0048 (0x002C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FSaveGameAreaInfo                           m_ParentSaveAreaInfo;                             		// 0x0074 (0x002C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FString                                     m_charId;                                         		// 0x00A0 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	int                                                m_ClassId;                                        		// 0x00AC (0x0004) [0x0000000000000000]              
	int                                                m_CharcterLevel;                                  		// 0x00B0 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bCanDelete : 1;                                 		// 0x00B4 (0x0004) [0x0000000000000000] [0x00000001] 
	int                                                m_DeviceId;                                       		// 0x00B8 (0x0004) [0x0000000000000000]              
	int                                                m_SaveGameNumber;                                 		// 0x00BC (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bIsAutoSave : 1;                                		// 0x00C0 (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      m_bIsQuickSave : 1;                               		// 0x00C0 (0x0004) [0x0000000000000000] [0x00000002] 
};

// ScriptStruct BIOC_Base.BioSeqAct_ActionStation.BioActionStationGestureData
// 0x0028
struct FBioActionStationGestureData
{
	struct FName                                       nmPoseSet;                                        		// 0x0000 (0x0008) [0x0000000000000000]              
	struct FName                                       nmPoseAnim;                                       		// 0x0008 (0x0008) [0x0000000000000000]              
	struct FName                                       nmTransitionSet;                                  		// 0x0010 (0x0008) [0x0000000000000000]              
	struct FName                                       nmTransitionAnim;                                 		// 0x0018 (0x0008) [0x0000000000000000]              
	struct FName                                       nmEnumName;                                       		// 0x0020 (0x0008) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSeqAct_ActionStation.ActionStationOffsetData
// 0x0020
struct FActionStationOffsetData
{
	struct FName                                       nmAnimSet;                                        		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	struct FVector                                     vOffset;                                          		// 0x0008 (0x000C) [0x0000000000000001]              ( CPF_Edit )
	struct FRotator                                    rRotation;                                        		// 0x0014 (0x000C) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioSeqAct_InteractProperty.BioPropertyMap
// 0x0010
struct FBioPropertyMap
{
	struct FName                                       sDisplayName;                                     		// 0x0000 (0x0008) [0x0000000000000000]              
	struct FName                                       sFuncPropName;                                    		// 0x0008 (0x0008) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSeqAct_InteractProperty.BioPropertyInfo
// 0x0015
struct FBioPropertyInfo
{
	struct FName                                       PropertyName;                                     		// 0x0000 (0x0008) [0x0000000000020003]              ( CPF_Edit | CPF_Const | CPF_EditConst )
	struct FName                                       ActualPropertyName;                               		// 0x0008 (0x0008) [0x0000000000000000]              
	unsigned long                                      bDisplayProperty : 1;                             		// 0x0010 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	unsigned long                                      bOldDisplayProperty : 1;                          		// 0x0010 (0x0004) [0x0000000000000000] [0x00000002] 
	unsigned char                                      ePropertyType;                                    		// 0x0014 (0x0001) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioWorldInfo.SlowMotionRequestType
// 0x0014
struct FSlowMotionRequestType
{
	int                                                nReqID;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	unsigned long                                      bIndefinite : 1;                                  		// 0x0004 (0x0004) [0x0000000000000000] [0x00000001] 
	float                                              fSpeed;                                           		// 0x0008 (0x0004) [0x0000000000000000]              
	float                                              fLifeTime;                                        		// 0x000C (0x0004) [0x0000000000000000]              
	int                                                nPriority;                                        		// 0x0010 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioWorldInfo.WorldStreamingState
// 0x000C
struct FWorldStreamingState
{
	struct FName                                       Name;                                             		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      Enabled : 1;                                      		// 0x0008 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioWorldInfo.VFXTemplatePoolSizeSpec
// 0x000C
struct FVFXTemplatePoolSizeSpec
{
	class UBioVFXTemplate*                             Template;                                         		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	int                                                MaxPoolSize;                                      		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	int                                                MinPoolSize;                                      		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioWorldInfo.VFXListNode
// 0x0010
struct FVFXListNode
{
	class ABioVisualEffect*                            Effect;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                NextNode;                                         		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                PrevNode;                                         		// 0x0008 (0x0004) [0x0000000000000000]              
	unsigned long                                      ValidNode : 1;                                    		// 0x000C (0x0004) [0x0000000000000000] [0x00000001] 
};

// ScriptStruct BIOC_Base.BioWorldInfo.WorldEnvironmentEffect
// 0x0028
struct FWorldEnvironmentEffect
{
	class ABioVisualEffect*                            m_Effect;                                         		// 0x0000 (0x0004) [0x0000000000002000]              ( CPF_Transient )
	struct FVector                                     m_vOffset;                                        		// 0x0004 (0x000C) [0x0000000000000000]              
	float                                              m_fBlendDuration;                                 		// 0x0010 (0x0004) [0x0000000000000000]              
	float                                              m_fBlendTime;                                     		// 0x0014 (0x0004) [0x0000000000000000]              
	float                                              m_fDesiredIntensity;                              		// 0x0018 (0x0004) [0x0000000000000000]              
	float                                              m_fCurrentIntensity;                              		// 0x001C (0x0004) [0x0000000000000000]              
	class AController*                                 m_TargetController;                               		// 0x0020 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bAttachToCamera : 1;                            		// 0x0024 (0x0004) [0x0000000000000000] [0x00000001] 
};

// ScriptStruct BIOC_Base.BioWorldInfo.BoughtVFXListEnds
// 0x0008
struct FBoughtVFXListEnds
{
	int                                                HeadIndex;                                        		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                TailIndex;                                        		// 0x0004 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSFHandler_MessageBox.BioMessageBoxOptionalParams
// 0x0018
struct FBioMessageBoxOptionalParams
{
	void*                                              srAText;                                          		// 0x0000 (0x0004) [0x0000000000000000]              
	void*                                              srBText;                                          		// 0x0004 (0x0004) [0x0000000000000000]              
	unsigned long                                      bNoFade : 1;                                      		// 0x0008 (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned char                                      nIconSet;                                         		// 0x000C (0x0001) [0x0000000000000000]              
	int                                                nIconIndex;                                       		// 0x0010 (0x0004) [0x0000000000000000]              
	unsigned long                                      bModal : 1;                                       		// 0x0014 (0x0004) [0x0000000000000000] [0x00000001] 
};

// ScriptStruct BIOC_Base.BioSeqData_DEBUGInfo.AvoidanceLocations
// 0x002C
struct FAvoidanceLocations
{
	struct FVector                                     vLoc;                                             		// 0x0000 (0x000C) [0x0000000000000000]              
	struct FVector                                     vVel;                                             		// 0x000C (0x000C) [0x0000000000000000]              
	struct FRotator                                    rRot;                                             		// 0x0018 (0x000C) [0x0000000000000000]              
	float                                              fCollision;                                       		// 0x0024 (0x0004) [0x0000000000000000]              
	float                                              fExtendedCollision;                               		// 0x0028 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSeqData_DEBUGInfo.AvoidanceLocationsArray
// 0x0034
struct FAvoidanceLocationsArray
{
	struct FVector                                     vPawnLoc;                                         		// 0x0000 (0x000C) [0x0000000000000000]              
	struct FVector                                     vStearing;                                        		// 0x000C (0x000C) [0x0000000000000000]              
	struct FVector                                     vAdjustPoint;                                     		// 0x0018 (0x000C) [0x0000000000000000]              
	float                                              fSearchDist;                                      		// 0x0024 (0x0004) [0x0000000000000000]              
	TArray< struct FAvoidanceLocations >               aoObsticals;                                      		// 0x0028 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioSeqData_DEBUGInfo.LastCompletionRecords
// 0x0014
struct FLastCompletionRecords
{
	struct FName                                       nmAction;                                         		// 0x0000 (0x0008) [0x0000000000000000]              
	struct FName                                       nmtech;                                           		// 0x0008 (0x0008) [0x0000000000000000]              
	int                                                nReason;                                          		// 0x0010 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSeqData_DEBUGInfo.LOSLinesRecord
// 0x001C
struct FLOSLinesRecord
{
	struct FVector                                     vStart;                                           		// 0x0000 (0x000C) [0x0000000000000000]              
	struct FVector                                     vEnd;                                             		// 0x000C (0x000C) [0x0000000000000000]              
	unsigned long                                      bSaw : 1;                                         		// 0x0018 (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      bEnemy : 1;                                       		// 0x0018 (0x0004) [0x0000000000000000] [0x00000002] 
};

// ScriptStruct BIOC_Base.BioSFHandler_BrowserWheel.BWPageStruct
// 0x0010
struct FBWPageStruct
{
	struct FName                                       Tag;                                              		// 0x0000 (0x0008) [0x0000000000000000]              
	void*                                              srLabel;                                          		// 0x0008 (0x0004) [0x0000000000000000]              
	class UBioSFHandler*                               oHandler;                                         		// 0x000C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSFHandler_Credits.BioCreditsDetails
// 0x000C
struct FBioCreditsDetails
{
	void*                                              srHeading;                                        		// 0x0000 (0x0004) [0x0000000000000000]              
	void*                                              srTitle;                                          		// 0x0004 (0x0004) [0x0000000000000000]              
	void*                                              srNames;                                          		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSFHandler_DataCodex.DataCodexEntryDetails
// 0x0018
struct FDataCodexEntryDetails
{
	int                                                nSectionID;                                       		// 0x0000 (0x0004) [0x0000000000000000]              
	struct FString                                     sName;                                            		// 0x0004 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	unsigned long                                      bUpdated : 1;                                     		// 0x0010 (0x0004) [0x0000000000000000] [0x00000001] 
	struct FPointer                                    pEntry;                                           		// 0x0014 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSFHandler_DesignerUI.BioDUIPulseDetails
// 0x0010
struct FBioDUIPulseDetails
{
	unsigned char                                      nElement;                                         		// 0x0000 (0x0001) [0x0000000000000000]              
	float                                              fHalfCycleTime;                                   		// 0x0004 (0x0004) [0x0000000000000000]              
	float                                              fMinAlpha;                                        		// 0x0008 (0x0004) [0x0000000000000000]              
	float                                              fCurCycle;                                        		// 0x000C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSFHandler_DesignerUI.BioDUITimerDetails
// 0x0014
struct FBioDUITimerDetails
{
	float                                              fCurTime;                                         		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              fEndTime;                                         		// 0x0004 (0x0004) [0x0000000000000000]              
	float                                              fIntervalTime;                                    		// 0x0008 (0x0004) [0x0000000000000000]              
	float                                              fNextInterval;                                    		// 0x000C (0x0004) [0x0000000000000000]              
	unsigned long                                      bIncrementing : 1;                                		// 0x0010 (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      bIntervalTriggered : 1;                           		// 0x0010 (0x0004) [0x0000000000000000] [0x00000002] 
	unsigned long                                      bCompleted : 1;                                   		// 0x0010 (0x0004) [0x0000000000000000] [0x00000004] 
	unsigned long                                      bRunning : 1;                                     		// 0x0010 (0x0004) [0x0000000000000000] [0x00000008] 
	unsigned long                                      bActive : 1;                                      		// 0x0010 (0x0004) [0x0000000000000000] [0x00000010] 
	unsigned long                                      bFirstUpdate : 1;                                 		// 0x0010 (0x0004) [0x0000000000000000] [0x00000020] 
};

// ScriptStruct BIOC_Base.BioSFHandler_DesignerUI.BioDUIElementStatus
// 0x0004
struct FBioDUIElementStatus
{
	unsigned long                                      bVisible : 1;                                     		// 0x0000 (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      bFading : 1;                                      		// 0x0000 (0x0004) [0x0000000000000000] [0x00000002] 
};

// ScriptStruct BIOC_Base.BioSFHandler_HUD.CustomMappingDisplayInfo
// 0x0024
struct FCustomMappingDisplayInfo
{
	struct FString                                     sTitle;                                           		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	unsigned char                                      eMapSlot;                                         		// 0x000C (0x0001) [0x0000000000000000]              
	int                                                nActivatable;                                     		// 0x0010 (0x0004) [0x0000000000000000]              
	int                                                nIconRef;                                         		// 0x0014 (0x0004) [0x0000000000000000]              
	int                                                nBarBehaviorType;                                 		// 0x0018 (0x0004) [0x0000000000000000]              
	int                                                nBarPercentage;                                   		// 0x001C (0x0004) [0x0000000000000000]              
	int                                                nQtyConsumable;                                   		// 0x0020 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSFHandler_HUD.BioHUDCachedInvoke
// 0x0014
struct FBioHUDCachedInvoke
{
	unsigned char                                      eLabel;                                           		// 0x0000 (0x0001) [0x0000000000000000]              
	int                                                nFramesTheSame;                                   		// 0x0004 (0x0004) [0x0000000000000000]              
	TArray< struct FASParams >                         lstParams;                                        		// 0x0008 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioSFHandler_Journal.GuiQuestInfo
// 0x0018
struct FGuiQuestInfo
{
	int                                                nId;                                              		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                nAdded;                                           		// 0x0004 (0x0004) [0x0000000000000000]              
	struct FString                                     sName;                                            		// 0x0008 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	unsigned long                                      bComplete : 1;                                    		// 0x0014 (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      bUpdated : 1;                                     		// 0x0014 (0x0004) [0x0000000000000000] [0x00000002] 
};

// ScriptStruct BIOC_Base.MassEffectGuiManager.BioMessageBoxData
// 0x0010
struct FBioMessageBoxData
{
	class UObject*                                     m_pCallbackObject;                                		// 0x0000 (0x0004) [0x0000000000000000]              
	struct FName                                       m_nmCallbackFunction;                             		// 0x0004 (0x0008) [0x0000000000000000]              
	void*                                              m_srTutorial;                                     		// 0x000C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.MassEffectGuiManager.BioTutorialParams
// 0x0038
struct FBioTutorialParams
{
	struct FName                                       nmTutorial;                                       		// 0x0000 (0x0008) [0x0000000000000000]              
	void*                                              srTutorial;                                       		// 0x0008 (0x0004) [0x0000000000000000]              
	float                                              fTime;                                            		// 0x000C (0x0004) [0x0000000000000000]              
	unsigned char                                      nPosition;                                        		// 0x0010 (0x0001) [0x0000000000000000]              
	struct FName                                       nmHilightElement;                                 		// 0x0014 (0x0008) [0x0000000000000000]              
	class UObject*                                     oCallbackObject;                                  		// 0x001C (0x0004) [0x0000000000000000]              
	struct FName                                       nmCallbackFunction;                               		// 0x0020 (0x0008) [0x0000000000000000]              
	unsigned long                                      bDoNotCacheInputMode : 1;                         		// 0x0028 (0x0004) [0x0000000000000000] [0x00000001] 
	struct FString                                     sTutorial;                                        		// 0x002C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioSFHandler_PartySelection.PartySelectMemberInfo
// 0x0024
struct FPartySelectMemberInfo
{
	struct FName                                       Tag;                                              		// 0x0000 (0x0008) [0x0000000000000000]              
	int                                                ImageIndex;                                       		// 0x0008 (0x0004) [0x0000000000000000]              
	struct FName                                       DiscoveredLabel;                                  		// 0x000C (0x0008) [0x0000000000000000]              
	struct FName                                       AvailableLabel;                                   		// 0x0014 (0x0008) [0x0000000000000000]              
	struct FName                                       InPartyLabel;                                     		// 0x001C (0x0008) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSFHandler_PCSettings.BioScreenResolution
// 0x000C
struct FBioScreenResolution
{
	int                                                Width;                                            		// 0x0000 (0x0004) [0x0000000000000000]              
	int                                                Height;                                           		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                SizeID;                                           		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSFHandler_PCSettings.BindingSetupDetails
// 0x003C
struct FBindingSetupDetails
{
	struct FString                                     srTitle;                                          		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FString                                     Action;                                           		// 0x000C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FName                                       DefaultKeyPrimary;                                		// 0x0018 (0x0008) [0x0000000000000000]              
	struct FName                                       DefaultKeySecondary;                              		// 0x0020 (0x0008) [0x0000000000000000]              
	struct FName                                       KeyPrimary;                                       		// 0x0028 (0x0008) [0x0000000000000000]              
	struct FName                                       KeySecondary;                                     		// 0x0030 (0x0008) [0x0000000000000000]              
	void*                                              HelpText;                                         		// 0x0038 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSFHandler_PCSettings.sConstBinding
// 0x0014
struct FsConstBinding
{
	struct FName                                       Key;                                              		// 0x0000 (0x0008) [0x0000000000000000]              
	struct FString                                     Command;                                          		// 0x0008 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioSFHandler_Reticule.LOSIndicatorInfo
// 0x0015
struct FLOSIndicatorInfo
{
	struct FVector                                     m_vPosition;                                      		// 0x0000 (0x000C) [0x0000000000000000]              
	unsigned long                                      m_bLOS : 1;                                       		// 0x000C (0x0004) [0x0000000000000000] [0x00000001] 
	int                                                m_nScale;                                         		// 0x0010 (0x0004) [0x0000000000000000]              
	unsigned char                                      m_eObjectType;                                    		// 0x0014 (0x0001) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSFHandler_Reticule.ReticuleInfo
// 0x002C
struct FReticuleInfo
{
	struct FVector                                     m_vPosition;                                      		// 0x0000 (0x000C) [0x0000000000000000]              
	float                                              m_fRadius;                                        		// 0x000C (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bDisplayCrosshair : 1;                          		// 0x0010 (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned char                                      m_eMode;                                          		// 0x0014 (0x0001) [0x0000000000000000]              
	float                                              m_fModeTotalTime;                                 		// 0x0018 (0x0004) [0x0000000000000000]              
	float                                              m_fModeTimeRemaining;                             		// 0x001C (0x0004) [0x0000000000000000]              
	float                                              m_fDistanceToTarget;                              		// 0x0020 (0x0004) [0x0000000000000000]              
	float                                              m_fMagnificationLevel;                            		// 0x0024 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bIsMagneticLocked : 1;                          		// 0x0028 (0x0004) [0x0000000000000000] [0x00000001] 
};

// ScriptStruct BIOC_Base.BioSFHandler_Reticule.SelectionIndicatorInfo
// 0x0024
struct FSelectionIndicatorInfo
{
	struct FVector                                     m_vPosition;                                      		// 0x0000 (0x000C) [0x0000000000000000]              
	unsigned long                                      m_bVisible : 1;                                   		// 0x000C (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      m_bActivatable : 1;                               		// 0x000C (0x0004) [0x0000000000000000] [0x00000002] 
	unsigned long                                      m_bHostile : 1;                                   		// 0x000C (0x0004) [0x0000000000000000] [0x00000004] 
	int                                                m_nScale;                                         		// 0x0010 (0x0004) [0x0000000000000000]              
	unsigned char                                      m_eObjectType;                                    		// 0x0014 (0x0001) [0x0000000000000000]              
	float                                              m_fTimeSinceSelectionChanged;                     		// 0x0018 (0x0004) [0x0000000000000000]              
	float                                              m_fTimeOnPreviousSelection;                       		// 0x001C (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bResetTargeting : 1;                            		// 0x0020 (0x0004) [0x0000000000000000] [0x00000001] 
};

// ScriptStruct BIOC_Base.BioShieldView.BioShieldViewParticleSystemSpec
// 0x001C
struct FBioShieldViewParticleSystemSpec
{
	class ABioVisualEffect*                            m_pEffect;                                        		// 0x0000 (0x0004) [0x0000000000002000]              ( CPF_Transient )
	unsigned char                                      m_ePosition;                                      		// 0x0004 (0x0001) [0x0000000000000001]              ( CPF_Edit )
	unsigned char                                      m_eOrientation;                                   		// 0x0005 (0x0001) [0x0000000000000001]              ( CPF_Edit )
	struct FVector                                     m_vSize;                                          		// 0x0008 (0x000C) [0x0000000000000001]              ( CPF_Edit )
	unsigned char                                      m_aScale[ 0x3 ];                                  		// 0x0014 (0x0003) [0x0000000000000001]              ( CPF_Edit )
	class UBioVFXTemplate*                             m_oEffect;                                        		// 0x0018 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioShieldView.BioShieldViewEventSpec
// 0x0010
struct FBioShieldViewEventSpec
{
	unsigned char                                      m_eEvent;                                         		// 0x0000 (0x0001) [0x0000000000020001]              ( CPF_Edit | CPF_EditConst )
	TArray< struct FBioShieldViewParticleSystemSpec >  m_aVisualEffects;                                 		// 0x0004 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioShop.ResourceShopInfo
// 0x0014
struct FResourceShopInfo
{
	void*                                              m_Name;                                           		// 0x0000 (0x0004) [0x0000000000000000]              
	void*                                              m_Description;                                    		// 0x0004 (0x0004) [0x0000000000000000]              
	int                                                m_lotSize;                                        		// 0x0008 (0x0004) [0x0000000000000000]              
	float                                              m_unitPrice;                                      		// 0x000C (0x0004) [0x0000000000000000]              
	int                                                m_iconIndex;                                      		// 0x0010 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioSkelControl_TurretConstrained.TurretConstraintData
// 0x000C
struct FTurretConstraintData
{
	int                                                PitchConstraint;                                  		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	int                                                YawConstraint;                                    		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	int                                                RollConstraint;                                   		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioSoundNodeAPActivity.APActivityEntry
// 0x000C
struct FAPActivityEntry
{
	struct FName                                       nmActivity;                                       		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      bOneShot : 1;                                     		// 0x0008 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioSoundNodeAPTransition.TransitionData
// 0x0018
struct FTransitionData
{
	struct FName                                       FromState;                                        		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	struct FName                                       ToState;                                          		// 0x0008 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	struct FName                                       TransName;                                        		// 0x0010 (0x0008) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioSoundNodeFootsteps.FootstepData
// 0x0010
struct FFootstepData
{
	int                                                nFoot;                                            		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	unsigned char                                      nHeel_Toe;                                        		// 0x0004 (0x0001) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      bActive : 1;                                      		// 0x0008 (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	int                                                nNodeIdx;                                         		// 0x000C (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioSpectatorModeCamera.BioModelViewerCamera
// 0x0030
struct FBioModelViewerCamera
{
	struct FString                                     sName;                                            		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	struct FVector                                     vLocation;                                        		// 0x000C (0x000C) [0x0000000000000000]              
	struct FRotator                                    rRotation;                                        		// 0x0018 (0x000C) [0x0000000000000000]              
	struct FRotator                                    rLockRotation;                                    		// 0x0024 (0x000C) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioStageType.BioStageCamera
// 0x001C
struct FBioStageCamera
{
	struct FName                                       sCameraTag;                                       		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	float                                              fFov;                                             		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fNearPlane;                                       		// 0x000C (0x0004) [0x0000000000000001]              ( CPF_Edit )
	struct FBioStageDOFData                            tDOFData;                                         		// 0x0010 (0x000C) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioTimedActivity.TwitchRec
// 0x0008
struct FTwitchRec
{
	int                                                nTwitchAnim;                                      		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fDelay;                                           		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioTimerList.BioTimer
// 0x0020
struct FBioTimer
{
	struct FScriptDelegate                             OnTimer;                                          		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	class UObject*                                     Params;                                           		// 0x000C (0x0004) [0x0000000000000000]              
	float                                              fTickTime;                                        		// 0x0010 (0x0004) [0x0000000000000000]              
	struct FString                                     sTimerName;                                       		// 0x0014 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioTlkFile.TlkHashEntry
// 0x000C
struct FTlkHashEntry
{
	void*                                              srID_NATIVE_MIRROR;                               		// 0x0000 (0x0004) [0x0000000000001002]              ( CPF_Const | CPF_Native )
	int                                                nFlags_NATIVE_MIRROR;                             		// 0x0004 (0x0004) [0x0000000000001002]              ( CPF_Const | CPF_Native )
	int                                                nStringIndex_NATIVE_MIRROR;                       		// 0x0008 (0x0004) [0x0000000000001002]              ( CPF_Const | CPF_Native )
};

// ScriptStruct BIOC_Base.BioTlkFile.BioHuffmanDecodeNode
// 0x0005
struct FBioHuffmanDecodeNode
{
	int                                                nData_NATIVE_MIRROR;                              		// 0x0000 (0x0004) [0x0000000000001002]              ( CPF_Const | CPF_Native )
	unsigned char                                      bLeaf_NATIVE_MIRROR;                              		// 0x0004 (0x0001) [0x0000000000001002]              ( CPF_Const | CPF_Native )
};

// ScriptStruct BIOC_Base.BioTlkFile.BioHuffmanEntry
// 0x0010
struct FBioHuffmanEntry
{
	int                                                nLength_NATIVE_MIRROR;                            		// 0x0000 (0x0004) [0x0000000000001002]              ( CPF_Const | CPF_Native )
	TArray< unsigned char >                            aBitStream_NATIVE_MIRROR;                         		// 0x0004 (0x000C) [0x0000000000001002]              ( CPF_Const | CPF_Native )
};

// ScriptStruct BIOC_Base.BioTriggerStream.BioStreamingState
// 0x0040
struct FBioStreamingState
{
	struct FName                                       StateName;                                        		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	struct FString                                     StateDescription;                                 		// 0x0008 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	struct FName                                       InChunkName;                                      		// 0x0014 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	unsigned long                                      ColorChunks : 1;                                  		// 0x001C (0x0004) [0x0000000000002001] [0x00000001] ( CPF_Edit | CPF_Transient )
	TArray< struct FName >                             VisibleChunkNames;                                		// 0x0020 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	TArray< struct FName >                             LoadChunkNames;                                   		// 0x002C (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	float                                              DesignFudget;                                     		// 0x0038 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              ArtFudget;                                        		// 0x003C (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioVehicleBehaviorBase.BioVehicleWeaponInfo
// 0x0014
struct FBioVehicleWeaponInfo
{
	int                                                nTurretIndex;                                     		// 0x0000 (0x0004) [0x0000000000000000]              
	unsigned long                                      bClampToTurret : 1;                               		// 0x0004 (0x0004) [0x0000000000000000] [0x00000001] 
	float                                              fTurretYawHalfAngleInDeg;                         		// 0x0008 (0x0004) [0x0000000000000000]              
	float                                              fTurretPitchHalfAngleInDeg;                       		// 0x000C (0x0004) [0x0000000000000000]              
	class ABioWeapon*                                  oVehicleWeaponPtr;                                		// 0x0010 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioVehicleTrackSet.TrackParticleSystem
// 0x0008
struct FTrackParticleSystem
{
	class UPhysicalMaterial*                           oPhysMat;                                         		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class UParticleSystem*                             oParticleSystem;                                  		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioVISFootstep.BioVISFootstepData
// 0x0010
struct FBioVISFootstepData
{
	float                                              fDirtValue;                                       		// 0x0000 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fGrimeValue;                                      		// 0x0004 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	float                                              fCombatValue;                                     		// 0x0008 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	class UPhysicalMaterial*                           oPhysMat;                                         		// 0x000C (0x0004) [0x0000000000000001]              ( CPF_Edit )
};

// ScriptStruct BIOC_Base.BioVisualEffectTrack.BioVisualEffectTrackKey
// 0x000C
struct FBioVisualEffectTrackKey
{
	float                                              Time;                                             		// 0x0000 (0x0004) [0x0000000000000000]              
	float                                              Duration;                                         		// 0x0004 (0x0004) [0x0000000000000000]              
	class ABioVisualEffect*                            m_Effect;                                         		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioVISXModSet.BioVISXData
// 0x000C
struct FBioVISXData
{
	class UPhysicalMaterial*                           oPhysMat;                                         		// 0x0000 (0x0004) [0x0000000000000000]              
	unsigned long                                      bOverrideWeapon : 1;                              		// 0x0004 (0x0004) [0x0000000000000000] [0x00000001] 
	class UBioVFXTemplate*                             oVFXTemplate;                                     		// 0x0008 (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioWeaponRanged.BioWeaponTraceInfo
// 0x0048
struct FBioWeaponTraceInfo
{
	unsigned char                                      m_eHit;                                           		// 0x0000 (0x0001) [0x0000000000000000]              
	class AActor*                                      m_pHitActor;                                      		// 0x0004 (0x0004) [0x0000000000000000]              
	struct FVector                                     m_vHitPoint;                                      		// 0x0008 (0x000C) [0x0000000000000000]              
	struct FVector                                     m_vHitNormal;                                     		// 0x0014 (0x000C) [0x0000000000000000]              
	struct FTraceHitInfo                               m_hitInfo;                                        		// 0x0020 (0x001C) [0x0000000000080000]              ( CPF_Component )
	struct FVector                                     m_vTrace;                                         		// 0x003C (0x000C) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioWeaponRanged.ImpactDelayInfo
// 0x0010
struct FImpactDelayInfo
{
	TArray< struct FBioWeaponTraceInfo >               aImpactTraceInfo;                                 		// 0x0000 (0x000C) [0x0000000000480000]              ( CPF_Component | CPF_NeedCtorLink )
	float                                              fTimeRemaining;                                   		// 0x000C (0x0004) [0x0000000000000000]              
};

// ScriptStruct BIOC_Base.BioWeaponSoundsSet.BioSoundSet
// 0x001C
struct FBioSoundSet
{
	struct FString                                     Label;                                            		// 0x0000 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	unsigned long                                      m_bRandomize : 1;                                 		// 0x000C (0x0004) [0x0000000000000001] [0x00000001] ( CPF_Edit )
	TArray< class USoundCue* >                         m_aSounds;                                        		// 0x0010 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioWorldInteractionType.ActivityRecord
// 0x0030
struct FActivityRecord
{
	struct FName                                       m_nmActivityName;                                 		// 0x0000 (0x0008) [0x0000000000000001]              ( CPF_Edit )
	TArray< class UAnimSet* >                          m_oMaleAnimations;                                		// 0x0008 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	TArray< class UAnimSet* >                          m_oFemaleAnimations;                              		// 0x0014 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
	class USoundCue*                                   m_oActivitySoundCue;                              		// 0x0020 (0x0004) [0x0000000000000001]              ( CPF_Edit )
	TArray< float >                                    m_faActivityTwitchWeighting;                      		// 0x0024 (0x000C) [0x0000000000400001]              ( CPF_Edit | CPF_NeedCtorLink )
};

// ScriptStruct BIOC_Base.BioWorldSaveObject.LevelLookup
// 0x0010
struct FLevelLookup
{
	struct FString                                     m_Name;                                           		// 0x0000 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	class UBioLevelSaveObject*                         m_Save;                                           		// 0x000C (0x0004) [0x0000000000000000]              
};


#ifdef _MSC_VER
	#pragma pack ( pop )
#endif