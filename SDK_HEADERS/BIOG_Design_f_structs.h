/*
#############################################################################################
# Mass Effect (2007) (1.2.20608.0) SDK
# Generated with TheFeckless UE3 SDK Generator v1.4_Beta-Rev.51
# ========================================================================================= #
# File: BIOG_Design_f_structs.h
# ========================================================================================= #
# Credits: uNrEaL, Tamimego, SystemFiles, R00T88, _silencer, the1domo, K@N@VEL
# Thanks: HOOAH07, lowHertz
# Forums: www.uc-forum.com, www.gamedeception.net
#############################################################################################
*/

#ifdef _MSC_VER
	#pragma pack ( push, 0x4 )
#endif

/*
# ========================================================================================= #
# Function Structs
# ========================================================================================= #
*/

// Function BIOG_Design.BioDeathVFXControlGethTrooper.Evaluate
// [0x00020802] ( FUNC_Event )
struct UBioDeathVFXControlGethTrooper_eventEvaluate_Parms
{
	class UBioDeathVFXSpecArrayWrapper*                pDeathVFXSpecArrayWrapper;                        		// 0x0000 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	class UBioDeathVFXGameState*                       pGameState;                                       		// 0x0004 (0x0004) [0x0000000000000080]              ( CPF_Parm )
	// class ABioPawn*                                 pPawn;                                            		// 0x0008 (0x0004) [0x0000000000000000]              
};


#ifdef _MSC_VER
	#pragma pack ( pop )
#endif