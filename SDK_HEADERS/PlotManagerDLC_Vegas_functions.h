/*
#############################################################################################
# Mass Effect (2007) (1.2.20608.0) SDK
# Generated with TheFeckless UE3 SDK Generator v1.4_Beta-Rev.51
# ========================================================================================= #
# File: PlotManagerDLC_Vegas_functions.h
# ========================================================================================= #
# Credits: uNrEaL, Tamimego, SystemFiles, R00T88, _silencer, the1domo, K@N@VEL
# Thanks: HOOAH07, lowHertz
# Forums: www.uc-forum.com, www.gamedeception.net
#############################################################################################
*/

#ifdef _MSC_VER
	#pragma pack ( push, 0x4 )
#endif

/*
# ========================================================================================= #
# Functions
# ========================================================================================= #
*/

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1987
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1987 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1987 = NULL;

	if ( ! pFnF1987 )
		pFnF1987 = (UFunction*) UObject::GObjObjects()->Data[ 122829 ];

	UBioAutoConditionals_execF1987_Parms F1987_Parms;
	F1987_Parms.BioWorld = BioWorld;
	F1987_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1987, &F1987_Parms, NULL );

	return F1987_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1985
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1985 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1985 = NULL;

	if ( ! pFnF1985 )
		pFnF1985 = (UFunction*) UObject::GObjObjects()->Data[ 122834 ];

	UBioAutoConditionals_execF1985_Parms F1985_Parms;
	F1985_Parms.BioWorld = BioWorld;
	F1985_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1985, &F1985_Parms, NULL );

	return F1985_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1983
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1983 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1983 = NULL;

	if ( ! pFnF1983 )
		pFnF1983 = (UFunction*) UObject::GObjObjects()->Data[ 122839 ];

	UBioAutoConditionals_execF1983_Parms F1983_Parms;
	F1983_Parms.BioWorld = BioWorld;
	F1983_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1983, &F1983_Parms, NULL );

	return F1983_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1982
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1982 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1982 = NULL;

	if ( ! pFnF1982 )
		pFnF1982 = (UFunction*) UObject::GObjObjects()->Data[ 122844 ];

	UBioAutoConditionals_execF1982_Parms F1982_Parms;
	F1982_Parms.BioWorld = BioWorld;
	F1982_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1982, &F1982_Parms, NULL );

	return F1982_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1975
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1975 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1975 = NULL;

	if ( ! pFnF1975 )
		pFnF1975 = (UFunction*) UObject::GObjObjects()->Data[ 122849 ];

	UBioAutoConditionals_execF1975_Parms F1975_Parms;
	F1975_Parms.BioWorld = BioWorld;
	F1975_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1975, &F1975_Parms, NULL );

	return F1975_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1974
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1974 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1974 = NULL;

	if ( ! pFnF1974 )
		pFnF1974 = (UFunction*) UObject::GObjObjects()->Data[ 122854 ];

	UBioAutoConditionals_execF1974_Parms F1974_Parms;
	F1974_Parms.BioWorld = BioWorld;
	F1974_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1974, &F1974_Parms, NULL );

	return F1974_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1973
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1973 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1973 = NULL;

	if ( ! pFnF1973 )
		pFnF1973 = (UFunction*) UObject::GObjObjects()->Data[ 122859 ];

	UBioAutoConditionals_execF1973_Parms F1973_Parms;
	F1973_Parms.BioWorld = BioWorld;
	F1973_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1973, &F1973_Parms, NULL );

	return F1973_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F2002
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F2002 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF2002 = NULL;

	if ( ! pFnF2002 )
		pFnF2002 = (UFunction*) UObject::GObjObjects()->Data[ 122863 ];

	UBioAutoConditionals_execF2002_Parms F2002_Parms;
	F2002_Parms.BioWorld = BioWorld;
	F2002_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF2002, &F2002_Parms, NULL );

	return F2002_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1970
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1970 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1970 = NULL;

	if ( ! pFnF1970 )
		pFnF1970 = (UFunction*) UObject::GObjObjects()->Data[ 122868 ];

	UBioAutoConditionals_execF1970_Parms F1970_Parms;
	F1970_Parms.BioWorld = BioWorld;
	F1970_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1970, &F1970_Parms, NULL );

	return F1970_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1969
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1969 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1969 = NULL;

	if ( ! pFnF1969 )
		pFnF1969 = (UFunction*) UObject::GObjObjects()->Data[ 122873 ];

	UBioAutoConditionals_execF1969_Parms F1969_Parms;
	F1969_Parms.BioWorld = BioWorld;
	F1969_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1969, &F1969_Parms, NULL );

	return F1969_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1968
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1968 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1968 = NULL;

	if ( ! pFnF1968 )
		pFnF1968 = (UFunction*) UObject::GObjObjects()->Data[ 122878 ];

	UBioAutoConditionals_execF1968_Parms F1968_Parms;
	F1968_Parms.BioWorld = BioWorld;
	F1968_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1968, &F1968_Parms, NULL );

	return F1968_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1967
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1967 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1967 = NULL;

	if ( ! pFnF1967 )
		pFnF1967 = (UFunction*) UObject::GObjObjects()->Data[ 122883 ];

	UBioAutoConditionals_execF1967_Parms F1967_Parms;
	F1967_Parms.BioWorld = BioWorld;
	F1967_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1967, &F1967_Parms, NULL );

	return F1967_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1966
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1966 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1966 = NULL;

	if ( ! pFnF1966 )
		pFnF1966 = (UFunction*) UObject::GObjObjects()->Data[ 122888 ];

	UBioAutoConditionals_execF1966_Parms F1966_Parms;
	F1966_Parms.BioWorld = BioWorld;
	F1966_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1966, &F1966_Parms, NULL );

	return F1966_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1965
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1965 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1965 = NULL;

	if ( ! pFnF1965 )
		pFnF1965 = (UFunction*) UObject::GObjObjects()->Data[ 122893 ];

	UBioAutoConditionals_execF1965_Parms F1965_Parms;
	F1965_Parms.BioWorld = BioWorld;
	F1965_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1965, &F1965_Parms, NULL );

	return F1965_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1964
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1964 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1964 = NULL;

	if ( ! pFnF1964 )
		pFnF1964 = (UFunction*) UObject::GObjObjects()->Data[ 122898 ];

	UBioAutoConditionals_execF1964_Parms F1964_Parms;
	F1964_Parms.BioWorld = BioWorld;
	F1964_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1964, &F1964_Parms, NULL );

	return F1964_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1963
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1963 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1963 = NULL;

	if ( ! pFnF1963 )
		pFnF1963 = (UFunction*) UObject::GObjObjects()->Data[ 122903 ];

	UBioAutoConditionals_execF1963_Parms F1963_Parms;
	F1963_Parms.BioWorld = BioWorld;
	F1963_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1963, &F1963_Parms, NULL );

	return F1963_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1962
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1962 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1962 = NULL;

	if ( ! pFnF1962 )
		pFnF1962 = (UFunction*) UObject::GObjObjects()->Data[ 122908 ];

	UBioAutoConditionals_execF1962_Parms F1962_Parms;
	F1962_Parms.BioWorld = BioWorld;
	F1962_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1962, &F1962_Parms, NULL );

	return F1962_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1961
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1961 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1961 = NULL;

	if ( ! pFnF1961 )
		pFnF1961 = (UFunction*) UObject::GObjObjects()->Data[ 122913 ];

	UBioAutoConditionals_execF1961_Parms F1961_Parms;
	F1961_Parms.BioWorld = BioWorld;
	F1961_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1961, &F1961_Parms, NULL );

	return F1961_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1960
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1960 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1960 = NULL;

	if ( ! pFnF1960 )
		pFnF1960 = (UFunction*) UObject::GObjObjects()->Data[ 122918 ];

	UBioAutoConditionals_execF1960_Parms F1960_Parms;
	F1960_Parms.BioWorld = BioWorld;
	F1960_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1960, &F1960_Parms, NULL );

	return F1960_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1959
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1959 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1959 = NULL;

	if ( ! pFnF1959 )
		pFnF1959 = (UFunction*) UObject::GObjObjects()->Data[ 122923 ];

	UBioAutoConditionals_execF1959_Parms F1959_Parms;
	F1959_Parms.BioWorld = BioWorld;
	F1959_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1959, &F1959_Parms, NULL );

	return F1959_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1958
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1958 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1958 = NULL;

	if ( ! pFnF1958 )
		pFnF1958 = (UFunction*) UObject::GObjObjects()->Data[ 122928 ];

	UBioAutoConditionals_execF1958_Parms F1958_Parms;
	F1958_Parms.BioWorld = BioWorld;
	F1958_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1958, &F1958_Parms, NULL );

	return F1958_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1957
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1957 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1957 = NULL;

	if ( ! pFnF1957 )
		pFnF1957 = (UFunction*) UObject::GObjObjects()->Data[ 122933 ];

	UBioAutoConditionals_execF1957_Parms F1957_Parms;
	F1957_Parms.BioWorld = BioWorld;
	F1957_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1957, &F1957_Parms, NULL );

	return F1957_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1956
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1956 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1956 = NULL;

	if ( ! pFnF1956 )
		pFnF1956 = (UFunction*) UObject::GObjObjects()->Data[ 122938 ];

	UBioAutoConditionals_execF1956_Parms F1956_Parms;
	F1956_Parms.BioWorld = BioWorld;
	F1956_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1956, &F1956_Parms, NULL );

	return F1956_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1955
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1955 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1955 = NULL;

	if ( ! pFnF1955 )
		pFnF1955 = (UFunction*) UObject::GObjObjects()->Data[ 122943 ];

	UBioAutoConditionals_execF1955_Parms F1955_Parms;
	F1955_Parms.BioWorld = BioWorld;
	F1955_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1955, &F1955_Parms, NULL );

	return F1955_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1954
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1954 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1954 = NULL;

	if ( ! pFnF1954 )
		pFnF1954 = (UFunction*) UObject::GObjObjects()->Data[ 122948 ];

	UBioAutoConditionals_execF1954_Parms F1954_Parms;
	F1954_Parms.BioWorld = BioWorld;
	F1954_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1954, &F1954_Parms, NULL );

	return F1954_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1953
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1953 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1953 = NULL;

	if ( ! pFnF1953 )
		pFnF1953 = (UFunction*) UObject::GObjObjects()->Data[ 122953 ];

	UBioAutoConditionals_execF1953_Parms F1953_Parms;
	F1953_Parms.BioWorld = BioWorld;
	F1953_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1953, &F1953_Parms, NULL );

	return F1953_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1952
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1952 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1952 = NULL;

	if ( ! pFnF1952 )
		pFnF1952 = (UFunction*) UObject::GObjObjects()->Data[ 122958 ];

	UBioAutoConditionals_execF1952_Parms F1952_Parms;
	F1952_Parms.BioWorld = BioWorld;
	F1952_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1952, &F1952_Parms, NULL );

	return F1952_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1951
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1951 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1951 = NULL;

	if ( ! pFnF1951 )
		pFnF1951 = (UFunction*) UObject::GObjObjects()->Data[ 122963 ];

	UBioAutoConditionals_execF1951_Parms F1951_Parms;
	F1951_Parms.BioWorld = BioWorld;
	F1951_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1951, &F1951_Parms, NULL );

	return F1951_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1950
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1950 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1950 = NULL;

	if ( ! pFnF1950 )
		pFnF1950 = (UFunction*) UObject::GObjObjects()->Data[ 122968 ];

	UBioAutoConditionals_execF1950_Parms F1950_Parms;
	F1950_Parms.BioWorld = BioWorld;
	F1950_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1950, &F1950_Parms, NULL );

	return F1950_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1949
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1949 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1949 = NULL;

	if ( ! pFnF1949 )
		pFnF1949 = (UFunction*) UObject::GObjObjects()->Data[ 122973 ];

	UBioAutoConditionals_execF1949_Parms F1949_Parms;
	F1949_Parms.BioWorld = BioWorld;
	F1949_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1949, &F1949_Parms, NULL );

	return F1949_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1948
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1948 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1948 = NULL;

	if ( ! pFnF1948 )
		pFnF1948 = (UFunction*) UObject::GObjObjects()->Data[ 122978 ];

	UBioAutoConditionals_execF1948_Parms F1948_Parms;
	F1948_Parms.BioWorld = BioWorld;
	F1948_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1948, &F1948_Parms, NULL );

	return F1948_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1947
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1947 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1947 = NULL;

	if ( ! pFnF1947 )
		pFnF1947 = (UFunction*) UObject::GObjObjects()->Data[ 122983 ];

	UBioAutoConditionals_execF1947_Parms F1947_Parms;
	F1947_Parms.BioWorld = BioWorld;
	F1947_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1947, &F1947_Parms, NULL );

	return F1947_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1946
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1946 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1946 = NULL;

	if ( ! pFnF1946 )
		pFnF1946 = (UFunction*) UObject::GObjObjects()->Data[ 123047 ];

	UBioAutoConditionals_execF1946_Parms F1946_Parms;
	F1946_Parms.BioWorld = BioWorld;
	F1946_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1946, &F1946_Parms, NULL );

	return F1946_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1945
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1945 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1945 = NULL;

	if ( ! pFnF1945 )
		pFnF1945 = (UFunction*) UObject::GObjObjects()->Data[ 123052 ];

	UBioAutoConditionals_execF1945_Parms F1945_Parms;
	F1945_Parms.BioWorld = BioWorld;
	F1945_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1945, &F1945_Parms, NULL );

	return F1945_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1944
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1944 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1944 = NULL;

	if ( ! pFnF1944 )
		pFnF1944 = (UFunction*) UObject::GObjObjects()->Data[ 123057 ];

	UBioAutoConditionals_execF1944_Parms F1944_Parms;
	F1944_Parms.BioWorld = BioWorld;
	F1944_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1944, &F1944_Parms, NULL );

	return F1944_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1943
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1943 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1943 = NULL;

	if ( ! pFnF1943 )
		pFnF1943 = (UFunction*) UObject::GObjObjects()->Data[ 123109 ];

	UBioAutoConditionals_execF1943_Parms F1943_Parms;
	F1943_Parms.BioWorld = BioWorld;
	F1943_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1943, &F1943_Parms, NULL );

	return F1943_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1942
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1942 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1942 = NULL;

	if ( ! pFnF1942 )
		pFnF1942 = (UFunction*) UObject::GObjObjects()->Data[ 123164 ];

	UBioAutoConditionals_execF1942_Parms F1942_Parms;
	F1942_Parms.BioWorld = BioWorld;
	F1942_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1942, &F1942_Parms, NULL );

	return F1942_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1941
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1941 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1941 = NULL;

	if ( ! pFnF1941 )
		pFnF1941 = (UFunction*) UObject::GObjObjects()->Data[ 123169 ];

	UBioAutoConditionals_execF1941_Parms F1941_Parms;
	F1941_Parms.BioWorld = BioWorld;
	F1941_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1941, &F1941_Parms, NULL );

	return F1941_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1940
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1940 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1940 = NULL;

	if ( ! pFnF1940 )
		pFnF1940 = (UFunction*) UObject::GObjObjects()->Data[ 123174 ];

	UBioAutoConditionals_execF1940_Parms F1940_Parms;
	F1940_Parms.BioWorld = BioWorld;
	F1940_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1940, &F1940_Parms, NULL );

	return F1940_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1935
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1935 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1935 = NULL;

	if ( ! pFnF1935 )
		pFnF1935 = (UFunction*) UObject::GObjObjects()->Data[ 123179 ];

	UBioAutoConditionals_execF1935_Parms F1935_Parms;
	F1935_Parms.BioWorld = BioWorld;
	F1935_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1935, &F1935_Parms, NULL );

	return F1935_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1934
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1934 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1934 = NULL;

	if ( ! pFnF1934 )
		pFnF1934 = (UFunction*) UObject::GObjObjects()->Data[ 123271 ];

	UBioAutoConditionals_execF1934_Parms F1934_Parms;
	F1934_Parms.BioWorld = BioWorld;
	F1934_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1934, &F1934_Parms, NULL );

	return F1934_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1933
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1933 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1933 = NULL;

	if ( ! pFnF1933 )
		pFnF1933 = (UFunction*) UObject::GObjObjects()->Data[ 123283 ];

	UBioAutoConditionals_execF1933_Parms F1933_Parms;
	F1933_Parms.BioWorld = BioWorld;
	F1933_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1933, &F1933_Parms, NULL );

	return F1933_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1932
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1932 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1932 = NULL;

	if ( ! pFnF1932 )
		pFnF1932 = (UFunction*) UObject::GObjObjects()->Data[ 123311 ];

	UBioAutoConditionals_execF1932_Parms F1932_Parms;
	F1932_Parms.BioWorld = BioWorld;
	F1932_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1932, &F1932_Parms, NULL );

	return F1932_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1931
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1931 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1931 = NULL;

	if ( ! pFnF1931 )
		pFnF1931 = (UFunction*) UObject::GObjObjects()->Data[ 123316 ];

	UBioAutoConditionals_execF1931_Parms F1931_Parms;
	F1931_Parms.BioWorld = BioWorld;
	F1931_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1931, &F1931_Parms, NULL );

	return F1931_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1930
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1930 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1930 = NULL;

	if ( ! pFnF1930 )
		pFnF1930 = (UFunction*) UObject::GObjObjects()->Data[ 123321 ];

	UBioAutoConditionals_execF1930_Parms F1930_Parms;
	F1930_Parms.BioWorld = BioWorld;
	F1930_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1930, &F1930_Parms, NULL );

	return F1930_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1929
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1929 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1929 = NULL;

	if ( ! pFnF1929 )
		pFnF1929 = (UFunction*) UObject::GObjObjects()->Data[ 123326 ];

	UBioAutoConditionals_execF1929_Parms F1929_Parms;
	F1929_Parms.BioWorld = BioWorld;
	F1929_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1929, &F1929_Parms, NULL );

	return F1929_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1928
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1928 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1928 = NULL;

	if ( ! pFnF1928 )
		pFnF1928 = (UFunction*) UObject::GObjObjects()->Data[ 123331 ];

	UBioAutoConditionals_execF1928_Parms F1928_Parms;
	F1928_Parms.BioWorld = BioWorld;
	F1928_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1928, &F1928_Parms, NULL );

	return F1928_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1927
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1927 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1927 = NULL;

	if ( ! pFnF1927 )
		pFnF1927 = (UFunction*) UObject::GObjObjects()->Data[ 123336 ];

	UBioAutoConditionals_execF1927_Parms F1927_Parms;
	F1927_Parms.BioWorld = BioWorld;
	F1927_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1927, &F1927_Parms, NULL );

	return F1927_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1926
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1926 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1926 = NULL;

	if ( ! pFnF1926 )
		pFnF1926 = (UFunction*) UObject::GObjObjects()->Data[ 123341 ];

	UBioAutoConditionals_execF1926_Parms F1926_Parms;
	F1926_Parms.BioWorld = BioWorld;
	F1926_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1926, &F1926_Parms, NULL );

	return F1926_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1925
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1925 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1925 = NULL;

	if ( ! pFnF1925 )
		pFnF1925 = (UFunction*) UObject::GObjObjects()->Data[ 123346 ];

	UBioAutoConditionals_execF1925_Parms F1925_Parms;
	F1925_Parms.BioWorld = BioWorld;
	F1925_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1925, &F1925_Parms, NULL );

	return F1925_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1924
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1924 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1924 = NULL;

	if ( ! pFnF1924 )
		pFnF1924 = (UFunction*) UObject::GObjObjects()->Data[ 123351 ];

	UBioAutoConditionals_execF1924_Parms F1924_Parms;
	F1924_Parms.BioWorld = BioWorld;
	F1924_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1924, &F1924_Parms, NULL );

	return F1924_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1923
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1923 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1923 = NULL;

	if ( ! pFnF1923 )
		pFnF1923 = (UFunction*) UObject::GObjObjects()->Data[ 123356 ];

	UBioAutoConditionals_execF1923_Parms F1923_Parms;
	F1923_Parms.BioWorld = BioWorld;
	F1923_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1923, &F1923_Parms, NULL );

	return F1923_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1922
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1922 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1922 = NULL;

	if ( ! pFnF1922 )
		pFnF1922 = (UFunction*) UObject::GObjObjects()->Data[ 123361 ];

	UBioAutoConditionals_execF1922_Parms F1922_Parms;
	F1922_Parms.BioWorld = BioWorld;
	F1922_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1922, &F1922_Parms, NULL );

	return F1922_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1921
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1921 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1921 = NULL;

	if ( ! pFnF1921 )
		pFnF1921 = (UFunction*) UObject::GObjObjects()->Data[ 123366 ];

	UBioAutoConditionals_execF1921_Parms F1921_Parms;
	F1921_Parms.BioWorld = BioWorld;
	F1921_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1921, &F1921_Parms, NULL );

	return F1921_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1920
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1920 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1920 = NULL;

	if ( ! pFnF1920 )
		pFnF1920 = (UFunction*) UObject::GObjObjects()->Data[ 123371 ];

	UBioAutoConditionals_execF1920_Parms F1920_Parms;
	F1920_Parms.BioWorld = BioWorld;
	F1920_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1920, &F1920_Parms, NULL );

	return F1920_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1919
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1919 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1919 = NULL;

	if ( ! pFnF1919 )
		pFnF1919 = (UFunction*) UObject::GObjObjects()->Data[ 123376 ];

	UBioAutoConditionals_execF1919_Parms F1919_Parms;
	F1919_Parms.BioWorld = BioWorld;
	F1919_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1919, &F1919_Parms, NULL );

	return F1919_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1918
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1918 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1918 = NULL;

	if ( ! pFnF1918 )
		pFnF1918 = (UFunction*) UObject::GObjObjects()->Data[ 123381 ];

	UBioAutoConditionals_execF1918_Parms F1918_Parms;
	F1918_Parms.BioWorld = BioWorld;
	F1918_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1918, &F1918_Parms, NULL );

	return F1918_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1917
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1917 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1917 = NULL;

	if ( ! pFnF1917 )
		pFnF1917 = (UFunction*) UObject::GObjObjects()->Data[ 123386 ];

	UBioAutoConditionals_execF1917_Parms F1917_Parms;
	F1917_Parms.BioWorld = BioWorld;
	F1917_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1917, &F1917_Parms, NULL );

	return F1917_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1916
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1916 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1916 = NULL;

	if ( ! pFnF1916 )
		pFnF1916 = (UFunction*) UObject::GObjObjects()->Data[ 123391 ];

	UBioAutoConditionals_execF1916_Parms F1916_Parms;
	F1916_Parms.BioWorld = BioWorld;
	F1916_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1916, &F1916_Parms, NULL );

	return F1916_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1915
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1915 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1915 = NULL;

	if ( ! pFnF1915 )
		pFnF1915 = (UFunction*) UObject::GObjObjects()->Data[ 123396 ];

	UBioAutoConditionals_execF1915_Parms F1915_Parms;
	F1915_Parms.BioWorld = BioWorld;
	F1915_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1915, &F1915_Parms, NULL );

	return F1915_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1914
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1914 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1914 = NULL;

	if ( ! pFnF1914 )
		pFnF1914 = (UFunction*) UObject::GObjObjects()->Data[ 123401 ];

	UBioAutoConditionals_execF1914_Parms F1914_Parms;
	F1914_Parms.BioWorld = BioWorld;
	F1914_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1914, &F1914_Parms, NULL );

	return F1914_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1913
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1913 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1913 = NULL;

	if ( ! pFnF1913 )
		pFnF1913 = (UFunction*) UObject::GObjObjects()->Data[ 123406 ];

	UBioAutoConditionals_execF1913_Parms F1913_Parms;
	F1913_Parms.BioWorld = BioWorld;
	F1913_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1913, &F1913_Parms, NULL );

	return F1913_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1912
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1912 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1912 = NULL;

	if ( ! pFnF1912 )
		pFnF1912 = (UFunction*) UObject::GObjObjects()->Data[ 123411 ];

	UBioAutoConditionals_execF1912_Parms F1912_Parms;
	F1912_Parms.BioWorld = BioWorld;
	F1912_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1912, &F1912_Parms, NULL );

	return F1912_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1911
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1911 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1911 = NULL;

	if ( ! pFnF1911 )
		pFnF1911 = (UFunction*) UObject::GObjObjects()->Data[ 123416 ];

	UBioAutoConditionals_execF1911_Parms F1911_Parms;
	F1911_Parms.BioWorld = BioWorld;
	F1911_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1911, &F1911_Parms, NULL );

	return F1911_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1910
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1910 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1910 = NULL;

	if ( ! pFnF1910 )
		pFnF1910 = (UFunction*) UObject::GObjObjects()->Data[ 123421 ];

	UBioAutoConditionals_execF1910_Parms F1910_Parms;
	F1910_Parms.BioWorld = BioWorld;
	F1910_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1910, &F1910_Parms, NULL );

	return F1910_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1909
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1909 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1909 = NULL;

	if ( ! pFnF1909 )
		pFnF1909 = (UFunction*) UObject::GObjObjects()->Data[ 123426 ];

	UBioAutoConditionals_execF1909_Parms F1909_Parms;
	F1909_Parms.BioWorld = BioWorld;
	F1909_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1909, &F1909_Parms, NULL );

	return F1909_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1908
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1908 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1908 = NULL;

	if ( ! pFnF1908 )
		pFnF1908 = (UFunction*) UObject::GObjObjects()->Data[ 123431 ];

	UBioAutoConditionals_execF1908_Parms F1908_Parms;
	F1908_Parms.BioWorld = BioWorld;
	F1908_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1908, &F1908_Parms, NULL );

	return F1908_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1907
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1907 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1907 = NULL;

	if ( ! pFnF1907 )
		pFnF1907 = (UFunction*) UObject::GObjObjects()->Data[ 123436 ];

	UBioAutoConditionals_execF1907_Parms F1907_Parms;
	F1907_Parms.BioWorld = BioWorld;
	F1907_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1907, &F1907_Parms, NULL );

	return F1907_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1906
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1906 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1906 = NULL;

	if ( ! pFnF1906 )
		pFnF1906 = (UFunction*) UObject::GObjObjects()->Data[ 123441 ];

	UBioAutoConditionals_execF1906_Parms F1906_Parms;
	F1906_Parms.BioWorld = BioWorld;
	F1906_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1906, &F1906_Parms, NULL );

	return F1906_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1905
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1905 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1905 = NULL;

	if ( ! pFnF1905 )
		pFnF1905 = (UFunction*) UObject::GObjObjects()->Data[ 123446 ];

	UBioAutoConditionals_execF1905_Parms F1905_Parms;
	F1905_Parms.BioWorld = BioWorld;
	F1905_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1905, &F1905_Parms, NULL );

	return F1905_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1904
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1904 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1904 = NULL;

	if ( ! pFnF1904 )
		pFnF1904 = (UFunction*) UObject::GObjObjects()->Data[ 123451 ];

	UBioAutoConditionals_execF1904_Parms F1904_Parms;
	F1904_Parms.BioWorld = BioWorld;
	F1904_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1904, &F1904_Parms, NULL );

	return F1904_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1903
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1903 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1903 = NULL;

	if ( ! pFnF1903 )
		pFnF1903 = (UFunction*) UObject::GObjObjects()->Data[ 123456 ];

	UBioAutoConditionals_execF1903_Parms F1903_Parms;
	F1903_Parms.BioWorld = BioWorld;
	F1903_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1903, &F1903_Parms, NULL );

	return F1903_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1902
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1902 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1902 = NULL;

	if ( ! pFnF1902 )
		pFnF1902 = (UFunction*) UObject::GObjObjects()->Data[ 123461 ];

	UBioAutoConditionals_execF1902_Parms F1902_Parms;
	F1902_Parms.BioWorld = BioWorld;
	F1902_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1902, &F1902_Parms, NULL );

	return F1902_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1901
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1901 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1901 = NULL;

	if ( ! pFnF1901 )
		pFnF1901 = (UFunction*) UObject::GObjObjects()->Data[ 123466 ];

	UBioAutoConditionals_execF1901_Parms F1901_Parms;
	F1901_Parms.BioWorld = BioWorld;
	F1901_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1901, &F1901_Parms, NULL );

	return F1901_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1900
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1900 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1900 = NULL;

	if ( ! pFnF1900 )
		pFnF1900 = (UFunction*) UObject::GObjObjects()->Data[ 123471 ];

	UBioAutoConditionals_execF1900_Parms F1900_Parms;
	F1900_Parms.BioWorld = BioWorld;
	F1900_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1900, &F1900_Parms, NULL );

	return F1900_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1899
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1899 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1899 = NULL;

	if ( ! pFnF1899 )
		pFnF1899 = (UFunction*) UObject::GObjObjects()->Data[ 123476 ];

	UBioAutoConditionals_execF1899_Parms F1899_Parms;
	F1899_Parms.BioWorld = BioWorld;
	F1899_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1899, &F1899_Parms, NULL );

	return F1899_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1898
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1898 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1898 = NULL;

	if ( ! pFnF1898 )
		pFnF1898 = (UFunction*) UObject::GObjObjects()->Data[ 123481 ];

	UBioAutoConditionals_execF1898_Parms F1898_Parms;
	F1898_Parms.BioWorld = BioWorld;
	F1898_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1898, &F1898_Parms, NULL );

	return F1898_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1897
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1897 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1897 = NULL;

	if ( ! pFnF1897 )
		pFnF1897 = (UFunction*) UObject::GObjObjects()->Data[ 123486 ];

	UBioAutoConditionals_execF1897_Parms F1897_Parms;
	F1897_Parms.BioWorld = BioWorld;
	F1897_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1897, &F1897_Parms, NULL );

	return F1897_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1896
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1896 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1896 = NULL;

	if ( ! pFnF1896 )
		pFnF1896 = (UFunction*) UObject::GObjObjects()->Data[ 123491 ];

	UBioAutoConditionals_execF1896_Parms F1896_Parms;
	F1896_Parms.BioWorld = BioWorld;
	F1896_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1896, &F1896_Parms, NULL );

	return F1896_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1895
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1895 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1895 = NULL;

	if ( ! pFnF1895 )
		pFnF1895 = (UFunction*) UObject::GObjObjects()->Data[ 123496 ];

	UBioAutoConditionals_execF1895_Parms F1895_Parms;
	F1895_Parms.BioWorld = BioWorld;
	F1895_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1895, &F1895_Parms, NULL );

	return F1895_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1894
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1894 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1894 = NULL;

	if ( ! pFnF1894 )
		pFnF1894 = (UFunction*) UObject::GObjObjects()->Data[ 123501 ];

	UBioAutoConditionals_execF1894_Parms F1894_Parms;
	F1894_Parms.BioWorld = BioWorld;
	F1894_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1894, &F1894_Parms, NULL );

	return F1894_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1888
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1888 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1888 = NULL;

	if ( ! pFnF1888 )
		pFnF1888 = (UFunction*) UObject::GObjObjects()->Data[ 123506 ];

	UBioAutoConditionals_execF1888_Parms F1888_Parms;
	F1888_Parms.BioWorld = BioWorld;
	F1888_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1888, &F1888_Parms, NULL );

	return F1888_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1986
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1986 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1986 = NULL;

	if ( ! pFnF1986 )
		pFnF1986 = (UFunction*) UObject::GObjObjects()->Data[ 123511 ];

	UBioAutoConditionals_execF1986_Parms F1986_Parms;
	F1986_Parms.BioWorld = BioWorld;
	F1986_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1986, &F1986_Parms, NULL );

	return F1986_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1998
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1998 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1998 = NULL;

	if ( ! pFnF1998 )
		pFnF1998 = (UFunction*) UObject::GObjObjects()->Data[ 123516 ];

	UBioAutoConditionals_execF1998_Parms F1998_Parms;
	F1998_Parms.BioWorld = BioWorld;
	F1998_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1998, &F1998_Parms, NULL );

	return F1998_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1997
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1997 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1997 = NULL;

	if ( ! pFnF1997 )
		pFnF1997 = (UFunction*) UObject::GObjObjects()->Data[ 123521 ];

	UBioAutoConditionals_execF1997_Parms F1997_Parms;
	F1997_Parms.BioWorld = BioWorld;
	F1997_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1997, &F1997_Parms, NULL );

	return F1997_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1996
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1996 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1996 = NULL;

	if ( ! pFnF1996 )
		pFnF1996 = (UFunction*) UObject::GObjObjects()->Data[ 123526 ];

	UBioAutoConditionals_execF1996_Parms F1996_Parms;
	F1996_Parms.BioWorld = BioWorld;
	F1996_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1996, &F1996_Parms, NULL );

	return F1996_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1995
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1995 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1995 = NULL;

	if ( ! pFnF1995 )
		pFnF1995 = (UFunction*) UObject::GObjObjects()->Data[ 123531 ];

	UBioAutoConditionals_execF1995_Parms F1995_Parms;
	F1995_Parms.BioWorld = BioWorld;
	F1995_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1995, &F1995_Parms, NULL );

	return F1995_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1994
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1994 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1994 = NULL;

	if ( ! pFnF1994 )
		pFnF1994 = (UFunction*) UObject::GObjObjects()->Data[ 123536 ];

	UBioAutoConditionals_execF1994_Parms F1994_Parms;
	F1994_Parms.BioWorld = BioWorld;
	F1994_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1994, &F1994_Parms, NULL );

	return F1994_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1971
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1971 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1971 = NULL;

	if ( ! pFnF1971 )
		pFnF1971 = (UFunction*) UObject::GObjObjects()->Data[ 123541 ];

	UBioAutoConditionals_execF1971_Parms F1971_Parms;
	F1971_Parms.BioWorld = BioWorld;
	F1971_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1971, &F1971_Parms, NULL );

	return F1971_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F2001
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F2001 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF2001 = NULL;

	if ( ! pFnF2001 )
		pFnF2001 = (UFunction*) UObject::GObjObjects()->Data[ 123546 ];

	UBioAutoConditionals_execF2001_Parms F2001_Parms;
	F2001_Parms.BioWorld = BioWorld;
	F2001_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF2001, &F2001_Parms, NULL );

	return F2001_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1887
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1887 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1887 = NULL;

	if ( ! pFnF1887 )
		pFnF1887 = (UFunction*) UObject::GObjObjects()->Data[ 123551 ];

	UBioAutoConditionals_execF1887_Parms F1887_Parms;
	F1887_Parms.BioWorld = BioWorld;
	F1887_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1887, &F1887_Parms, NULL );

	return F1887_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1891
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1891 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1891 = NULL;

	if ( ! pFnF1891 )
		pFnF1891 = (UFunction*) UObject::GObjObjects()->Data[ 123556 ];

	UBioAutoConditionals_execF1891_Parms F1891_Parms;
	F1891_Parms.BioWorld = BioWorld;
	F1891_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1891, &F1891_Parms, NULL );

	return F1891_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1890
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1890 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1890 = NULL;

	if ( ! pFnF1890 )
		pFnF1890 = (UFunction*) UObject::GObjObjects()->Data[ 123561 ];

	UBioAutoConditionals_execF1890_Parms F1890_Parms;
	F1890_Parms.BioWorld = BioWorld;
	F1890_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1890, &F1890_Parms, NULL );

	return F1890_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1892
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1892 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1892 = NULL;

	if ( ! pFnF1892 )
		pFnF1892 = (UFunction*) UObject::GObjObjects()->Data[ 123566 ];

	UBioAutoConditionals_execF1892_Parms F1892_Parms;
	F1892_Parms.BioWorld = BioWorld;
	F1892_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1892, &F1892_Parms, NULL );

	return F1892_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1885
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1885 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1885 = NULL;

	if ( ! pFnF1885 )
		pFnF1885 = (UFunction*) UObject::GObjObjects()->Data[ 123571 ];

	UBioAutoConditionals_execF1885_Parms F1885_Parms;
	F1885_Parms.BioWorld = BioWorld;
	F1885_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1885, &F1885_Parms, NULL );

	return F1885_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1882
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1882 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1882 = NULL;

	if ( ! pFnF1882 )
		pFnF1882 = (UFunction*) UObject::GObjObjects()->Data[ 123576 ];

	UBioAutoConditionals_execF1882_Parms F1882_Parms;
	F1882_Parms.BioWorld = BioWorld;
	F1882_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1882, &F1882_Parms, NULL );

	return F1882_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1881
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1881 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1881 = NULL;

	if ( ! pFnF1881 )
		pFnF1881 = (UFunction*) UObject::GObjObjects()->Data[ 123581 ];

	UBioAutoConditionals_execF1881_Parms F1881_Parms;
	F1881_Parms.BioWorld = BioWorld;
	F1881_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1881, &F1881_Parms, NULL );

	return F1881_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1880
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1880 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1880 = NULL;

	if ( ! pFnF1880 )
		pFnF1880 = (UFunction*) UObject::GObjObjects()->Data[ 123586 ];

	UBioAutoConditionals_execF1880_Parms F1880_Parms;
	F1880_Parms.BioWorld = BioWorld;
	F1880_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1880, &F1880_Parms, NULL );

	return F1880_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1879
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1879 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1879 = NULL;

	if ( ! pFnF1879 )
		pFnF1879 = (UFunction*) UObject::GObjObjects()->Data[ 123591 ];

	UBioAutoConditionals_execF1879_Parms F1879_Parms;
	F1879_Parms.BioWorld = BioWorld;
	F1879_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1879, &F1879_Parms, NULL );

	return F1879_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1878
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1878 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1878 = NULL;

	if ( ! pFnF1878 )
		pFnF1878 = (UFunction*) UObject::GObjObjects()->Data[ 123596 ];

	UBioAutoConditionals_execF1878_Parms F1878_Parms;
	F1878_Parms.BioWorld = BioWorld;
	F1878_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1878, &F1878_Parms, NULL );

	return F1878_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1877
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1877 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1877 = NULL;

	if ( ! pFnF1877 )
		pFnF1877 = (UFunction*) UObject::GObjObjects()->Data[ 123601 ];

	UBioAutoConditionals_execF1877_Parms F1877_Parms;
	F1877_Parms.BioWorld = BioWorld;
	F1877_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1877, &F1877_Parms, NULL );

	return F1877_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1876
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1876 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1876 = NULL;

	if ( ! pFnF1876 )
		pFnF1876 = (UFunction*) UObject::GObjObjects()->Data[ 123606 ];

	UBioAutoConditionals_execF1876_Parms F1876_Parms;
	F1876_Parms.BioWorld = BioWorld;
	F1876_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1876, &F1876_Parms, NULL );

	return F1876_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1875
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1875 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1875 = NULL;

	if ( ! pFnF1875 )
		pFnF1875 = (UFunction*) UObject::GObjObjects()->Data[ 123611 ];

	UBioAutoConditionals_execF1875_Parms F1875_Parms;
	F1875_Parms.BioWorld = BioWorld;
	F1875_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1875, &F1875_Parms, NULL );

	return F1875_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1874
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1874 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1874 = NULL;

	if ( ! pFnF1874 )
		pFnF1874 = (UFunction*) UObject::GObjObjects()->Data[ 123616 ];

	UBioAutoConditionals_execF1874_Parms F1874_Parms;
	F1874_Parms.BioWorld = BioWorld;
	F1874_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1874, &F1874_Parms, NULL );

	return F1874_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1873
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1873 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1873 = NULL;

	if ( ! pFnF1873 )
		pFnF1873 = (UFunction*) UObject::GObjObjects()->Data[ 123621 ];

	UBioAutoConditionals_execF1873_Parms F1873_Parms;
	F1873_Parms.BioWorld = BioWorld;
	F1873_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1873, &F1873_Parms, NULL );

	return F1873_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1872
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1872 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1872 = NULL;

	if ( ! pFnF1872 )
		pFnF1872 = (UFunction*) UObject::GObjObjects()->Data[ 123626 ];

	UBioAutoConditionals_execF1872_Parms F1872_Parms;
	F1872_Parms.BioWorld = BioWorld;
	F1872_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1872, &F1872_Parms, NULL );

	return F1872_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1871
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1871 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1871 = NULL;

	if ( ! pFnF1871 )
		pFnF1871 = (UFunction*) UObject::GObjObjects()->Data[ 123631 ];

	UBioAutoConditionals_execF1871_Parms F1871_Parms;
	F1871_Parms.BioWorld = BioWorld;
	F1871_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1871, &F1871_Parms, NULL );

	return F1871_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1870
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1870 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1870 = NULL;

	if ( ! pFnF1870 )
		pFnF1870 = (UFunction*) UObject::GObjObjects()->Data[ 123636 ];

	UBioAutoConditionals_execF1870_Parms F1870_Parms;
	F1870_Parms.BioWorld = BioWorld;
	F1870_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1870, &F1870_Parms, NULL );

	return F1870_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1869
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1869 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1869 = NULL;

	if ( ! pFnF1869 )
		pFnF1869 = (UFunction*) UObject::GObjObjects()->Data[ 123641 ];

	UBioAutoConditionals_execF1869_Parms F1869_Parms;
	F1869_Parms.BioWorld = BioWorld;
	F1869_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1869, &F1869_Parms, NULL );

	return F1869_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1868
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1868 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1868 = NULL;

	if ( ! pFnF1868 )
		pFnF1868 = (UFunction*) UObject::GObjObjects()->Data[ 123646 ];

	UBioAutoConditionals_execF1868_Parms F1868_Parms;
	F1868_Parms.BioWorld = BioWorld;
	F1868_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1868, &F1868_Parms, NULL );

	return F1868_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1867
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1867 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1867 = NULL;

	if ( ! pFnF1867 )
		pFnF1867 = (UFunction*) UObject::GObjObjects()->Data[ 123651 ];

	UBioAutoConditionals_execF1867_Parms F1867_Parms;
	F1867_Parms.BioWorld = BioWorld;
	F1867_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1867, &F1867_Parms, NULL );

	return F1867_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1866
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1866 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1866 = NULL;

	if ( ! pFnF1866 )
		pFnF1866 = (UFunction*) UObject::GObjObjects()->Data[ 123656 ];

	UBioAutoConditionals_execF1866_Parms F1866_Parms;
	F1866_Parms.BioWorld = BioWorld;
	F1866_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1866, &F1866_Parms, NULL );

	return F1866_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F2003
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F2003 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF2003 = NULL;

	if ( ! pFnF2003 )
		pFnF2003 = (UFunction*) UObject::GObjObjects()->Data[ 123661 ];

	UBioAutoConditionals_execF2003_Parms F2003_Parms;
	F2003_Parms.BioWorld = BioWorld;
	F2003_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF2003, &F2003_Parms, NULL );

	return F2003_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1972
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1972 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1972 = NULL;

	if ( ! pFnF1972 )
		pFnF1972 = (UFunction*) UObject::GObjObjects()->Data[ 123666 ];

	UBioAutoConditionals_execF1972_Parms F1972_Parms;
	F1972_Parms.BioWorld = BioWorld;
	F1972_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1972, &F1972_Parms, NULL );

	return F1972_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1981
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1981 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1981 = NULL;

	if ( ! pFnF1981 )
		pFnF1981 = (UFunction*) UObject::GObjObjects()->Data[ 123671 ];

	UBioAutoConditionals_execF1981_Parms F1981_Parms;
	F1981_Parms.BioWorld = BioWorld;
	F1981_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1981, &F1981_Parms, NULL );

	return F1981_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1980
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1980 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1980 = NULL;

	if ( ! pFnF1980 )
		pFnF1980 = (UFunction*) UObject::GObjObjects()->Data[ 123676 ];

	UBioAutoConditionals_execF1980_Parms F1980_Parms;
	F1980_Parms.BioWorld = BioWorld;
	F1980_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1980, &F1980_Parms, NULL );

	return F1980_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1939
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1939 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1939 = NULL;

	if ( ! pFnF1939 )
		pFnF1939 = (UFunction*) UObject::GObjObjects()->Data[ 123681 ];

	UBioAutoConditionals_execF1939_Parms F1939_Parms;
	F1939_Parms.BioWorld = BioWorld;
	F1939_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1939, &F1939_Parms, NULL );

	return F1939_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1884
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1884 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1884 = NULL;

	if ( ! pFnF1884 )
		pFnF1884 = (UFunction*) UObject::GObjObjects()->Data[ 123686 ];

	UBioAutoConditionals_execF1884_Parms F1884_Parms;
	F1884_Parms.BioWorld = BioWorld;
	F1884_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1884, &F1884_Parms, NULL );

	return F1884_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1883
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1883 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1883 = NULL;

	if ( ! pFnF1883 )
		pFnF1883 = (UFunction*) UObject::GObjObjects()->Data[ 123710 ];

	UBioAutoConditionals_execF1883_Parms F1883_Parms;
	F1883_Parms.BioWorld = BioWorld;
	F1883_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1883, &F1883_Parms, NULL );

	return F1883_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1886
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1886 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1886 = NULL;

	if ( ! pFnF1886 )
		pFnF1886 = (UFunction*) UObject::GObjObjects()->Data[ 123756 ];

	UBioAutoConditionals_execF1886_Parms F1886_Parms;
	F1886_Parms.BioWorld = BioWorld;
	F1886_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1886, &F1886_Parms, NULL );

	return F1886_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1865
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1865 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1865 = NULL;

	if ( ! pFnF1865 )
		pFnF1865 = (UFunction*) UObject::GObjObjects()->Data[ 123765 ];

	UBioAutoConditionals_execF1865_Parms F1865_Parms;
	F1865_Parms.BioWorld = BioWorld;
	F1865_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1865, &F1865_Parms, NULL );

	return F1865_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1864
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1864 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1864 = NULL;

	if ( ! pFnF1864 )
		pFnF1864 = (UFunction*) UObject::GObjObjects()->Data[ 123859 ];

	UBioAutoConditionals_execF1864_Parms F1864_Parms;
	F1864_Parms.BioWorld = BioWorld;
	F1864_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1864, &F1864_Parms, NULL );

	return F1864_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1863
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1863 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1863 = NULL;

	if ( ! pFnF1863 )
		pFnF1863 = (UFunction*) UObject::GObjObjects()->Data[ 123867 ];

	UBioAutoConditionals_execF1863_Parms F1863_Parms;
	F1863_Parms.BioWorld = BioWorld;
	F1863_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1863, &F1863_Parms, NULL );

	return F1863_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1862
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1862 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1862 = NULL;

	if ( ! pFnF1862 )
		pFnF1862 = (UFunction*) UObject::GObjObjects()->Data[ 123875 ];

	UBioAutoConditionals_execF1862_Parms F1862_Parms;
	F1862_Parms.BioWorld = BioWorld;
	F1862_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1862, &F1862_Parms, NULL );

	return F1862_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1861
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1861 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1861 = NULL;

	if ( ! pFnF1861 )
		pFnF1861 = (UFunction*) UObject::GObjObjects()->Data[ 123902 ];

	UBioAutoConditionals_execF1861_Parms F1861_Parms;
	F1861_Parms.BioWorld = BioWorld;
	F1861_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1861, &F1861_Parms, NULL );

	return F1861_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1860
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1860 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1860 = NULL;

	if ( ! pFnF1860 )
		pFnF1860 = (UFunction*) UObject::GObjObjects()->Data[ 123912 ];

	UBioAutoConditionals_execF1860_Parms F1860_Parms;
	F1860_Parms.BioWorld = BioWorld;
	F1860_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1860, &F1860_Parms, NULL );

	return F1860_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1859
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1859 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1859 = NULL;

	if ( ! pFnF1859 )
		pFnF1859 = (UFunction*) UObject::GObjObjects()->Data[ 124322 ];

	UBioAutoConditionals_execF1859_Parms F1859_Parms;
	F1859_Parms.BioWorld = BioWorld;
	F1859_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1859, &F1859_Parms, NULL );

	return F1859_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1858
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1858 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1858 = NULL;

	if ( ! pFnF1858 )
		pFnF1858 = (UFunction*) UObject::GObjObjects()->Data[ 124327 ];

	UBioAutoConditionals_execF1858_Parms F1858_Parms;
	F1858_Parms.BioWorld = BioWorld;
	F1858_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1858, &F1858_Parms, NULL );

	return F1858_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1857
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1857 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1857 = NULL;

	if ( ! pFnF1857 )
		pFnF1857 = (UFunction*) UObject::GObjObjects()->Data[ 124332 ];

	UBioAutoConditionals_execF1857_Parms F1857_Parms;
	F1857_Parms.BioWorld = BioWorld;
	F1857_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1857, &F1857_Parms, NULL );

	return F1857_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1856
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1856 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1856 = NULL;

	if ( ! pFnF1856 )
		pFnF1856 = (UFunction*) UObject::GObjObjects()->Data[ 124384 ];

	UBioAutoConditionals_execF1856_Parms F1856_Parms;
	F1856_Parms.BioWorld = BioWorld;
	F1856_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1856, &F1856_Parms, NULL );

	return F1856_Parms.ReturnValue;
};

// Function PlotManagerDLC_Vegas.BioAutoConditionals.F1855
// [0x00020002] 
// Parameters infos:
// bool                           ReturnValue                    ( CPF_Parm | CPF_OutParm | CPF_ReturnParm )
// class ABioWorldInfo*           BioWorld                       ( CPF_Parm )
// int                            Argument                       ( CPF_Parm )

bool UBioAutoConditionals::F1855 ( class ABioWorldInfo* BioWorld, int Argument )
{
	static UFunction* pFnF1855 = NULL;

	if ( ! pFnF1855 )
		pFnF1855 = (UFunction*) UObject::GObjObjects()->Data[ 124439 ];

	UBioAutoConditionals_execF1855_Parms F1855_Parms;
	F1855_Parms.BioWorld = BioWorld;
	F1855_Parms.Argument = Argument;

	this->ProcessEvent ( pFnF1855, &F1855_Parms, NULL );

	return F1855_Parms.ReturnValue;
};


#ifdef _MSC_VER
	#pragma pack ( pop )
#endif