/*
#############################################################################################
# Mass Effect (2007) (1.2.20608.0) SDK
# Generated with TheFeckless UE3 SDK Generator v1.4_Beta-Rev.51
# ========================================================================================= #
# File: BIOG_Design_functions.h
# ========================================================================================= #
# Credits: uNrEaL, Tamimego, SystemFiles, R00T88, _silencer, the1domo, K@N@VEL
# Thanks: HOOAH07, lowHertz
# Forums: www.uc-forum.com, www.gamedeception.net
#############################################################################################
*/

#ifdef _MSC_VER
	#pragma pack ( push, 0x4 )
#endif

/*
# ========================================================================================= #
# Functions
# ========================================================================================= #
*/

// Function BIOG_Design.BioDeathVFXControlGethTrooper.Evaluate
// [0x00020802] ( FUNC_Event )
// Parameters infos:
// class UBioDeathVFXSpecArrayWrapper* pDeathVFXSpecArrayWrapper      ( CPF_Parm )
// class UBioDeathVFXGameState*   pGameState                     ( CPF_Parm )

void UBioDeathVFXControlGethTrooper::eventEvaluate ( class UBioDeathVFXSpecArrayWrapper* pDeathVFXSpecArrayWrapper, class UBioDeathVFXGameState* pGameState )
{
	static UFunction* pFnEvaluate = NULL;

	if ( ! pFnEvaluate )
		pFnEvaluate = (UFunction*) UObject::GObjObjects()->Data[ 58260 ];

	UBioDeathVFXControlGethTrooper_eventEvaluate_Parms Evaluate_Parms;
	Evaluate_Parms.pDeathVFXSpecArrayWrapper = pDeathVFXSpecArrayWrapper;
	Evaluate_Parms.pGameState = pGameState;

	this->ProcessEvent ( pFnEvaluate, &Evaluate_Parms, NULL );
};


#ifdef _MSC_VER
	#pragma pack ( pop )
#endif