/*
#############################################################################################
# Mass Effect (2007) (1.2.20608.0) SDK
# Generated with TheFeckless UE3 SDK Generator v1.4_Beta-Rev.51
# ========================================================================================= #
# File: BIOC_VehicleResources_classes.h
# ========================================================================================= #
# Credits: uNrEaL, Tamimego, SystemFiles, R00T88, _silencer, the1domo, K@N@VEL
# Thanks: HOOAH07, lowHertz
# Forums: www.uc-forum.com, www.gamedeception.net
#############################################################################################
*/

#ifdef _MSC_VER
	#pragma pack ( push, 0x4 )
#endif

/*
# ========================================================================================= #
# Constants
# ========================================================================================= #
*/


/*
# ========================================================================================= #
# Enums
# ========================================================================================= #
*/


/*
# ========================================================================================= #
# Classes
# ========================================================================================= #
*/

// Class BIOC_VehicleResources.BioVehicleResources
// 0x0024 (0x0060 - 0x003C)
class UBioVehicleResources : public UObject
{
public:
	TArray< class UMaterialInstance* >                 lstMaterials;                                     		// 0x003C (0x000C) [0x0000000000402000]              ( CPF_Transient | CPF_NeedCtorLink )
	TArray< class UBioAppearanceItem* >                lstAppearanceItems;                               		// 0x0048 (0x000C) [0x0000000000402000]              ( CPF_Transient | CPF_NeedCtorLink )
	TArray< class UBioVFXTemplate* >                   lstVFXTemplates;                                  		// 0x0054 (0x000C) [0x0000000000402000]              ( CPF_Transient | CPF_NeedCtorLink )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 111515 ];

		return pClassPointer;
	};

};

UClass* UBioVehicleResources::pClassPointer = NULL;


#ifdef _MSC_VER
	#pragma pack ( pop )
#endif