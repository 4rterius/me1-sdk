/*
#############################################################################################
# Mass Effect (2007) (1.2.20608.0) SDK
# Generated with TheFeckless UE3 SDK Generator v1.4_Beta-Rev.51
# ========================================================================================= #
# File: BIOG_Design_classes.h
# ========================================================================================= #
# Credits: uNrEaL, Tamimego, SystemFiles, R00T88, _silencer, the1domo, K@N@VEL
# Thanks: HOOAH07, lowHertz
# Forums: www.uc-forum.com, www.gamedeception.net
#############################################################################################
*/

#ifdef _MSC_VER
	#pragma pack ( push, 0x4 )
#endif

/*
# ========================================================================================= #
# Constants
# ========================================================================================= #
*/


/*
# ========================================================================================= #
# Enums
# ========================================================================================= #
*/


/*
# ========================================================================================= #
# Classes
# ========================================================================================= #
*/

// Class BIOG_Design.BioDeathVFXControlGethTrooper
// 0x0000 (0x0044 - 0x0044)
class UBioDeathVFXControlGethTrooper : public UBioDeathVFXControlBasic
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 58259 ];

		return pClassPointer;
	};

	void eventEvaluate ( class UBioDeathVFXSpecArrayWrapper* pDeathVFXSpecArrayWrapper, class UBioDeathVFXGameState* pGameState );
};

UClass* UBioDeathVFXControlGethTrooper::pClassPointer = NULL;


#ifdef _MSC_VER
	#pragma pack ( pop )
#endif