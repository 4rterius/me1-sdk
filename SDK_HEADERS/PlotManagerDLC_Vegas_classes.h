/*
#############################################################################################
# Mass Effect (2007) (1.2.20608.0) SDK
# Generated with TheFeckless UE3 SDK Generator v1.4_Beta-Rev.51
# ========================================================================================= #
# File: PlotManagerDLC_Vegas_classes.h
# ========================================================================================= #
# Credits: uNrEaL, Tamimego, SystemFiles, R00T88, _silencer, the1domo, K@N@VEL
# Thanks: HOOAH07, lowHertz
# Forums: www.uc-forum.com, www.gamedeception.net
#############################################################################################
*/

#ifdef _MSC_VER
	#pragma pack ( push, 0x4 )
#endif

/*
# ========================================================================================= #
# Constants
# ========================================================================================= #
*/


/*
# ========================================================================================= #
# Enums
# ========================================================================================= #
*/


/*
# ========================================================================================= #
# Classes
# ========================================================================================= #
*/

// Class PlotManagerDLC_Vegas.BioAutoConditionals
// 0x0000 (0x003C - 0x003C)
class UBioAutoConditionals : public UBioConditionals
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 124440 ];

		return pClassPointer;
	};

	bool F1987 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1985 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1983 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1982 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1975 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1974 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1973 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F2002 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1970 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1969 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1968 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1967 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1966 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1965 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1964 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1963 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1962 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1961 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1960 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1959 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1958 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1957 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1956 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1955 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1954 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1953 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1952 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1951 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1950 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1949 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1948 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1947 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1946 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1945 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1944 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1943 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1942 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1941 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1940 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1935 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1934 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1933 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1932 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1931 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1930 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1929 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1928 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1927 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1926 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1925 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1924 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1923 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1922 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1921 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1920 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1919 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1918 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1917 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1916 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1915 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1914 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1913 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1912 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1911 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1910 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1909 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1908 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1907 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1906 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1905 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1904 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1903 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1902 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1901 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1900 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1899 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1898 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1897 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1896 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1895 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1894 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1888 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1986 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1998 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1997 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1996 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1995 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1994 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1971 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F2001 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1887 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1891 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1890 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1892 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1885 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1882 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1881 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1880 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1879 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1878 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1877 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1876 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1875 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1874 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1873 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1872 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1871 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1870 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1869 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1868 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1867 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1866 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F2003 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1972 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1981 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1980 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1939 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1884 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1883 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1886 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1865 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1864 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1863 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1862 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1861 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1860 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1859 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1858 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1857 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1856 ( class ABioWorldInfo* BioWorld, int Argument );
	bool F1855 ( class ABioWorldInfo* BioWorld, int Argument );
};

UClass* UBioAutoConditionals::pClassPointer = NULL;


#ifdef _MSC_VER
	#pragma pack ( pop )
#endif