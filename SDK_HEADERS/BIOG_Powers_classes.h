/*
#############################################################################################
# Mass Effect (2007) (1.2.20608.0) SDK
# Generated with TheFeckless UE3 SDK Generator v1.4_Beta-Rev.51
# ========================================================================================= #
# File: BIOG_Powers_classes.h
# ========================================================================================= #
# Credits: uNrEaL, Tamimego, SystemFiles, R00T88, _silencer, the1domo, K@N@VEL
# Thanks: HOOAH07, lowHertz
# Forums: www.uc-forum.com, www.gamedeception.net
#############################################################################################
*/

#ifdef _MSC_VER
	#pragma pack ( push, 0x4 )
#endif

/*
# ========================================================================================= #
# Constants
# ========================================================================================= #
*/

#define CONST_GFX_GROUP_ALWAYS_STACKS                            -1
#define CONST_GFX_GROUP_Marksman                                 0
#define CONST_GFX_GROUP_Critical_Dmg                             1
#define CONST_GFX_GROUP_Critical_Stun                            2
#define CONST_GFX_GROUP_Suppression_Caster                       3
#define CONST_GFX_GROUP_Suppression_Target                       4
#define CONST_GFX_GROUP_Shield_Boost                             5
#define CONST_GFX_GROUP_Adrenaline                               6
#define CONST_GFX_GROUP_Take_Down                                7
#define CONST_GFX_GROUP_Lift                                     8
#define CONST_GFX_GROUP_Warp_DoT                                 9
#define CONST_GFX_GROUP_Weaken                                   10
#define CONST_GFX_GROUP_Stasis                                   11
#define CONST_GFX_GROUP_Sabotage_WeaponPowers                    12
#define CONST_GFX_GROUP_AI_Hacking                               13
#define CONST_GFX_GROUP_Damping_Field                            14
#define CONST_GFX_GROUP_Neural_Shock                             15
#define CONST_GFX_GROUP_Immunity                                 16
#define CONST_GFX_GROUP_Resin_Slow                               18
#define CONST_GFX_GROUP_Acid_DR_Lowered                          19
#define CONST_GFX_GROUP_Singularity                              20
#define CONST_GFX_GROUP_Sabotage_Pistol                          21
#define CONST_GFX_GROUP_Sabotage_Shotgun                         22
#define CONST_GFX_GROUP_Sabotage_Assault                         23
#define CONST_GFX_GROUP_Sabotage_Sniper                          24
#define CONST_GFX_GROUP_Entrench                                 25
#define CONST_GFX_GROUP_Carnage_Setup                            26
#define CONST_GFX_GROUP_Sabotage_BurnDamage                      27
#define CONST_BARRIER_ACTOR_TYPE                                 "BIOG_V_B_Barrier_Z.BioticBarrier01_Type"
#define CONST_BARRIER_HEIGHT                                     150
#define CONST_BARRIER_WIDTH                                      240
#define CONST_BARRIER_DEPTH                                      50
#define CONST_HEAL_INTERVAL                                      0.1
#define CONST_BARRIER_ACTOR_TYPE01                               "BIOG_APL_TYPES.Cover.HexL_90_Type"
#define CONST_DEGREES_TO_UUR                                     0
#define CONST_DEGREES_TO_UUR01                                   16384
#define CONST_TICK_INTERVAL                                      0.1f
#define CONST_REPAIR_INTERVAL                                    0.1
#define CONST_TALENTID_ELECTRONICS                               84
#define CONST_BURN_INTERVAL                                      0.1
#define CONST_FORCE_INTERVAL                                     0.25
#define CONST_WARP_INTERVAL                                      0.1

/*
# ========================================================================================= #
# Enums
# ========================================================================================= #
*/


/*
# ========================================================================================= #
# Classes
# ========================================================================================= #
*/

// Class BIOG_Powers.BioPowerScriptDesign
// 0x0000 (0x005C - 0x005C)
class UBioPowerScriptDesign : public UBioPowerScript
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56733 ];

		return pClassPointer;
	};

	bool AdjustInventoryResource ( class AActor* oActor, unsigned char eResource, float fAmount );
	bool EffectCarnageSetup ( class AActor* oCaster, int nNumberOfUses, float fDuration );
	bool InstantRegenerateShield ( class AActor* oActor, float fPercentOfShieldRegained );
	bool IncreaseAllPowerCooldowns ( class AActor* oActor, float fPercentOfTotalToIncreaseBy, unsigned long bIncreaseTech, unsigned long bIncreaseBiotic, unsigned long bIncreaseWeapon, unsigned long bIncreaseCombat );
	bool ClearAllPowerCooldowns ( class AActor* oActor );
	bool EffectSingularity ( class AActor* oCaster, class AActor* oImpacted, struct FVector vSingularityLocation, float fForce, float fForceInterval, float fDuration, float fBubbleRadius );
	bool EffectRagdoll ( class AActor* oCaster, class AActor* oImpacted, float fDuration, int nGroupID );
	bool EffectEntrenchShield ( class AActor* oCaster, class AActor* oImpacted, float fShieldInc, float fDuration );
	bool EffectRegenerateShield ( class AActor* oCaster, class AActor* oImpacted, float fAmountPerInterval, float fInterval, float fDuration );
	bool EffectDamageVulnerability ( class AActor* oCaster, class AActor* oImpacted, float fDamageMultiplier, float fDuration );
	bool EffectRegen ( class AActor* oCaster, float fHealingFactor, float fDuration );
	bool EffectHealInstant ( class AActor* oCaster, class AActor* oImpacted, float fHealth, float fToxic );
	bool EffectHeal ( class AActor* oCaster, class AActor* oImpacted, float fHealthPerInterval, float fToxicPerInterval, float fInterval, float fDuration );
	bool EffectCombatBoost ( class AActor* oCaster, class AActor* oImpacted, float fDuration, float fDmgDurBonus, float fRegenBonus, float fSuppResistBonus, float fMobilityBonus );
	bool EffectImmunity ( class AActor* oCaster, float fDuration, float fDamageResistBonus );
	bool EffectSniperCritical ( class AActor* oCaster, float fDuration, float fDamageBonus, float fStunDuration, float fMaxDriftRed, float fMinDriftRed );
	bool EffectOverkill ( class AActor* oCaster, float fDuration, float fSuppDamage, float fKickbackRed, float fHeatRed );
	bool EffectMarksman ( class AActor* oCaster, float fDuration, float fMaxDriftRed, float fMinDriftRed, float fDamageBonus, float fRPSBonus, float fHeatRed );
	bool EffectCorrosion ( class AActor* oImpacted, class AActor* oCaster, float fCorrosion, float fDuration );
	bool EffectSuppressingFire ( class AActor* oCaster, float fDuration, float fDriftInc );
	bool EffectZeroGLift ( class AActor* oImpacted, class AActor* oCaster, float fDuration, float fLiftForce, float fLiftDistance, float fDeceleration, float fMinVelocity );
	bool EffectAIHacking ( class AActor* oCaster, class AActor* oImpacted, float fDuration );
	bool EffectDisablePowers ( class AActor* oCaster, class AActor* oImpacted, float fDuration, int nGFXGroup, unsigned long bDisableTech, unsigned long bDisableBiotics, unsigned long bDisableWeaponPowers, unsigned long bDisableCombat );
	bool EffectOverheatWeapons ( class AActor* oCaster, class AActor* oImpacted, float fDuration, float fHeat );
	bool EffectDisableWeapons ( class AActor* oCaster, class AActor* oImpacted, float fDuration, int nGFXGroup );
	bool EffectDisableActions ( class AActor* oCaster, class AActor* oImpacted, float fDuration, int nGFXGroup, unsigned long bDisableActionQueue, unsigned long bDisableMovementStack );
	bool EffectAdjustStability ( class ABioPawn* oTarget, class AActor* oCaster, float fAmount, float fApplyInterval, float fTotalTime );
	bool EffectDamageOverTime ( class AActor* oImpacted, class AActor* oCaster, float fDamagePerInterval, struct FVector vMomentumPerInterval, class UBioDamageType* pDamage, float fInterval, float fDuration, int nGroup );
	bool EffectTakeDamage ( class AActor* oImpacted, class AActor* oCaster, float fDamage, struct FVector vMomentum, class UBioDamageType* pDamage );
	class AActor* SpawnBeacon ( class UBioActorBehavior* oCasterBehavior, struct FString sBeaconActorType, struct FVector vLocation, struct FRotator vFacing, float fDuration, unsigned long bFloatingBeacon, unsigned long bSnapToFloor, unsigned long bSpawnAsProxMine, float fSquadActivateDelay, float fRadiusMulti, float fDamageMulti, class AActor* oOwner );
	class AActor* SpawnBarrier ( class UBioActorBehavior* oCasterBehavior, struct FString sBarrierActorType, struct FVector vLocation, struct FRotator vFacing, float fHealth, float fDuration, unsigned long bSnapToFloor, unsigned long bAddToSquadCover, class AActor* oOwner );
	unsigned char GetFactionRelationship ( class AActor* oCaster, class AActor* oTarget );
	float GetTechResistance ( class AActor* oTarget );
	float GetBioticResistance ( class AActor* oTarget );
	float GetTargetResistance ( class AActor* oTarget );
	float GetDistanceModifier ( class AActor* oTarget, unsigned char eFalloff );
	float GetDmgDurModifier ( class AActor* oCaster );
	int GetPhysicsLevel ( class AActor* oImpacted );
	bool IsOfRace ( class AActor* oImpacted, unsigned char eRace );
	bool HasShields ( class AActor* oImpacted, unsigned long bCheckMax );
	bool IsDeadBody ( class AActor* oImpacted );
	bool IsPlaceable ( class AActor* oImpacted );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioPowerScriptDesign::pClassPointer = NULL;

// Class BIOG_Powers.BioAdrenalineScript
// 0x0000 (0x005C - 0x005C)
class UBioAdrenalineScript : public UBioPowerScriptDesign
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56734 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
};

UClass* UBioAdrenalineScript::pClassPointer = NULL;

// Class BIOG_Powers.BioBarrierScript
// 0x0000 (0x005C - 0x005C)
class UBioBarrierScript : public UBioPowerScriptDesign
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56735 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
};

UClass* UBioBarrierScript::pClassPointer = NULL;

// Class BIOG_Powers.BioCarnageScript
// 0x0018 (0x0074 - 0x005C)
class UBioCarnageScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fDamage;                                        		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fForce;                                         		// 0x0060 (0x0004) [0x0000000000000000]              
	struct FVector                                     m_vCasterLocation;                                		// 0x0064 (0x000C) [0x0000000000000000]              
	class UBioDamageType*                              m_oDamageType;                                    		// 0x0070 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56736 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioCarnageScript::pClassPointer = NULL;

// Class BIOG_Powers.BioCarnageSetupScript
// 0x0000 (0x005C - 0x005C)
class UBioCarnageSetupScript : public UBioPowerScriptDesign
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56737 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
};

UClass* UBioCarnageSetupScript::pClassPointer = NULL;

// Class BIOG_Powers.BioSpawnBeacon
// 0x0020 (0x007C - 0x005C)
class UBioSpawnBeacon : public UBioPowerScriptDesign
{
public:
	TArray< struct FString >                           m_asBeaconActorType;                              		// 0x005C (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )
	float                                              m_fBeaconOffset;                                  		// 0x0068 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bRecordUses : 1;                                		// 0x006C (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      m_bProxMine : 1;                                  		// 0x006C (0x0004) [0x0000000000000000] [0x00000002] 
	unsigned long                                      m_bFloating : 1;                                  		// 0x006C (0x0004) [0x0000000000000000] [0x00000004] 
	unsigned long                                      m_bSnapToGround : 1;                              		// 0x006C (0x0004) [0x0000000000000000] [0x00000008] 
	struct FString                                     m_sRecordUseName;                                 		// 0x0070 (0x000C) [0x0000000000400000]              ( CPF_NeedCtorLink )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56738 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventEndPhase ( unsigned char ePhase, class AActor* oCaster );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioSpawnBeacon::pClassPointer = NULL;

// Class BIOG_Powers.BioDampingBeacon
// 0x0000 (0x007C - 0x007C)
class UBioDampingBeacon : public UBioSpawnBeacon
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56739 ];

		return pClassPointer;
	};

};

UClass* UBioDampingBeacon::pClassPointer = NULL;

// Class BIOG_Powers.BioDampingScript
// 0x0018 (0x0074 - 0x005C)
class UBioDampingScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fMineDamage;                                    		// 0x005C (0x0004) [0x0000000000000000]              
	class UBioDamageType*                              m_oDTExplosion;                                   		// 0x0060 (0x0004) [0x0000000000002000]              ( CPF_Transient )
	float                                              m_fCooldownIncPct;                                		// 0x0064 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bDampenBiotics : 1;                             		// 0x0068 (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      m_bDampenTech : 1;                                		// 0x0068 (0x0004) [0x0000000000000000] [0x00000002] 
	unsigned long                                      m_bDampenWeapons : 1;                             		// 0x0068 (0x0004) [0x0000000000000000] [0x00000004] 
	unsigned long                                      m_bDampenCombat : 1;                              		// 0x0068 (0x0004) [0x0000000000000000] [0x00000008] 
	float                                              m_fDuration;                                      		// 0x006C (0x0004) [0x0000000000000000]              
	int                                                m_nDifficultyLevel;                               		// 0x0070 (0x0004) [0x0000000000000000]              

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56740 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioDampingScript::pClassPointer = NULL;

// Class BIOG_Powers.BioDampingSuicideScript
// 0x0004 (0x0078 - 0x0074)
class UBioDampingSuicideScript : public UBioDampingScript
{
public:
	class UBioDamageType*                              m_oSuicideDamageType;                             		// 0x0074 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56741 ];

		return pClassPointer;
	};

	bool eventEndPhase ( unsigned char ePhase, class AActor* oCaster );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioDampingSuicideScript::pClassPointer = NULL;

// Class BIOG_Powers.BioEMPBeacon
// 0x0000 (0x007C - 0x007C)
class UBioEMPBeacon : public UBioSpawnBeacon
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56742 ];

		return pClassPointer;
	};

};

UClass* UBioEMPBeacon::pClassPointer = NULL;

// Class BIOG_Powers.BioEMPScript
// 0x0020 (0x007C - 0x005C)
class UBioEMPScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fMineDamage;                                    		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fEMPDamage;                                     		// 0x0060 (0x0004) [0x0000000000000000]              
	float                                              m_fSuppressionDamage;                             		// 0x0064 (0x0004) [0x0000000000000000]              
	float                                              m_fDamageVulnerability;                           		// 0x0068 (0x0004) [0x0000000000000000]              
	float                                              m_fDuration;                                      		// 0x006C (0x0004) [0x0000000000000000]              
	class UBioDamageType*                              m_oDTExplosion;                                   		// 0x0070 (0x0004) [0x0000000000002000]              ( CPF_Transient )
	class UBioDamageType*                              m_oDTStability;                                   		// 0x0074 (0x0004) [0x0000000000002000]              ( CPF_Transient )
	class UBioDamageType*                              m_oDTElectroMagneticPulse;                        		// 0x0078 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56743 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioEMPScript::pClassPointer = NULL;

// Class BIOG_Powers.BioEMPSuicideScript
// 0x0004 (0x0080 - 0x007C)
class UBioEMPSuicideScript : public UBioEMPScript
{
public:
	class UBioDamageType*                              m_oSuicideDamageType;                             		// 0x007C (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56744 ];

		return pClassPointer;
	};

	bool eventEndPhase ( unsigned char ePhase, class AActor* oCaster );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioEMPSuicideScript::pClassPointer = NULL;

// Class BIOG_Powers.BioFakePowerScript
// 0x0004 (0x0060 - 0x005C)
class UBioFakePowerScript : public UBioPowerScriptDesign
{
public:
	unsigned long                                      m_bIgnoreFriendly : 1;                            		// 0x005C (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      m_bIgnoreNeutral : 1;                             		// 0x005C (0x0004) [0x0000000000000000] [0x00000002] 
	unsigned long                                      m_bIgnoreHostile : 1;                             		// 0x005C (0x0004) [0x0000000000000000] [0x00000004] 
	unsigned long                                      m_bIgnoreHumanoid : 1;                            		// 0x005C (0x0004) [0x0000000000000000] [0x00000008] 
	unsigned long                                      m_bIgnoreAnimal : 1;                              		// 0x005C (0x0004) [0x0000000000000000] [0x00000010] 
	unsigned long                                      m_bIgnoreMachine : 1;                             		// 0x005C (0x0004) [0x0000000000000000] [0x00000020] 
	unsigned long                                      m_bIgnorePlaceables : 1;                          		// 0x005C (0x0004) [0x0000000000000000] [0x00000040] 

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56745 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
};

UClass* UBioFakePowerScript::pClassPointer = NULL;

// Class BIOG_Powers.BioGethCarnageScript
// 0x0018 (0x0074 - 0x005C)
class UBioGethCarnageScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fDamage;                                        		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fForce;                                         		// 0x0060 (0x0004) [0x0000000000000000]              
	struct FVector                                     m_vCasterLocation;                                		// 0x0064 (0x000C) [0x0000000000000000]              
	class UBioDamageType*                              m_oDamageType;                                    		// 0x0070 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56746 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioGethCarnageScript::pClassPointer = NULL;

// Class BIOG_Powers.BioHackingScript
// 0x000C (0x0068 - 0x005C)
class UBioHackingScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fHackingTime;                                   		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fEliteMult;                                     		// 0x0060 (0x0004) [0x0000000000000000]              
	float                                              m_fSubBossMult;                                   		// 0x0064 (0x0004) [0x0000000000000000]              

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56747 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
};

UClass* UBioHackingScript::pClassPointer = NULL;

// Class BIOG_Powers.BioHealScript
// 0x0018 (0x0074 - 0x005C)
class UBioHealScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fDuration;                                      		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fHealAmountPerInterval;                         		// 0x0060 (0x0004) [0x0000000000000000]              
	float                                              m_fToxicAmountPerInterval;                        		// 0x0064 (0x0004) [0x0000000000000000]              
	float                                              m_fHealInterval;                                  		// 0x0068 (0x0004) [0x0000000000000000]              
	unsigned long                                      m_bHealsAnimals : 1;                              		// 0x006C (0x0004) [0x0000000000000000] [0x00000001] 
	unsigned long                                      m_bHealsHumanoids : 1;                            		// 0x006C (0x0004) [0x0000000000000000] [0x00000002] 
	unsigned long                                      m_bHealsMachines : 1;                             		// 0x006C (0x0004) [0x0000000000000000] [0x00000004] 
	float                                              m_fMediGelPerUse;                                 		// 0x0070 (0x0004) [0x0000000000000000]              

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56748 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
};

UClass* UBioHealScript::pClassPointer = NULL;

// Class BIOG_Powers.BioHealSelfScript
// 0x0000 (0x0074 - 0x0074)
class UBioHealSelfScript : public UBioHealScript
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56749 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventCanStartPower ( class AActor* oCaster );
};

UClass* UBioHealSelfScript::pClassPointer = NULL;

// Class BIOG_Powers.BioHealSquadScript
// 0x0018 (0x0074 - 0x005C)
class UBioHealSquadScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fDuration;                                      		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fHealAmountPerInterval;                         		// 0x0060 (0x0004) [0x0000000000000000]              
	float                                              m_fToxicAmountPerInterval;                        		// 0x0064 (0x0004) [0x0000000000000000]              
	float                                              m_fHealInterval;                                  		// 0x0068 (0x0004) [0x0000000000000000]              
	float                                              m_fCooldownMulti;                                 		// 0x006C (0x0004) [0x0000000000000000]              
	float                                              m_fRezHealthPct;                                  		// 0x0070 (0x0004) [0x0000000000000000]              

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56750 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	bool eventCanStartPower ( class AActor* oCaster );
	bool eventAdjustCooldown ( float* fCoolDownTime );
	bool DoesSquadNeedHealing ( class AActor* oActor );
	float GetCooldownMultiForSquad ( class AActor* oCaster );
	void GetHealAmountForSquad ( class AActor* oCaster, float* fHealAmount, float* fToxicAmount );
};

UClass* UBioHealSquadScript::pClassPointer = NULL;

// Class BIOG_Powers.BioHexBarrierScript
// 0x0000 (0x005C - 0x005C)
class UBioHexBarrierScript : public UBioPowerScriptDesign
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56751 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
};

UClass* UBioHexBarrierScript::pClassPointer = NULL;

// Class BIOG_Powers.BioImmunityScript
// 0x0000 (0x005C - 0x005C)
class UBioImmunityScript : public UBioPowerScriptDesign
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56752 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
};

UClass* UBioImmunityScript::pClassPointer = NULL;

// Class BIOG_Powers.BioLanceArmScript
// 0x0018 (0x0074 - 0x005C)
class UBioLanceArmScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fForce;                                         		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fDamage;                                        		// 0x0060 (0x0004) [0x0000000000000000]              
	struct FVector                                     m_vCasterLocation;                                		// 0x0064 (0x000C) [0x0000000000000000]              
	class UBioDamageType*                              m_oDamageType;                                    		// 0x0070 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56753 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioLanceArmScript::pClassPointer = NULL;

// Class BIOG_Powers.BioLiftScript
// 0x0010 (0x006C - 0x005C)
class UBioLiftScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fForce;                                         		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fDistance;                                      		// 0x0060 (0x0004) [0x0000000000000000]              
	float                                              m_fDuration;                                      		// 0x0064 (0x0004) [0x0000000000000000]              
	int                                                m_nPhysicsLevel;                                  		// 0x0068 (0x0004) [0x0000000000000000]              

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56754 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
};

UClass* UBioLiftScript::pClassPointer = NULL;

// Class BIOG_Powers.BioLiftPLCScript
// 0x0004 (0x0070 - 0x006C)
class UBioLiftPLCScript : public UBioLiftScript
{
public:
	float                                              m_fPawnDurationMult;                              		// 0x006C (0x0004) [0x0000000000000000]              

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56755 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
};

UClass* UBioLiftPLCScript::pClassPointer = NULL;

// Class BIOG_Powers.BioMarksmanScript
// 0x0000 (0x005C - 0x005C)
class UBioMarksmanScript : public UBioPowerScriptDesign
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56756 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
};

UClass* UBioMarksmanScript::pClassPointer = NULL;

// Class BIOG_Powers.BioNeuralShockScript
// 0x0018 (0x0074 - 0x005C)
class UBioNeuralShockScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fDuration;                                      		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fToxicDamage;                                   		// 0x0060 (0x0004) [0x0000000000000000]              
	float                                              m_fPushForce;                                     		// 0x0064 (0x0004) [0x0000000000000000]              
	float                                              m_fPushDamage;                                    		// 0x0068 (0x0004) [0x0000000000000000]              
	class UBioDamageType*                              m_oDTToxic;                                       		// 0x006C (0x0004) [0x0000000000002000]              ( CPF_Transient )
	class UBioDamageType*                              m_oDTPhysics;                                     		// 0x0070 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56757 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioNeuralShockScript::pClassPointer = NULL;

// Class BIOG_Powers.BioOverkillScript
// 0x0000 (0x005C - 0x005C)
class UBioOverkillScript : public UBioPowerScriptDesign
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56758 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
};

UClass* UBioOverkillScript::pClassPointer = NULL;

// Class BIOG_Powers.BioRegenBurstScript
// 0x0000 (0x005C - 0x005C)
class UBioRegenBurstScript : public UBioPowerScriptDesign
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56759 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
};

UClass* UBioRegenBurstScript::pClassPointer = NULL;

// Class BIOG_Powers.BioRepairScript
// 0x000C (0x0068 - 0x005C)
class UBioRepairScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fDuration;                                      		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fRepairAmountPerInterval;                       		// 0x0060 (0x0004) [0x0000000000000000]              
	float                                              m_fRepairInterval;                                		// 0x0064 (0x0004) [0x0000000000000000]              

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56760 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	bool eventCanStartPower ( class AActor* oCaster );
	int GetSquadElectronicsRank ( class AActor* oCaster );
};

UClass* UBioRepairScript::pClassPointer = NULL;

// Class BIOG_Powers.BioSabotageBeacon
// 0x0000 (0x007C - 0x007C)
class UBioSabotageBeacon : public UBioSpawnBeacon
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56761 ];

		return pClassPointer;
	};

};

UClass* UBioSabotageBeacon::pClassPointer = NULL;

// Class BIOG_Powers.BioSabotageScript
// 0x0018 (0x0074 - 0x005C)
class UBioSabotageScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fMineDamage;                                    		// 0x005C (0x0004) [0x0000000000000000]              
	class UBioDamageType*                              m_oDTExplosion;                                   		// 0x0060 (0x0004) [0x0000000000002000]              ( CPF_Transient )
	float                                              m_fDuration;                                      		// 0x0064 (0x0004) [0x0000000000000000]              
	float                                              m_fHeat;                                          		// 0x0068 (0x0004) [0x0000000000000000]              
	float                                              m_fDamagePerSecond;                               		// 0x006C (0x0004) [0x0000000000000000]              
	class UBioDamageType*                              m_oDTBurn;                                        		// 0x0070 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56762 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioSabotageScript::pClassPointer = NULL;

// Class BIOG_Powers.BioSabotageSuicideScript
// 0x0004 (0x0078 - 0x0074)
class UBioSabotageSuicideScript : public UBioSabotageScript
{
public:
	class UBioDamageType*                              m_oSuicideDamageType;                             		// 0x0074 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56763 ];

		return pClassPointer;
	};

	bool eventEndPhase ( unsigned char ePhase, class AActor* oCaster );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioSabotageSuicideScript::pClassPointer = NULL;

// Class BIOG_Powers.BioShieldBoostScript
// 0x0008 (0x0064 - 0x005C)
class UBioShieldBoostScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fDuration;                                      		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fShieldPercentPerSecond;                        		// 0x0060 (0x0004) [0x0000000000000000]              

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56764 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	bool eventCanStartPower ( class AActor* oCaster );
};

UClass* UBioShieldBoostScript::pClassPointer = NULL;

// Class BIOG_Powers.BioShieldEntrenchScript
// 0x000C (0x0068 - 0x005C)
class UBioShieldEntrenchScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fDuration;                                      		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fShieldMax;                                     		// 0x0060 (0x0004) [0x0000000000000000]              
	float                                              m_fShieldsPerInterval;                            		// 0x0064 (0x0004) [0x0000000000000000]              

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56765 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
};

UClass* UBioShieldEntrenchScript::pClassPointer = NULL;

// Class BIOG_Powers.BioSiegePulseScript
// 0x0018 (0x0074 - 0x005C)
class UBioSiegePulseScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fDamage;                                        		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fForce;                                         		// 0x0060 (0x0004) [0x0000000000000000]              
	struct FVector                                     m_vCasterLocation;                                		// 0x0064 (0x000C) [0x0000000000000000]              
	class UBioDamageType*                              m_oDamageType;                                    		// 0x0070 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56766 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioSiegePulseScript::pClassPointer = NULL;

// Class BIOG_Powers.BioSingularityScript
// 0x0020 (0x007C - 0x005C)
class UBioSingularityScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fForcePerSecond;                                		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fDuration;                                      		// 0x0060 (0x0004) [0x0000000000000000]              
	struct FVector                                     m_vSingularityLocation;                           		// 0x0064 (0x000C) [0x0000000000000000]              
	unsigned long                                      m_bLocationSet : 1;                               		// 0x0070 (0x0004) [0x0000000000000000] [0x00000001] 
	float                                              m_fRadius;                                        		// 0x0074 (0x0004) [0x0000000000000000]              
	int                                                m_nPhysicsLevel;                                  		// 0x0078 (0x0004) [0x0000000000000000]              

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56767 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	struct FVector BumpUpFromFloor ( struct FVector vOriginalLocation, float fBumpDistance );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioSingularityScript::pClassPointer = NULL;

// Class BIOG_Powers.BioSmashScript
// 0x0018 (0x0074 - 0x005C)
class UBioSmashScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fForce;                                         		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fDamage;                                        		// 0x0060 (0x0004) [0x0000000000000000]              
	struct FVector                                     m_vCasterLocation;                                		// 0x0064 (0x000C) [0x0000000000000000]              
	class UBioDamageType*                              m_oDamageType;                                    		// 0x0070 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56768 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioSmashScript::pClassPointer = NULL;

// Class BIOG_Powers.BioSniperCritScript
// 0x0000 (0x005C - 0x005C)
class UBioSniperCritScript : public UBioPowerScriptDesign
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56769 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
};

UClass* UBioSniperCritScript::pClassPointer = NULL;

// Class BIOG_Powers.BioStasisScript
// 0x0008 (0x0064 - 0x005C)
class UBioStasisScript : public UBioPowerScriptDesign
{
public:
	unsigned long                                      m_bAllowDamage : 1;                               		// 0x005C (0x0004) [0x0000000000000000] [0x00000001] 
	float                                              m_fDuration;                                      		// 0x0060 (0x0004) [0x0000000000000000]              

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56770 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
};

UClass* UBioStasisScript::pClassPointer = NULL;

// Class BIOG_Powers.BioTakeDownScript
// 0x0000 (0x005C - 0x005C)
class UBioTakeDownScript : public UBioPowerScriptDesign
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56771 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
};

UClass* UBioTakeDownScript::pClassPointer = NULL;

// Class BIOG_Powers.BioTeslaBurstScript
// 0x0010 (0x006C - 0x005C)
class UBioTeslaBurstScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fEMPDamage;                                     		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fDamage;                                        		// 0x0060 (0x0004) [0x0000000000000000]              
	class UBioDamageType*                              m_oDTElectroMagneticPulse;                        		// 0x0064 (0x0004) [0x0000000000002000]              ( CPF_Transient )
	class UBioDamageType*                              m_oDTElectric;                                    		// 0x0068 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56772 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioTeslaBurstScript::pClassPointer = NULL;

// Class BIOG_Powers.BioThrowScript
// 0x001C (0x0078 - 0x005C)
class UBioThrowScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fForce;                                         		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fDamage;                                        		// 0x0060 (0x0004) [0x0000000000000000]              
	struct FVector                                     m_vCasterLocation;                                		// 0x0064 (0x000C) [0x0000000000000000]              
	int                                                m_nPhysicsLevel;                                  		// 0x0070 (0x0004) [0x0000000000000000]              
	class UBioDamageType*                              m_oDamageType;                                    		// 0x0074 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56773 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioThrowScript::pClassPointer = NULL;

// Class BIOG_Powers.BioThrowWarpScript
// 0x002C (0x0088 - 0x005C)
class UBioThrowWarpScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fThrowForce;                                    		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fThrowDamage;                                   		// 0x0060 (0x0004) [0x0000000000000000]              
	struct FVector                                     m_vCasterLocation;                                		// 0x0064 (0x000C) [0x0000000000000000]              
	int                                                m_nPhysicsLevel;                                  		// 0x0070 (0x0004) [0x0000000000000000]              
	class UBioDamageType*                              m_oThrowDamageType;                               		// 0x0074 (0x0004) [0x0000000000002000]              ( CPF_Transient )
	float                                              m_fDamagePerSecond;                               		// 0x0078 (0x0004) [0x0000000000000000]              
	float                                              m_fDuration;                                      		// 0x007C (0x0004) [0x0000000000000000]              
	float                                              m_fDamageMultiplier;                              		// 0x0080 (0x0004) [0x0000000000000000]              
	class UBioDamageType*                              m_oWarpDamageType;                                		// 0x0084 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56774 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioThrowWarpScript::pClassPointer = NULL;

// Class BIOG_Powers.BioToxicSpitScript
// 0x0008 (0x0064 - 0x005C)
class UBioToxicSpitScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fDamage;                                        		// 0x005C (0x0004) [0x0000000000000000]              
	class UBioDamageType*                              m_oDamageType;                                    		// 0x0060 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56775 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioToxicSpitScript::pClassPointer = NULL;

// Class BIOG_Powers.BioToxicSpitSuicideScript
// 0x0004 (0x0068 - 0x0064)
class UBioToxicSpitSuicideScript : public UBioToxicSpitScript
{
public:
	class UBioDamageType*                              m_oSuicideDamageType;                             		// 0x0064 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56776 ];

		return pClassPointer;
	};

	bool eventEndPhase ( unsigned char ePhase, class AActor* oCaster );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioToxicSpitSuicideScript::pClassPointer = NULL;

// Class BIOG_Powers.BioUnityScript
// 0x0000 (0x005C - 0x005C)
class UBioUnityScript : public UBioPowerScriptDesign
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56777 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventCanStartPower ( class AActor* oCaster );
	bool DoesSquadHaveDeadMember ( class AActor* oActor );
};

UClass* UBioUnityScript::pClassPointer = NULL;

// Class BIOG_Powers.BioWarpScript
// 0x0010 (0x006C - 0x005C)
class UBioWarpScript : public UBioPowerScriptDesign
{
public:
	float                                              m_fDamagePerSecond;                               		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fDuration;                                      		// 0x0060 (0x0004) [0x0000000000000000]              
	float                                              m_fDamageMultiplier;                              		// 0x0064 (0x0004) [0x0000000000000000]              
	class UBioDamageType*                              m_oDamageType;                                    		// 0x0068 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 56778 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioWarpScript::pClassPointer = NULL;

// Class BIOG_Powers.BioArmorEmitter
// 0x0018 (0x0074 - 0x005C)
class UBioArmorEmitter : public UBioPowerScriptDesign
{
public:
	float                                              m_fDamage;                                        		// 0x005C (0x0004) [0x0000000000000000]              
	float                                              m_fType;                                          		// 0x0060 (0x0004) [0x0000000000000000]              
	class UBioDamageType*                              m_oEnervate;                                      		// 0x0064 (0x0004) [0x0000000000002000]              ( CPF_Transient )
	class UBioDamageType*                              m_oToxic;                                         		// 0x0068 (0x0004) [0x0000000000002000]              ( CPF_Transient )
	class UBioDamageType*                              m_oSiphon;                                        		// 0x006C (0x0004) [0x0000000000002000]              ( CPF_Transient )
	class UBioDamageType*                              m_oDestab;                                        		// 0x0070 (0x0004) [0x0000000000002000]              ( CPF_Transient )

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 57235 ];

		return pClassPointer;
	};

	bool eventOnImpact ( class AActor* oCaster, float fCasterStability, class AActor* oImpacted, int nPreviouslyImpacted );
	bool eventStartPhase ( unsigned char ePhase, class AActor* oCaster, float fDuration );
	void eventInitializePowerScript ( class UBioPower* pPower );
};

UClass* UBioArmorEmitter::pClassPointer = NULL;

// Class BIOG_Powers.BioHealMachineScript
// 0x0000 (0x0074 - 0x0074)
class UBioHealMachineScript : public UBioHealScript
{
public:

private:
	static UClass* pClassPointer;

public:
	static UClass* StaticClass()
	{
		if ( ! pClassPointer )
			pClassPointer = (UClass*) UObject::GObjObjects()->Data[ 57540 ];

		return pClassPointer;
	};

};

UClass* UBioHealMachineScript::pClassPointer = NULL;


#ifdef _MSC_VER
	#pragma pack ( pop )
#endif