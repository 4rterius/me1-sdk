/*
#############################################################################################
# Mass Effect (2007) (1.2.20608.0) SDK
# Generated with TheFeckless UE3 SDK Generator v1.4_Beta-Rev.51
# ========================================================================================= #
# File: SdkHeaders.h
# ========================================================================================= #
# Credits: uNrEaL, Tamimego, SystemFiles, R00T88, _silencer, the1domo, K@N@VEL
# Thanks: HOOAH07, lowHertz
# Forums: www.uc-forum.com, www.gamedeception.net
#############################################################################################
*/

/*
# ========================================================================================= #
# Defines
# ========================================================================================= #
*/

#define GObjects			ASI_OFFSET_GObjects
#define GNames				ASI_OFFSET_GNames

/*
# ========================================================================================= #
# Structs
# ========================================================================================= #
*/

template< class T > struct TArray 
{ 
public: 
	T* Data; 
	int Count; 
	int Max; 

public: 
	TArray() 
	{ 
		Data = NULL; 
		Count = Max = 0; 
	}; 

public: 
	int Num() 
	{ 
		return this->Count; 
	}; 

	T& operator() ( int i ) 
	{ 
		return this->Data[ i ]; 
	}; 

	const T& operator() ( int i ) const 
	{ 
		return this->Data[ i ]; 
	}; 

	void Add ( T InputData ) 
	{ 
		Data = (T*) realloc ( Data, sizeof ( T ) * ( Count + 1 ) ); 
		Data[ Count++ ] = InputData; 
		Max = Count; 
	}; 

	void Clear() 
	{ 
		free ( Data ); 
		Count = Max = 0; 
	}; 
}; 

template<class TK, class TV> struct TMap
{
public:
	class TPair
	{
		int HashNext;
		TK  Key;
		TK  Value;
	};
	TArray<TPair> Pairs;
	int* Hash;
	int           HashCount;
};

struct FNameEntry 
{ 
	unsigned char	UnknownData00[ 0x10 ]; 
	wchar_t			Name[ 0x10 ]; 
}; 

struct FName 
{ 
	int				Index; 
	int				Number;
	//unsigned char	unknownData00[ 0x4 ]; 

	FName() : Index ( 0 ) {}; 

	FName ( int i ) : Index ( i ) {}; 

	~FName() {}; 

	FName ( wchar_t* FindName ) 
	{ 
		static TArray< int > NameCache; 

		for ( int i = 0; i < NameCache.Count; ++i ) 
		{ 
		if ( ! wcscmp ( this->Names()->Data[ NameCache ( i ) ]->Name, FindName ) ) 
			{ 
				Index = NameCache ( i ); 
				return; 
			} 
		} 

		for ( int i = 0; i < this->Names()->Count; ++i ) 
		{ 
			if ( this->Names()->Data[ i ] ) 
			{ 
				if ( ! wcscmp ( this->Names()->Data[ i ]->Name, FindName ) ) 
				{ 
					NameCache.Add ( i ); 
					Index = i; 
				} 
			} 
		} 
	}; 

	static TArray< FNameEntry* >* Names() 
	{ 
		return (TArray< FNameEntry* >*) GNames; 
	}; 

	wchar_t* GetName() 
	{ 
		return this->Names()->Data[ Index ]->Name; 
	}; 

	bool operator == ( const FName& A ) const 
	{ 
		return ( Index == A.Index ); 
	}; 
}; 

struct FString : public TArray< wchar_t > 
{ 
	FString() {}; 

	FString ( wchar_t* Other ) 
	{ 
		this->Max = this->Count = *Other ? ( wcslen ( Other ) + 1 ) : 0; 

		if ( this->Count ) 
			this->Data = Other; 
	}; 

	~FString() {}; 

	FString operator = ( wchar_t* Other ) 
	{ 
		if ( this->Data != Other ) 
		{ 
			this->Max = this->Count = *Other ? ( wcslen ( Other ) + 1 ) : 0; 

			if ( this->Count ) 
				this->Data = Other; 
		} 

		return *this; 
	}; 
}; 

struct FScriptDelegate 
{ 
	unsigned char UnknownData00[ 0xC ]; 
}; 

struct FRepRecord
{
	class UProperty* Property;
	int Index;
};

/*
# ========================================================================================= #
# Includes
# ========================================================================================= #
*/

#include "SDK_HEADERS\Core_structs.h"
#include "SDK_HEADERS\Core_classes.h"
#include "SDK_HEADERS\Core_f_structs.h"
#include "SDK_HEADERS\Core_functions.h"
#include "SDK_HEADERS\Engine_structs.h"
#include "SDK_HEADERS\Engine_classes.h"
#include "SDK_HEADERS\Engine_f_structs.h"
#include "SDK_HEADERS\Engine_functions.h"
#include "SDK_HEADERS\GameFramework_structs.h"
#include "SDK_HEADERS\GameFramework_classes.h"
#include "SDK_HEADERS\GameFramework_f_structs.h"
#include "SDK_HEADERS\GameFramework_functions.h"
#include "SDK_HEADERS\IpDrv_structs.h"
#include "SDK_HEADERS\IpDrv_classes.h"
#include "SDK_HEADERS\IpDrv_f_structs.h"
#include "SDK_HEADERS\IpDrv_functions.h"
#include "SDK_HEADERS\ISACTAudio_structs.h"
#include "SDK_HEADERS\ISACTAudio_classes.h"
#include "SDK_HEADERS\ISACTAudio_f_structs.h"
#include "SDK_HEADERS\ISACTAudio_functions.h"
#include "SDK_HEADERS\WinDrv_structs.h"
#include "SDK_HEADERS\WinDrv_classes.h"
#include "SDK_HEADERS\WinDrv_f_structs.h"
#include "SDK_HEADERS\WinDrv_functions.h"
#include "SDK_HEADERS\BIOC_Base_structs.h"
#include "SDK_HEADERS\BIOC_Base_classes.h"
#include "SDK_HEADERS\BIOC_Base_f_structs.h"
#include "SDK_HEADERS\BIOC_Base_functions.h"
#include "SDK_HEADERS\PlotManagerMap_structs.h"
#include "SDK_HEADERS\PlotManagerMap_classes.h"
#include "SDK_HEADERS\PlotManagerMap_f_structs.h"
#include "SDK_HEADERS\PlotManagerMap_functions.h"
#include "SDK_HEADERS\BIOG_StrategicAI_structs.h"
#include "SDK_HEADERS\BIOG_StrategicAI_classes.h"
#include "SDK_HEADERS\BIOG_StrategicAI_f_structs.h"
#include "SDK_HEADERS\BIOG_StrategicAI_functions.h"
#include "SDK_HEADERS\BIOG_Powers_structs.h"
#include "SDK_HEADERS\BIOG_Powers_classes.h"
#include "SDK_HEADERS\BIOG_Powers_f_structs.h"
#include "SDK_HEADERS\BIOG_Powers_functions.h"
#include "SDK_HEADERS\BIOG_Design_structs.h"
#include "SDK_HEADERS\BIOG_Design_classes.h"
#include "SDK_HEADERS\BIOG_Design_f_structs.h"
#include "SDK_HEADERS\BIOG_Design_functions.h"
#include "SDK_HEADERS\PlotManager_structs.h"
#include "SDK_HEADERS\PlotManager_classes.h"
#include "SDK_HEADERS\PlotManager_f_structs.h"
#include "SDK_HEADERS\PlotManager_functions.h"
#include "SDK_HEADERS\BIOC_Materials_structs.h"
#include "SDK_HEADERS\BIOC_Materials_classes.h"
#include "SDK_HEADERS\BIOC_Materials_f_structs.h"
#include "SDK_HEADERS\BIOC_Materials_functions.h"
#include "SDK_HEADERS\BIOC_WorldResources_structs.h"
#include "SDK_HEADERS\BIOC_WorldResources_classes.h"
#include "SDK_HEADERS\BIOC_WorldResources_f_structs.h"
#include "SDK_HEADERS\BIOC_WorldResources_functions.h"
#include "SDK_HEADERS\BIOC_VehicleResources_structs.h"
#include "SDK_HEADERS\BIOC_VehicleResources_classes.h"
#include "SDK_HEADERS\BIOC_VehicleResources_f_structs.h"
#include "SDK_HEADERS\BIOC_VehicleResources_functions.h"
//#include "SDK_HEADERS\PlotManagerDLC_Vegas_structs.h"
//#include "SDK_HEADERS\PlotManagerDLC_Vegas_classes.h"
//#include "SDK_HEADERS\PlotManagerDLC_Vegas_f_structs.h"
//#include "SDK_HEADERS\PlotManagerDLC_Vegas_functions.h"
#include "SDK_HEADERS\BIOG_QA_structs.h"
#include "SDK_HEADERS\BIOG_QA_classes.h"
#include "SDK_HEADERS\BIOG_QA_f_structs.h"
#include "SDK_HEADERS\BIOG_QA_functions.h"
//#include "SDK_HEADERS\PlotManagerDLC_UNC_structs.h"
//#include "SDK_HEADERS\PlotManagerDLC_UNC_classes.h"
//#include "SDK_HEADERS\PlotManagerDLC_UNC_f_structs.h"
//#include "SDK_HEADERS\PlotManagerDLC_UNC_functions.h"


struct FOutputDeviceVtbl
{
	void* Serialize;
	void* Flush;
	void* TearDown;
};

struct FOutputDevice
{
	struct FOutputDeviceVtbl* vfptr;
};

struct FFrame : public FOutputDevice
{
	UStruct* Node;          // +0x04
	UObject* Object;        // +0x08
	BYTE* Code;             // +0x0C
	BYTE* Locals;           // +0x10
	FFrame* PreviousFrame;  // +0x14
};
