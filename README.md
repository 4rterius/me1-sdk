# Mass Effect (2007) UE3 SDK

Generated with TheFeckless UE3 SDK Generator v1.4_Beta-Rev.51.

Targets MassEffect.exe v.1.2.20608.0 (tested on english Origin distribution).

## To use:

Include `ME1-SDK\Include.h` into the main translation unit of your DLL.